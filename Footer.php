        </div>
    </div>

</div>
<!-- footer -->
<div class="flex row footer">
        <div class="flex row linkfooter">
        <?php
        // Vérification de la session de connexion
        if (!empty($_SESSION['isAdmin']) )  { ?>
            <a href="controller/Forum.php">Forum</a> 
            <?php }?>
            <a href="view/Legal_Notice.php">Mentions légales</a>
            <a href="view/Terms_of_use.php">Conditions d'utilisation</a>
            <a href="view/RGPD.php">RGPD</a>
        </div>
        <div class="flex row logo_reseau">
            <div class="img_logo"><a href="https://www.instagram.com/?hl=fr"><img src="image/logo_insta.png" alt="Logo instagram"></a></div>
            <div class="img_logo"><a href="https://twitter.com/?lang=fr"><img src="image/logo_twitter.png" alt="Logo twitter"></a></div>                
            <div class="img_logo"><a href="https://fr-fr.facebook.com/"><img src="image/logo_facebook.png" alt="Logo facebook"></a></div>
        </div>
    </div>
</body>
</html>