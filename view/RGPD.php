<?php include '../Header.php'?>
<div class="text_legale">
    <h1>Reglement général sur la protection des données</h1>
    <p>

    <h3> vie privée et le RGPD</h3>

        Soucieux de la protection de votre vie privée, Le Site Français s’engage à assurer le meilleur niveau de protection de vos données personnelles conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée et au Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016.

        Le Site Français s’adresse avant tout à des professionnels qui ont vocation à nous contacter et à nous fournir des coordonnées professionnelles et non-personnelles. Toutes les modalités de traitement des données réalisées par Le Site Français sont réalisées sur la base de données professionnelles.

        Notre politique de protection de la vie privée vous permet d’en savoir plus sur la collecte et le traitement de vos données personnelles par Le Site Français ainsi que sur vos droits.

        Vous pouvez consulter la liste des traitements vous concernant en vous adressant au Data Protection Officer (DPO) de la société Le Site Français :
        •    par voie postale à l’adresse suivante :  Snaketendo – DPO – 2 Rue de la chataigne.
        •    ou par courrier électronique à l’adresse rgpd@naintendo.com

        Pour mieux connaître vos droits vous pouvez également consulter le site de la Commission Nationale de l’Informatique et des Libertés à l’adresse www.cnil.fr.

            Identité du responsable du traitement

        Les données personnelles professionnelles sont collectées par :

        Le Site Français
        9 Rue Vauban
        33000 Bordeaux

        L’équipe du Site Français vous demande de ne transmettre que des données professionnelles.

            Les traitements relatifs à vos données personnelles

        <h3>De quelles données parle-t-on ?</h3>

        En visitant et/ou en utilisant le service proposé sur notre site https://le-site-francais.fr, vous acceptez la collecte, et l’utilisation de vos données personnelles dans les limites et le cadre définis ci-après.

        Ainsi, dans certains cas (formulaires, candidature à des offres d’emploi, participation à des jeux concours…), vous pouvez être invité à laisser des données professionnelles qui pourraient être définies comme personnelles (adresse électronique, nom, prénom, adresse, numéro de téléphone…) nécessaires à l’exécution des services offerts par le site en remplissant le formulaire correspondant. Le caractère obligatoire ou facultatif des données vous est signalé lors de la collecte par un astérisque.

        Le cas échéant, les formulaires sont accompagnés d’une ou plusieurs cases à cocher vous permettant d’accepter ou de refuser que nous utilisions vos données à des fins commerciales pour notre compte et/ou pour le compte de nos partenaires. Sachez que, si vous avez accepté une telle utilisation et que vous changez ultérieurement d’avis, vous pouvez à tout moment nous en informer en utilisant les coordonnées indiquées dans le paragraphe 3 ci-dessous « Quels sont vos droits ? » ou en cliquant sur le lien de désabonnement figurant en bas de chaque communication.

        Nous ne collectons aucune donnée sensible, à savoir aucune donnée relative à vos origines raciales ou ethniques, à vos opinions politiques, philosophiques ou religieuses, ou votre appartenance syndicale, ou qui sont relatives à votre santé ou votre vie sexuelle.

        Par ailleurs, lors de la consultation de notre site web et de l’utilisation de nos services, nous sommes amenés à collecter et traiter des données relatives à votre navigation (notamment des cookies…), et à votre terminal (type de navigateur utilisé, modèle et version de votre système d’exploitation, résolution de votre écran, présence de certains plug-ins…). Ces données seront utilisées d’une part pour adapter nos contenus et services à vos besoins, et d’autre part pour réaliser des statistiques d’utilisation de nos services afin de les rendre plus pertinents.

        Dans certains cas, les données peuvent être directement collectées par un partenaire extérieur au site via la mise en ligne de son propre formulaire sur le site. La gestion des données spécifiques à ce formulaire relève dans ces cas de la responsabilité du partenaire.

        Quand ?

        Nous collectons vos données notamment quand :

            vous naviguez sur notre site et consultez nos produits et services,
            vous participez à un jeu ou un concours,
            vous remplissez un formulaire présent sur notre site.

        Quelles finalités ?

        Nous utilisons vos données personnelles pour :

             vous permettre d’utiliser nos services,
            vous informer sur les services pour lesquels vous avez exprimé un intérêt et qui vous sont proposés sur notre site,
            vous proposer de participer à des jeux ou des concours,
            vous faire parvenir nos communications,
            nous permettre d’établir des statistiques et comptages de fréquentation et d’utilisation des diverses rubriques et contenus de notre site et de nos services notamment afin d’adapter lesdites rubriques et lesdits contenus en fonction de votre navigation,
            vous proposer des offres publicitaires et des informations sur notre site et en dehors de notre site en rapport avec vos centres d’intérêt.

        Le cas échéant, le formulaire de collecte pourra être accompagné d’une case à cocher vous permettant d’accepter ou de refuser que vos données soient utilisées à des fins commerciales pour le compte de tiers.

        Par ailleurs, afin de profiter pleinement de votre offre, nous vous invitons à configurer la connexion avec certains partenaires comme par exemple Google via votre fiche Google My Business. Pour se faire, vous devez autoriser Le Site Français à accéder à votre compte Google.

        Cela permettra au Site Français de :

            Consulter, modifier, créer et supprimer vos fiches d’établissement sur Google, y compris le nom de votre établissement, votre numéro de téléphone et vos horaires d’ouverture
            Valider la propriété de vos fiches d’établissement
            Attribuer leur propriété à un autre utilisateur de Google

        En cas de création de fiche Google My Business via le service proposé par Le Site Français, vous serez invité à gérer votre établissement et la propriété de votre fiche pourra vous être transférée. Pour en savoir plus sur les Règles Google My Business cliquez ici.

        <h3>Cookies</h3>

        Les cookies sont des petits fichiers de textes conservés dans votre navigateur Web lorsque vous naviguez sur notre site et qui vont être utilisés pour reconnaître votre terminal lorsque vous vous connectez à notre site afin de vous proposer des services personnalisés et de mesurer l’audience du site.

        Lors de votre arrivée sur notre site, il se peut donc qu’il vous soit demandé votre accord pour l’utilisation de ces cookies.

        Quels destinataires ?

        Les données collectées sur notre site sont exclusivement destinées à l’équipe du Site Français. Toute transmission de ces données à des sociétés tierces est soumise à votre consentement préalable.

            Quels sont vos droits ?

        Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée, vous disposez notamment de droits d’accès, de rectification et d’opposition aux données personnelles vous concernant. Vous pouvez exercer ces droits à tout moment de la manière suivante :

             par voie postale à l’adresse suivante : Le Site Français, 9 Rue Vauban 33000 Bordeaux
             par email à l’adresse suivante : rgpd@le-site-francais.fr

        Nous pourrons vous demander de nous transmettre un justificatif d’identité par voie postale à l’adresse précitée.

        Nous nous réservons le droit de modifier, à tout moment et sans préavis, tout ou partie de nos règles relatives à la protection de la vie privée. En conséquence, nous vous conseillons de vous référer régulièrement à la dernière version desdites règles disponible en permanence sur notre site. Les modifications entrent en vigueur à la date de leur mise en ligne sur notre site et sont opposables à la date de votre première utilisation de celle-ci. L’utilisation de notre site implique la pleine et entière acceptation de toute révision ou modification de nos règles relatives à la protection de la vie privée.
    </p>
</div>
<?php include '../Footer.php'?>