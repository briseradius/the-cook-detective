<?php
session_start();
include '../Header.php';
include "../bdd.php";

// Récupération des utilisateurs depuis la base de données
$sql = "SELECT id, CONCAT(firstname, ' ', lastname) AS fullname, nickname, birthday, isAdmin FROM users";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);

if (count($users) > 0) {
    // Affichage du tableau HTML
    echo '<table>';
    echo '<tr><th>ID</th><th>Nom complet</th><th>Pseudo</th><th>Âge</th><th>Action</th></tr>';

    foreach ($users as $user) {
        // Calcul de l'âge
        $birthday = new DateTime($user['birthday']);
        $now = new DateTime();
        $age = $birthday->diff($now)->y;

        if(!$user['isAdmin']){
            // Affichage des données de chaque utilisateur dans une ligne du tableau
            echo '<tr>';
            echo '<td>' . $user['id'] . '</td>';
            echo '<td>' . $user['fullname'].'</td>';
            echo '<td>' . $user['nickname'] . '</td>';
            echo '<td>' . $age . '</td>';
            echo '<td>';
            echo '<form method="POST" action="model/deleteUsers.php">';
            echo '<input type="hidden" name="token" value="' . $_SESSION['token'] . '">';
            echo '<input type="hidden" name="id" value="' . $user['id'] . '">';
            echo '<input type="submit" value="Supp">';
            echo '</form>';
            echo '</td>';
            echo '</tr>';
        }
    }

    echo '</table>';
}
else
{
    echo 'Aucun utilisateur trouvé.';
}
include "../Footer.php";
?>
