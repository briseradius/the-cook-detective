<?php
session_start();
include '../Header.php';
include '../bdd.php';
// Vérifier si un ID de recette est passé en paramètre
if (!isset($_GET['id'])) {
  header("Location: view/usersCheckRecipes.php"); // Rediriger vers la page de liste des recettes si aucun ID n'est spécifié
  exit();
}

// Récupérer l'ID de la recette à afficher
$recipeId = $_GET['id'];

// Récupérer les informations de la recette depuis la base de données
$query = $pdo->prepare("SELECT * FROM recipes WHERE id = ?");
$query->execute([$recipeId]);
$recipe = $query->fetch(PDO::FETCH_ASSOC);

// Vérifier si le formulaire de commentaire a été soumis
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['comment'])) {
  // Récupérer les données du formulaire
  $comment = $_POST['comment'];
  $userId = $_SESSION['id'];

  // Insérer le commentaire dans la base de données
  $insertQuery = $pdo->prepare("INSERT INTO comments (users_id, contents, hours, recipes_id) VALUES (?, ?, NOW(), ?)");
  $insertQuery->execute([$userId, $comment, $recipeId]);
}

// Vérifier si le formulaire de modification de commentaire a été soumis
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['edit-comment']) && isset($_POST['comment-id'])) {
  // Récupérer les données du formulaire
  $commentId = $_POST['comment-id'];
  $editedComment = $_POST['edit-comment'];
  $userId = $_SESSION['id'];

  // Mettre à jour le commentaire dans la base de données
  $updateQuery = $pdo->prepare("UPDATE comments SET contents = ? WHERE id = ? AND users_id = ?");
  $updateQuery->execute([$editedComment, $commentId, $userId]);
}

// Vérifier si une demande de suppression de commentaire a été effectuée
if (isset($_GET['delete-comment']) && isset($_GET['comment-id'])) {
  // Vérifier si l'utilisateur est un administrateur ou s'il s'agit de son propre commentaire
  $userId = $_SESSION['id'];
  $isAdmin = true; // Remplacez cette valeur par la logique appropriée pour vérifier si l'utilisateur est un administrateur

  // Supprimer le commentaire de la base de données si l'utilisateur est un administrateur ou s'il s'agit de son propre commentaire
  if ($isAdmin || $_GET['delete-comment'] == $userId) {
    $commentId = $_GET['comment-id'];
    $deleteQuery = $pdo->prepare("DELETE FROM comments WHERE id = ?");
    $deleteQuery->execute([$commentId]);
  }
}

// Récupérer les commentaires de la recette depuis la base de données
$commentsQuery = $pdo->prepare("SELECT c.id, c.contents, c.hours, c.users_id, u.nickname
                               FROM comments c
                               LEFT JOIN users u ON c.users_id = u.id
                               WHERE c.recipes_id = ? AND c.isDelete = 0"); // Ajout de la condition pour exclure les commentaires supprimés
$commentsQuery->execute([$recipeId]);
$comments = $commentsQuery->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="recipe-details">
  <img class="recipe-image" src="uploads/<?php echo $recipe['pictures']; ?>" alt="Photo de la recette <?php echo $recipe['title']; ?>">
  <h1><?php echo $recipe['title']; ?></h1>
  <h3>Ingrédients :</h3>
  <?php
  // Récupérer les ingrédients de la recette à partir de la table "ingredients"
  $ingredientsQuery = $pdo->prepare("SELECT i.name, i.quantity, u.unite
                                     FROM ingredients i
                                     INNER JOIN unity u ON i.unity_id = u.id
                                     WHERE i.recipes_id = ?");
  $ingredientsQuery->execute([$recipeId]);
  $ingredients = $ingredientsQuery->fetchAll(PDO::FETCH_ASSOC);

  if (!empty($ingredients)) {
    echo "<ul class='ingredient-list'>";
    // Afficher chaque ingrédient dans une liste
    foreach ($ingredients as $ingredient) {
      echo "<li>" . htmlspecialchars($ingredient['quantity']) . " " . htmlspecialchars($ingredient['unite']) . " " . htmlspecialchars($ingredient['name']) . "</li>";
    }
    echo "</ul>";
  }
  ?>
  <h3>Description :</h3>
  <p><?php echo $recipe['description']; ?></p>
  <div class="recipe-actions">
  </div>
</div>

<div class="comments-section">
  <h2>Commentaires</h2>
  <?php
  // Afficher les commentaires existants
  foreach ($comments as $comment) {
    //var_dump($comment);
    echo "<div class='comment'>";
    echo "<p>Posté par " . (isset($comment['nickname']) ? htmlspecialchars($comment['nickname']) : 'Utilisateur inconnu') . " le " . (isset($comment['hours']) ? date('d-m-Y H:i', strtotime($comment['hours'])) : 'Date inconnue') . "</p>";
    echo "<p>" . (isset($comment['contents']) ? htmlspecialchars($comment['contents']) : 'Contenu du commentaire indisponible') . "</p>";

    // Afficher les options de modification et de suppression pour les commentaires de l'utilisateur connecté
    $userId = $_SESSION['id'];
    if (isset($comment['users_id']) && $comment['users_id'] == $userId || $_SESSION['isAdmin']) 
    {
      echo "<a href='model/delete-comment.php?comment-id=" . $comment['id'] . "&recipe-id=" . $recipeId . "'>Supprimer</a>";
    }

    echo "</div>";
  }
  ?>

  <h3>Ajouter un commentaire</h3>
  <form action="" method="post">
    <label for="comment">Commentaire :</label><br>
    <textarea class="comments" name="comment" id="comment" required></textarea><br>
    <input class="custom-button subComment" type="submit" value="Ajouter le commentaire">
  </form>
</div>

<?php include '../Footer.php'; ?>
