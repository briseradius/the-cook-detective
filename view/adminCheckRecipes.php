<?php
session_start();
include '../Header.php';
include '../bdd.php';
if ($_SESSION['isAdmin']) 
{
  // Récupérer toutes les recettes de la base de données
  $query = $pdo->prepare("SELECT * FROM recipes");
  $query->execute();
  $recipes = $query->fetchAll(PDO::FETCH_ASSOC);
?>
  <title>Liste des recettes</title>
  <h1>Liste des recettes</h1>

  <?php foreach ($recipes as $recipe):
    if (!$recipe['is_verified']) 
    { ?>
      <div class="card">
        <img src="uploads/<?php echo htmlspecialchars($recipe['pictures']); ?>" alt="Photo de la recette">
        <h2><?php echo htmlspecialchars($recipe['title']); ?></h2>
        <h3>Ingrédients :</h3>
        <?php
        // Récupérer les ingrédients de la recette depuis la base de données
        $recipeId = $recipe['id'];
        $ingredientsQuery = $pdo->prepare("SELECT i.name, i.quantity, u.unite 
                                           FROM ingredients i 
                                           INNER JOIN unity u ON i.unity_id = u.id 
                                           WHERE i.recipes_id = ?");
        $ingredientsQuery->execute([$recipeId]);
        $ingredients = $ingredientsQuery->fetchAll(PDO::FETCH_ASSOC);
        ?>

        <ul>
          <?php foreach ($ingredients as $ingredient): ?>
            <li><?php echo htmlspecialchars($ingredient['quantity']) . ' ' . htmlspecialchars($ingredient['unite']) . ' ' . htmlspecialchars($ingredient['name']); ?></li>
          <?php endforeach; ?>
        </ul>

        <a href="view/adminViewRecipes.php?id=<?php echo $recipe['id']; ?>">Consulter</a>
      </div>
  <?php }
  endforeach;

  include '../Footer.php';
} 
else 
{
  header("Location: index.php");
}
?>
