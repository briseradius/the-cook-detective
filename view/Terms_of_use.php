<?php include '../Header.php'?>
<div class="text_legale">
    <h1>Conditions d'utilisation</h1>
    <p>Le présent Accord concerne votre utilisation (y compris tout accès à) notre site Web actuellement (ainsi que 
        tout matériel et service disponible sur celui-ci, et le(s) site(s) successeur(s) de celui-ci, le « Site »).
        EN UTILISANT LE SITE, VOUS ACCEPTEZ CES CONDITIONS D'UTILISATION ET AFFIRMEZ QUE VOUS AVEZ PLUS DE 13 ANS.
        Les références à « vous » et « votre » dans le présent accord se réfèrent à la fois à la personne qui utilise le site.
        CET ACCORD CONTIENT UNE DISPOSITION D'ARBITRAGE OBLIGATOIRE QUI, COMME ÉNONCÉ À LA SECTION 18 CI-DESSOUS, EXIGE LE RECOURS À 
        L'ARBITRAGE SUR UNE BASE INDIVIDUELLE POUR RÉSOUDRE LES DIFFÉRENDS, PLUTÔT QUE DES PROCÈS DEVANT JURY OU TOUTE AUTRE 
        PROCÉDURE JUDICIAIRE, OU DES RECOURS COLLECTIFS DE QUELQUE NATURE QUE CE SOIT.

        <H3>1. Modifications</H3>

        Nous pouvons modifier le présent Contrat de temps à autre en vous informant de ces modifications par tout moyen raisonnable, y 
        compris en publiant un Contrat révisé sur le Site. De telles modifications ne s'appliqueront pas à tout litige 
        entre vous et nous survenant avant la date à laquelle nous avons publié l'Accord révisé incorporant ces modifications, 
        ou vous avons autrement informé de ces modifications.
        Vous convenez que votre utilisation continue du Site après toute modification du présent Contrat constituera une présomption 
        irréfutable de votre acceptation de ces modifications. La légende « Dernière mise à jour » ci-dessus indique la date à laquelle 
        le présent accord a été modifié pour la dernière fois. Nous pouvons, à tout moment et sans responsabilité, modifier ou 
        interrompre tout ou partie du Site (y compris l'accès au Site via tout lien tiers) ; facturer, modifier ou renoncer aux frais 
        (qui resteront gratuits dans un avenir prévisible) requis pour utiliser le Site ; ou offrir des opportunités à certains ou à 
        tous les utilisateurs du Site.

        <h3>Informations soumises via le site </h3> 

        Votre soumission d'informations via le Site est régie par la Politique de confidentialité de SnakeTenDo, 
        située à l'adresse https://www.snaketendo.com/privacy-policy (la « Politique de confidentialité »). 
        Vous déclarez et garantissez que toutes les informations que vous fournissez en relation avec le Site sont et resteront exactes 
        et complètes, et que vous conserverez et mettrez à jour ces informations si nécessaire. En cas de contradiction entre 
        les dispositions du présent Contrat et la Politique de confidentialité concernant les données personnelles, les dispositions de 
        la Politique de confidentialité prévaudront.

        <h3>3. Questions juridictionnelles</h3>

        Le Site est contrôlé ou exploité (ou les deux) à partir de l'Espace économique européen et n'est pas destiné à nous soumettre 
        à une juridiction ou à une loi en dehors de cette zone. Le site peut ne pas être approprié ou disponible pour une utilisation 
        dans certaines juridictions. Toute utilisation du site est à vos risques et périls et vous devez vous conformer à toutes les lois, 
        règles et réglementations applicables. Nous pouvons limiter la disponibilité du Site à tout moment, en tout ou en partie, 
        à toute personne, zone géographique ou juridiction que nous choisissons.

        <h3>4. Règles de conduite</h3>

        En relation avec le Site, vous ne devez pas :

        Publier, transmettre ou autrement rendre disponible via ou en relation avec le Site tout matériel qui est ou peut être : (a) 
        menaçant, harcelant, dégradant, haineux ou intimidant, ou ne respectant pas les droits et la dignité d'autrui ; (b) diffamatoire, 
        calomnieux, frauduleux ou autrement délictuel ; (c) obscène, indécent, pornographique ou autrement répréhensible ; ou (d) 
        protégé par un droit d'auteur, une marque de commerce, un secret commercial, un droit de publicité ou de confidentialité ou tout 
        autre droit de propriété, sans le consentement écrit exprès préalable du propriétaire concerné.
        Publier, transmettre ou autrement rendre disponible via ou en relation avec le Site tout virus, ver, cheval de Troie, œuf de Pâques, bombe à retardement, logiciel espion ou autre code informatique, fichier ou programme qui est ou est potentiellement dangereux ou invasif ou destiné à endommager ou détourner le fonctionnement ou surveiller l'utilisation de tout matériel, logiciel ou équipement (chacun, un « virus »).
        Utiliser le Site à des fins commerciales autres que celles convenues par CrazyGames, ou à des fins frauduleuses ou autrement 
        délictueuses ou illégales.
        Récolter ou collecter des informations sur les utilisateurs du Site, sauf si cela est fait conformément à la Politique de 
        confidentialité.
        Interférer avec ou perturber le fonctionnement du Site ou des serveurs ou réseaux utilisés pour rendre le Site disponible, y 
        compris en piratant ou en défigurant toute partie du Site ; ou violer toute exigence, procédure ou politique de ces serveurs ou 
        réseaux.
        Restreindre ou empêcher toute autre personne d'utiliser le Site.
        Reproduire, modifier, adapter, traduire, créer des œuvres dérivées de, vendre, louer, prêter, prêter, partager, distribuer ou 
        autrement exploiter toute partie (ou toute utilisation de) du Site, sauf autorisation expresse dans les présentes, sans notre 
        consentement écrit préalable exprès .
        Faire de l'ingénierie inverse, décompiler ou désassembler toute partie du site, sauf si une telle restriction est expressément 
        interdite par la loi applicable.
        Supprimez tout droit d'auteur, marque de commerce ou autre avis de droits de propriété du site.
        Encadrer ou refléter toute partie du site, ou incorporer toute partie du site dans tout produit ou service, sans notre 
        consentement écrit préalable exprès.
        Télécharger et stocker systématiquement le contenu du Site.
        Utiliser un robot, une araignée, une application de recherche/récupération de site ou tout autre dispositif manuel ou automatique 
        pour récupérer, indexer, « gratter », « extraire des données » ou autrement rassembler le contenu du site, ou reproduire ou 
        contourner la structure de navigation ou la présentation du site, sans notre consentement exprès écrit préalable. 
        Vous êtes responsable de l'obtention, de la maintenance et du paiement de tout le matériel et de toutes les télécommunications et 
        autres services nécessaires à l'utilisation du Site.

        <h3>5. Produits</h3>

        Le Site peut mettre à disposition des listes, des descriptions et des images de biens ou de services ou des coupons ou remises 
        connexes (collectivement, les « Produits »), ainsi que des références et des liens vers des Produits. Ces Produits peuvent être 
        mis à disposition par nous ou par des tiers, et peuvent être mis à disposition à toutes fins, y compris à des fins d'information 
        générale. La disponibilité sur le site de toute liste, description ou image d'un produit n'implique pas notre approbation de ce 
        produit ou notre affiliation avec le fournisseur de ce produit. Nous ne faisons aucune déclaration quant à l'exhaustivité, 
        l'exactitude, la fiabilité, la validité ou l'actualité de ces listes, descriptions ou images (y compris les caractéristiques, 
        spécifications et prix qui y sont contenus). Ces informations et la disponibilité de tout produit (y compris la validité de tout 
        coupon ou remise) sont susceptibles d'être modifiées à tout moment sans préavis. Il est de votre responsabilité de vérifier et de 
        respecter toutes les lois locales, étatiques, fédérales et étrangères applicables (y compris les exigences d'âge minimum) 
        concernant la possession, l'utilisation et la vente de tout Produit.

        <h3>6. License</h3>

        À des fins de clarté, vous conservez la propriété de vos Soumissions. Pour chaque soumission, vous nous accordez par la présente 
        une licence mondiale, libre de droits, entièrement payée, non exclusive, perpétuelle, irrévocable, transférable et entièrement 
        sous-licenciable (à plusieurs niveaux), sans contrepartie supplémentaire pour vous ou tout tiers. partie, de reproduire, 
        distribuer, exécuter et afficher (publiquement ou autrement), créer des œuvres dérivées de, adapter, modifier et autrement 
        utiliser, analyser et exploiter cette Soumission, dans tout format ou support maintenant connu ou développé par la suite, et à 
        toute fin ( y compris à des fins promotionnelles, telles que des témoignages).

        En outre, si vous nous fournissez des idées, des propositions, des suggestions ou d'autres éléments par quelque moyen que ce soit 
        («Commentaires»), qu'ils soient liés au site ou autrement, ces commentaires seront considérés comme une soumission, et vous 
        reconnaissez et acceptez par la présente que ces Les commentaires ne sont pas confidentiels, et que votre fourniture de tels 
        commentaires est gratuite, non sollicitée et sans restriction, et ne place CrazyGames sous aucune obligation fiduciaire ou autre.

        Vous déclarez et garantissez que vous disposez de tous les droits nécessaires pour accorder les licences accordées dans cette 
        section, et que vos soumissions, et votre fourniture de celles-ci via et en relation avec le site, sont complètes et exactes, 
        et ne sont pas frauduleuses, délictuelles ou autrement en violation de toute loi applicable ou de tout droit d'un tiers. 
        Dans la mesure permise par la loi applicable, vous renoncez en outre irrévocablement à tout « droit moral » ou à tout autre droit 
        relatif à l'attribution de la paternité ou à l'intégrité des documents concernant chaque Soumission que vous pourriez avoir en 
        vertu de toute loi applicable en vertu de toute théorie juridique.

        <h3>7. Surveillance</h3>

        Nous pouvons (mais n'avons aucune obligation de) surveiller, évaluer, modifier ou supprimer les Soumissions avant ou après leur 
        apparition sur le Site, ou analyser votre accès ou votre utilisation du Site. Nous pouvons divulguer des informations concernant 
        votre accès et votre utilisation du site, ainsi que les circonstances entourant cet accès et cette utilisation, à quiconque pour 
        quelque raison ou fin que ce soit, sans préjudice de la politique de confidentialité.

        <h3>8. Vos droits limités</h3>

        Sous réserve de votre respect du présent Accord, et uniquement aussi longtemps que nous vous autorisons à utiliser le Site, 
        vous pouvez consulter toute partie du Site à laquelle nous vous donnons accès en vertu du présent Accord uniquement pour votre 
        usage personnel, non un usage commercial.

        <h3>9. Droits de propriété de snaketendo</h3>

        Nous et nos fournisseurs possédons le Site, qui est protégé par des droits de propriété et des lois. Nos noms commerciaux, 
        marques de commerce et marques de service incluent Naintendo ainsi que  tous les logos associés. Tous les noms commerciaux, 
        marques de commerce, marques de service et logos sur le site qui ne nous appartiennent pas sont la propriété de leurs propriétaires respectifs. Vous ne pouvez pas utiliser nos noms commerciaux, marques de commerce, marques de service ou logos en relation avec tout produit ou service qui n'est pas le nôtre, ou d'une manière susceptible de prêter à confusion. Aucune information contenue ou mise à disposition sur le Site ne doit être interprétée comme accordant un quelconque droit d'utiliser des noms commerciaux, des marques de commerce, des marques de service ou des logos sans l'autorisation écrite expresse préalable du propriétaire.

        <h3>10. Exclusions de garantie</h3>

        DANS TOUTE LA MESURE AUTORISÉE PAR LA LOI APPLICABLE : (A) LE SITE ET TOUS LES PRODUITS ET MATÉRIAUX DE TIERS SONT MIS À VOTRE 
        DISPOSITION "EN L'ÉTAT", "OÙ EST" ET "LÀ OÙ DISPONIBLE", SANS AUCUNE GARANTIE D'AUCUNE SORTE , QU'ELLE SOIT EXPRESSE, IMPLICITE OU 
        LÉGALE ; ET (B) Naintendo DÉCLINE TOUTE GARANTIE CONCERNANT LE SITE ET TOUT PRODUIT ET MATÉRIEL DE TIERS, Y COMPRIS LES GARANTIES 
        DE QUALITÉ MARCHANDE, D'ADÉQUATION À UN USAGE PARTICULIER, DE NON-CONTREFAÇON ET DE PROPRIÉTÉ. TOUS LES AVIS DE NON-RESPONSABILITÉ 
        DE QUELQUE NATURE QUE CE SOIT (Y COMPRIS DANS CETTE SECTION ET AILLEURS DANS CET ACCORD) SONT FAITS AU PROFIT DE Naintendo ET DE 
        SES AFFILIÉS ET DE LEURS ACTIONNAIRES, ADMINISTRATEURS, DIRIGEANTS, EMPLOYÉS, AFFILIÉS, AGENTS, REPRÉSENTANTS, CONCÉDANTS, 
        FOURNISSEURS ET PRESTATAIRES DE SERVICES RESPECTIFS. (COLLECTIVEMENT, LES « ENTITÉS AFFILIÉES »), ET LEURS SUCCESSEURS ET 
        ATTRIBUTEURS RESPECTIFS.

        Bien que nous essayions de maintenir l'actualité, l'intégrité et la sécurité du Site, nous ne garantissons pas que le Site est ou 
        restera à jour, complet, correct ou sécurisé, ou que l'accès au Site sera ininterrompu. Le site peut contenir des inexactitudes, 
        des erreurs et des éléments qui violent ou sont en conflit avec le présent accord. De plus, des tiers peuvent apporter des 
        modifications non autorisées au site. Si vous avez connaissance d'une telle modification, contactez-nous à legal@naintendo.com 
        avec une description de cette modification et son emplacement sur le Site.

        <h3>11. Limitation de responsabilité</h3>

        DANS TOUTE LA MESURE AUTORISÉE PAR LA LOI APPLICABLE : (A) Naintendo NE SERA PAS RESPONSABLE DES DOMMAGES INDIRECTS, ACCESSOIRES, CONSÉCUTIFS, SPÉCIAUX, EXEMPLAIRES OU 
        PUNITIFS DE QUELQUE NATURE QUE CE SOIT, EN VERTU DE TOUT CONTRAT, TORT (Y COMPRIS LA NÉGLIGENCE), RESPONSABILITÉ STRICTE OU AUTRE 
        THÉORIE , Y COMPRIS LES DOMMAGES POUR PERTE DE PROFITS, D'UTILISATION OU DE DONNÉES, LA PERTE D'AUTRES BIENS INCORPORELS, LA PERTE DE 
        SÉCURITÉ DES SOUMISSIONS (Y COMPRIS L'INTERCEPTION NON AUTORISÉE PAR DES TIERS DE TOUTES SOUMISSIONS (B) SANS LIMITER CE QUI PRÉCÈDE, 
        NOUS NE SERONS PAS RESPONSABLES DES DOMMAGES DE QUELQUE NATURE QUE CE SOIT. RÉSULTANT DE VOTRE UTILISATION OU DE VOTRE INCAPACITÉ À 
        UTILISER LE SITE OU DE TOUT PRODUIT OU MATÉRIEL DE TIERS, Y COMPRIS DE TOUT VIRUS POUVANT ÊTRE TRANSMIS EN RELATION AVEC CE SITE ; (C) 
        VOTRE SEUL ET UNIQUE RECOURS EN CAS D'INSATISFACTION À L'ÉGARD DU SITE OU DE TOUT PRODUIT OU D'UN TIERS LE MATÉRIEL DE LA PARTIE DOIT 
        CESSER D'UTILISER LE SITE ; ET (D) NOTRE RESPONSABILITÉ GLOBALE MAXIMALE POUR TOUS DOMMAGES, PERTES ET CAUSES D'ACTION, QUE CE SOIT 
        CONTRACTUEL, DÉLICTUEL (Y COMPRIS LA NÉGLIGENCE) OU AUTRE, SERA LE PLUS ÉLEVÉ DU MONTANT TOTAL, LE CAS ÉCHÉANT, QUE VOUS NOUS AVEZ 
        PAYÉ POUR UTILISER LE SITE, MAIS PAS PLUS DE 100 EUR CONSIDÉRANT QUE L'UTILISATION DU SITE EST GRATUITE. TOUTES LES LIMITATIONS DE 
        RESPONSABILITÉ DE QUELQUE NATURE QUE CE SOIT (Y COMPRIS DANS CETTE SECTION ET AILLEURS DANS LE PRÉSENT CONTRAT) SONT FAITES POUR 
        LE BÉNÉFICE DE NOUS ET DES ENTITÉS AFFILIÉES, ET DE LEURS SUCCESSEURS ET ATTRIBUTEURS RESPECTIFS.

        <h3>12. Résiliation</h3>

        Le présent accord est effectif jusqu'à sa résiliation. Nous pouvons résilier ou suspendre votre utilisation du Site à tout moment 
        et sans préavis, pour quelque raison que ce soit, y compris, sans s'y limiter, si Naintendo détermine à sa seule discrétion que 
        vous n'avez pas respecté les Règles de conduite ou si naintendo estime que vous avez violé ou agi de manière incompatible avec la 
        lettre ou l'esprit du présent accord. Lors d'une telle résiliation ou suspension, votre droit d'utiliser le Site cessera 
        immédiatement, et naintendo peut, sans responsabilité envers vous ou tout tiers, immédiatement désactiver ou supprimer votre nom 
        d'utilisateur, mot de passe et compte, et tous les matériaux associés, sans aucune obligation. de fournir tout autre accès à ces 
        documents.
    </p>
</div>
<?php include '../Footer.php'?>