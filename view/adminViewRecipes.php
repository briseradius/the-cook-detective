<?php
session_start();
include '../Header.php';
include '../bdd.php';
if ($_SESSION['isAdmin']) 
{
  // Vérifier si un ID de recette est passé en paramètre
  if (!isset($_GET['id'])) 
  {
    header("Location: view/adminCheckRecipes.php"); // Rediriger vers la page de liste des recettes si aucun ID n'est spécifié
    exit();
  }

  // Récupérer et valider l'ID de la recette à afficher
  $recipeId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

  if ($recipeId) 
  {
    // Récupérer les informations de la recette depuis la base de données
    $query = $pdo->prepare("SELECT * FROM recipes WHERE id = ?");
    $query->execute([$recipeId]);
    $recipe = $query->fetch(PDO::FETCH_ASSOC);

    // Vérifier si la recette existe
    if ($recipe) 
    {
      // Vérifier si l'utilisateur est connecté et s'il est l'auteur de la recette (ou un administrateur)
      $userId = $_SESSION['id'] ?? null;
      $isAuthor = ($userId && $userId == $recipe['users_id']);
      $isAdministrator = ($_SESSION['isAdmin'] ?? false);

      // Traitement des actions "Effacer" et "Vérifier"
      if ($_SERVER['REQUEST_METHOD'] === 'POST') 
      {
        if (isset($_POST['delete'])) 
        {
          // Supprimer la recette de la base de données seulement si l'utilisateur est autorisé
          if ($isAdministrator) 
          {
            $deleteQuery = $pdo->prepare("DELETE FROM recipes WHERE id = ?");
            $deleteQuery->execute([$recipeId]);
            exit();
          } 
          else 
          {
            // L'utilisateur n'est pas autorisé à supprimer la recette
            echo "Vous n'êtes pas autorisé à supprimer cette recette.";
          }
        } 
        elseif (isset($_POST['verify'])) 
        {
          // Mettre à jour la valeur de is_verified à 1 seulement si l'utilisateur est un administrateur
          if ($isAdministrator) 
          {
            $verifyQuery = $pdo->prepare("UPDATE recipes SET is_verified = 1 WHERE id = ?");
            $verifyQuery->execute([$recipeId]);
          } 
          else
          {
            // L'utilisateur n'est pas autorisé à vérifier la recette
            echo "Vous n'êtes pas autorisé à vérifier cette recette.";
          }
        }
      }

      // Affichage des détails de la recette
      ?>
      <div class="recipe-details">
        <img class="recipe-image" src="uploads/<?php echo $recipe['pictures']; ?>" alt="Photo de la recette">
        <h1><?php echo htmlspecialchars($recipe['title']); ?></h1>
        <h3>Ingrédients :</h3>
        <?php
        // Récupérer les ingrédients de la recette depuis la base de données
        $ingredientsQuery = $pdo->prepare("SELECT i.name, i.quantity, u.unite
                                           FROM ingredients i
                                           INNER JOIN unity u ON i.unity_id = u.id
                                           WHERE i.recipes_id = ?");
        $ingredientsQuery->execute([$recipeId]);
        $ingredients = $ingredientsQuery->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($ingredients)) 
        {
          echo "<ul class='ingredient-list'>";
          // Afficher chaque ingrédient dans une liste
          foreach ($ingredients as $ingredient) 
          {
            echo "<li>" . htmlspecialchars($ingredient['quantity']) . " " . htmlspecialchars($ingredient['unite']) . " " . htmlspecialchars($ingredient['name']) . "</li>";
          }
          echo "</ul>";
        }
        ?>
        <h3>Description :</h3>
        <p><?php echo htmlspecialchars($recipe['description']); ?></p>
        <div class="recipe-actions">
          <form method="post" style="display: inline;">
            <input class="custom-button" type="submit" name="delete" value="Effacer">
          </form>
          <?php if (!$recipe['is_verified'] && $isAdministrator): ?>
            <form method="post" style="display: inline;">
              <input class="custom-button" type="submit" name="verify" value="Vérifier">
            </form>
          <?php endif; ?>
        </div>
      </div>
      <?php
    } 
    else 
    {
      header("Location: view/adminCheckRecipes.php");
      exit();
    }
  } 
  else 
  {
    // ID de recette invalide
    header("Location: view/adminCheckRecipes.php");
    exit();
  }
  include '../Footer.php';
} 
else 
{
  header("Location:index.php");
}
?>
