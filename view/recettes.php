<?php include '../Header.php'?>
<div class="service_products">
    <h1>Recettes</h1>
    <div class="flex row produits">
        <div class="" id="recette-container"></div>
    </div>
</div>
<script defer>
  
const API_KEY = "2ddd3b02978343c4a2e0eb6525bec221";
const language = "fr-FR"; 
const container = document.getElementById("recette-container");

fetch(`https://api.spoonacular.com/recipes/random?number=6&apiKey=${API_KEY}&lang=${language}`)
  .then(response => {
    if (!response.ok) 
    {
      throw new Error("Erreur de réseau");
    }
    return response.json();
  })
  .then(data => 
  {
    data.recipes.forEach(recipe => 
    {
      const card = document.createElement("div");
      card.className = "card";
      
      // Limiter la description à 150 caractères et ajouter un lien "read more" si nécessaire
      let description = recipe.summary;
      if (description.length > 150) 
      {
        let shortDescription = description.substring(0, 150);
        let lastSpaceIndex = shortDescription.lastIndexOf(" ");
        if (lastSpaceIndex !== -1) {
          shortDescription = shortDescription.substring(0, lastSpaceIndex);
        }
        description = `${shortDescription} <a href="#" class="read-more">read more</a>`;
      }
      
      card.innerHTML = `
        <img src="${recipe.image}" alt="world_food picture_of_${recipe.title} cuisine recette">
        <h2>${recipe.title}</h2>
        <p>${description}</p>
      `;
      container.appendChild(card);
      
      // Ajouter un événement click sur le lien "read more"
      const readMoreLink = card.querySelector(".read-more");
      if (readMoreLink) 
      {
        readMoreLink.addEventListener("click", event => 
        {
          event.preventDefault();
          readMoreLink.remove();
          const fullDescription = recipe.summary;
          card.querySelector("p").innerHTML = fullDescription;
        });
      }
    });
  })
  .catch(error => {
    console.error("Erreur : " + error);
  });
</script>

<?php include '../Footer.php'?>