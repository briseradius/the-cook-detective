<?php include '../Header.php'?>
<div class="text_legale">
    <h1>Mentions légales</h1>
    <p>
    Un site édité par une entreprise ou association (personne morale) se doit de faire figurer plusieurs informations obligatoires :

        Dénomination ou raison sociale,
        Siège social
        Numéro de téléphone,
        Nom du responsable de la rédaction du site
        Nom du directeur de la publication
        Adresse email de contact

        Et s’il y a lieu :

        <h3>Structure juridique</h3>
        Capital social (si structure sociale)
        Numéro de SIREN, TVA et APE

        Dans le cas d'une profession réglementée :
        Référence aux règles professionnelles applicables et au titre professionnel
        Hébergement
        Il est impératif d’ajouter à la page des mentions légales, quel que soit le statut (particulier ou entreprise), les informations relatives 
        à l’hébergement du site :

        Nom de l’hébergeur,
        Raison sociale,
        Adresse
        Numéro de téléphone

        L’hébergement personnel devra être précisé, s’il y a lieu.
        <h3>Obligations liées à la transparence de l'information</h3>

        Coordonnées du délégué à la protection des données de l'organisme ou d’un point de contact pour toute question au sujet de la protection 
        de données personnelles
        Finalité poursuivie par le traitement de données
        Caractère obligatoire ou facultatif des réponses et les conséquences éventuelles pour l’internaute dans le cas d’un défaut de réponse
        Destinataires ou catégories de destinataires des données
        Informations sur les droits d’opposition, d’interrogation, d’accès et de rectification
        Si besoin : les transferts de données envisagés à destination d’un Etat hors Union européenne
        Base juridique du traitement de données (consentement des personnes concernées, respect d’une obligation prévue par un texte, exécution 
        d’un contrat…)
        Mention du droit d’introduire une plainte auprès de la CNIL

        <h3>Politique de protection des données personnelles (RGPD)</h3>

        Depuis le 25 mai 2018, date de l’entrée en vigueur de la nouvelle législation européenne sur la protection des données personnelles (RGPD), il est important de rappeler à l'internaute la possibilité d'exercer ses droits et de consulter la politique de protection des données personnelles en mentionnant l'url de la page "Données personnelles" (ou protection des données personnelles). Toute personne ayant eu des données personnelles collectées sur votre site peut demander à exercer ses droits : droit de rectification, limitation du traitement, portabilité des données, opposition...
        Autres obligations légales
        Respecter le code de la propriété intellectuelle

        Dès qu’il y a création, il y a propriété. Le code de la propriété intellectuelle s’applique à toutes les formes de création de l’esprit 
        tels que les photos, les articles, les illustration etc.
        Si vous souhaitez utilisez des contenus qui ne vous appartiennent pas, demandez l’autorisation à l’auteur et/ou acquittez-vous des droits 
        d’auteur et droits voisins correspondants.
        Pour toute création, les droits sont cédés pour une utilisation précisée au départ. Pour une nouvelle utilisation, par exemple sur un autre support, il vous faudra faire une nouvelle demande.
        Vous pouvez cependant avoir recours à des contenus libres. Il s’agit de créations protégées par le droit d’auteur, mais librement diffusées
        par la volonté de leurs auteurs.
        Respecter le droit à l’image

        Toute personne, quelque soit sa notoriété, dispose d’un droit exclusif sur son image et sur l’utilisation qui en est faite. Elle peut donc 
        s’opposer à sa reproduction et à sa diffusion.

        Parce que l’image d’une personne est une donnée à caractère personnel, les principes de la loi “informatique et libertés” s’appliquent. 
        La diffusion à partir d’un site web, par exemple, de l’image ou de la vidéo d’une personne doit se faire dans le respect des principes 
        protecteurs de la loi du 6 janvier 1978 modifiée. Ces principes rejoignent les garanties issues du droit à l’image. (CNIL)<br>

        ——————————————————————————<br>

        Ces informations vous sont proposées par l’agence référencement ID Interactive, une agence web basée en Bretagne qui vous propose la 
        création de sites internet professionnels mais également le référencement naturel de vos sites.
    </p>
</div>
<?php include '../Footer.php'?>