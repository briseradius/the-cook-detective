<?php
session_start();
include '../Header.php';
include '../bdd.php';

// Récupérer jusqu'à 15 recettes de la base de données dans un ordre aléatoire
$query = $pdo->query("SELECT * FROM recipes WHERE is_verified = 1 ORDER BY RAND() LIMIT 15");
$recipes = $query->fetchAll(PDO::FETCH_ASSOC);
?>

<title>Liste des recettes</title>
<h1>Liste des recettes</h1>

<?php
foreach ($recipes as $recipe) {
    // Afficher les détails de chaque recette
    ?>
    <div class="card">
        <img src="uploads/<?php echo $recipe['pictures']; ?>" alt="Photo de la recette<?php echo $recipe['title']; ?>">
        <h2><?php echo $recipe['title']; ?></h2>
        <h3>Ingrédients :</h3>
        <?php
        // Récupérer les ingrédients de la recette à partir de la table "ingredients"
        $recipeId = $recipe['id'];
        $ingredientsQuery = $pdo->prepare("SELECT i.name, i.quantity, u.unite 
                                         FROM ingredients i 
                                         INNER JOIN unity u ON i.unity_id = u.id 
                                         WHERE i.recipes_id = ?");
        $ingredientsQuery->execute([$recipeId]);
        $ingredients = $ingredientsQuery->fetchAll(PDO::FETCH_ASSOC);
        ?>

        <ul>
            <?php foreach ($ingredients as $ingredient) : ?>
                <li><?php echo $ingredient['quantity'] . ' ' . $ingredient['unite'] . ' ' . $ingredient['name']; ?></li>
            <?php endforeach; ?>
        </ul>

        <a href="view/usersViewRecipes.php?id=<?php echo $recipe['id']; ?>">Consulter</a>
    </div>
<?php
}

include '../Footer.php';
?>
