<?php 
session_start();
include 'Header.php';
?>
<div class="flex row presentation">
    <div class="flex row logo_jeu">
        <img id="indexDetectiveImage" src="image/image-removebg-preview.png" alt="cuisinne detective cook food" class="homeImg">
        <div class="name">
            <h2>The</h2>
            <h2>Cook</h2>
            <h2>Detective</h2>
        </div>
    </div>
    <div class="texte_acceuil">
        <h3>Bonjour et bienvenue sur notre site</h3>
        <p>Vous êtes les bienvenus sur notre site qui propose un large éventail de recettes, avec un système de recherche par ingrédient.</p>
        <p>Nous espérons que vous passerez un agréable moment en explorant nos recettes et que vous n'aurez plus à vous tracasser pour trouver quoi cuisiner.</p>
    </div>
</div>

    <div class="flex row presentationTwo">
        <div class="imageacceuil">
            <img src="image\ingredient.jpeg" alt="cuisine ingredient nouriture food cook"></img>
        </div>
    </div>
</div>
<?php include 'Footer.php'?>
<!-- gerer token attaque csrf -->