-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 19 mai 2023 à 16:50
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tcd`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `contents` text DEFAULT NULL,
  `recipes_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `forum`
--

CREATE TABLE `forum` (
  `id` int(11) NOT NULL,
  `thread` varchar(255) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `IngredientNameEN` varchar(200) NOT NULL,
  `IngredientNameFR` varchar(200) NOT NULL,
  `IngredientSynonymEN` varchar(200) NOT NULL,
  `IngredientSynonymFR` varchar(200) NOT NULL,
  `CategoryEN` varchar(200) NOT NULL,
  `CategoryFR` varchar(200) NOT NULL,
  `ContituentIngredientsEN` varchar(200) NOT NULL,
  `ContituentIngredientsFR` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`id`, `IngredientNameEN`, `IngredientNameFR`, `IngredientSynonymEN`, `IngredientSynonymFR`, `CategoryEN`, `CategoryFR`, `ContituentIngredientsEN`, `ContituentIngredientsFR`) VALUES
(1, 'Egg', 'Œuf', 'egg', 'œuf', 'Meat', 'Viande', 'Egg', 'Œuf\r'),
(2, 'Bread', 'Pain', 'bread; bun', 'pain ; brioche', 'Bakery', 'Boulangerie', 'Bread', 'Pain\r'),
(3, 'Rye Bread', 'Pain de seigle', 'bread-rye', 'pain-rye', 'Bakery', 'Boulangerie', 'Rye Bread', 'Pain de seigle\r'),
(4, 'Wheaten Bread', 'Pain de blé', 'bread-wheaten', 'pain-blé', 'Bakery', 'Boulangerie', 'Wheaten Bread', 'Pain de blé\r'),
(5, 'White Bread', 'Pain blanc', 'bread-white; baguette', 'pain-blanc ; baguette', 'Bakery', 'Boulangerie', 'White Bread', 'Pain blanc\r'),
(6, 'Wholewheat Bread', 'Pain de blé entier', 'bread-whole-wheat', 'pain-entier-blé', 'Bakery', 'Boulangerie', 'Wholewheat Bread', 'Pain de blé entier\r'),
(7, 'Wort', 'Wort', 'wort', 'moût', 'Beverage', 'Boissons', 'Wort', 'Wort\r'),
(8, 'Arrack', 'Arrack', 'arrack; arak', 'arrack ; arak', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Arrack', 'Arrack\r'),
(9, 'Beer', 'Bière', 'beer; lager', 'bière ; lager', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Beer', 'Bière\r'),
(10, 'Bantu Beer', 'Bière bantoue', 'bantu-beer; kaffir-beer', 'bière bantoue ; bière kaffir', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Bantu Beer', 'Bière bantoue\r'),
(11, 'Brandy', 'Brandy', 'brandy', 'eau-de-vie', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Brandy', 'Brandy\r'),
(12, 'Anise Brandy', 'Eau-de-vie d\'anis', 'brandy-anise', 'brandy-anis', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Anise Brandy', 'Eau-de-vie d\'anis\r'),
(13, 'Apple Brandy', 'Brandy de pomme', 'brandy-apple', 'brandy-pomme', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Apple Brandy', 'Brandy de pomme\r'),
(14, 'Armagnac Brandy', 'Brandy d\'armagnac', 'armagnac-brandy', 'armagnac-brandy', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Armagnac Brandy', 'Brandy d\'armagnac\r'),
(15, 'Blackberry Brandy', 'Brandy de mûre', 'blackberry-brandy', 'brandy-mûre', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Blackberry Brandy', 'Brandy de mûre\r'),
(16, 'Cherry Brandy', 'Brandy de cerise', 'brandy-cherry', 'brandy-cerise', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Cherry Brandy', 'Brandy de cerise\r'),
(17, 'Cognac Brandy', 'Cognac Brandy', 'brandy-cognac', 'brandy-cognac', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Cognac Brandy', 'Cognac Brandy\r'),
(18, 'Papaya Brandy', 'Brandy de papaye', 'brandy-papaya', 'brandy-papaye', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Papaya Brandy', 'Brandy de papaye\r'),
(19, 'Pear Brandy', 'Poire Brandy', 'brandy-pear; palinka; poire williams; rakia', 'brandy-poire ; palinka ; poire williams ; rakia', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Pear Brandy', 'Poire Brandy\r'),
(20, 'Plum Brandy', 'Brandy de prune', 'brandy-plum; slivovitz; tuica; ljivovica; asliwowica; schlivowitz', 'brandy-prune ; slivovitz ; tuica ; ljivovica ; asliwowica ; schlivowitz', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Plum Brandy', 'Brandy de prune\r'),
(21, 'Raspberry Brandy', 'Brandy de framboise', 'raspberry-brandy', 'framboise-brandy', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Raspberry Brandy', 'Brandy de framboise\r'),
(22, 'Weinbrand Brandy', 'Brandy Weinbrand', 'weinbrand-brandy', 'weinbrand-brandy', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Weinbrand Brandy', 'Brandy Weinbrand\r'),
(23, 'Gin', 'Gin', 'gin', 'gin', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Gin', 'Gin\r'),
(24, 'Rum', 'Rhum', 'rum', 'rhum', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Rum', 'Rhum\r'),
(25, 'Whisky', 'Whisky', 'whisky; whiskey', 'whisky ; whiskey', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Whisky', 'Whisky\r'),
(26, 'Bourbon Whisky', 'Whisky Bourbon', 'bourbon-whisky; bourbon-whiskey', 'bourbon-whisky ; bourbon-whiskey', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Bourbon Whisky', 'Whisky Bourbon\r'),
(27, 'Canadian Whisky', 'Whisky canadien', 'canadian-whisky; canadian-whiskey', 'canadian-whisky ; canadian-whiskey', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Canadian Whisky', 'Whisky canadien\r'),
(28, 'Finnish Whisky', 'Whisky finlandais', 'finnish-whisky; finnish-whiskey', 'finnish-whisky ; finnish-whiskey', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Finnish Whisky', 'Whisky finlandais\r'),
(29, 'Japanese Whisky', 'Whisky japonais', 'japanese-whisky; japanese-whiskey', 'whisky japonais ; whisky japonais', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Japanese Whisky', 'Whisky japonais\r'),
(30, 'Malt Whisky', 'Whisky de malt', 'whisky-malt; whiskey-malt', 'whisky-malt ; whisky-malt', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Malt Whisky', 'Whisky de malt\r'),
(31, 'Scotch', 'Scotch', 'scotch', 'scotch', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Scotch', 'Scotch\r'),
(32, 'Wine', 'Vin', 'wine', 'vin', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Wine', 'Vin\r'),
(33, 'Bilberry Wine', 'Vin de myrtille', 'bilberry-wine', 'vin de myrtille', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Bilberry Wine', 'Vin de myrtille\r'),
(34, 'Botrytized Wine', 'Vin botrytisé', 'botrytized-wine', 'vin botrytisé', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Botrytized Wine', 'Vin botrytisé\r'),
(35, 'Champagne', 'Champagne', 'champagne', 'champagne', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Champagne', 'Champagne\r'),
(36, 'Cider', 'Cidre', 'cider', 'cidre', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Cider', 'Cidre\r'),
(37, 'Plum Wine', 'Vin de Prune', 'plum-wine', 'vin de prune', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Plum Wine', 'Vin de Prune\r'),
(38, 'Port Wine', 'Vin de Porto', 'port-wine; vinho do porto', 'porto-wine ; vinho do porto', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Port Wine', 'Vin de Porto\r'),
(39, 'Red Wine', 'Vin rouge', 'wine-red', 'vin-rouge', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Red Wine', 'Vin rouge\r'),
(40, 'Rose Wine', 'Vin rosé', 'rose-wine; rosada', 'rose-wine ; rosada', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Rose Wine', 'Vin rosé\r'),
(41, 'Sake', 'Sake', 'sake', 'saké', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Sake', 'Sake\r'),
(42, 'Sherry', 'Sherry', 'sherry', 'sherry', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Sherry', 'Sherry\r'),
(43, 'Sparkling Wine', 'Vin pétillant', 'sparkling-wine', 'vin mousseux', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Sparkling Wine', 'Vin pétillant\r'),
(44, 'Strawberry Wine', 'Vin de fraise', 'strawberry-wine', 'vin de fraise', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Strawberry Wine', 'Vin de fraise\r'),
(45, 'White Wine', 'Vin blanc', 'wine-white', 'vin-blanc', 'Beverage Alcoholic', 'Boissons alcoolisées', 'White Wine', 'Vin blanc\r'),
(46, 'Coffee', 'Café', 'coffee', 'café', 'Beverage', 'Boisson', 'Coffee', 'Café\r'),
(47, 'Mate', 'Mate', 'mate; yerba', 'maté ; yerba', 'Beverage', 'Boisson', 'Mate', 'Mate\r'),
(48, 'Black Tea', 'Thé noir', 'tea-black', 'thé-noir', 'Beverage', 'Boisson', 'Black Tea', 'Thé noir\r'),
(49, 'Green Tea', 'Thé vert', 'tea-green', 'thé-vert', 'Beverage', 'Boisson', 'Green Tea', 'Thé vert\r'),
(50, 'Roibos Tea', 'Thé Roibos', 'tea-rooibos; tea-bush', 'thé-rooibos ; thé-bush', 'Beverage', 'Boisson', 'Roibos Tea', 'Thé Roibos\r'),
(51, 'Barley', 'Orge', 'barley', 'orge', 'Cereal', 'Céréales', 'Barley', 'Orge\r'),
(52, 'Crispbread', 'Pain croustillant', 'crisp-bread', 'croustillant-pain', 'Cereal', 'Céréales', 'Crispbread', 'Pain croustillant\r'),
(53, 'Malt', 'Malt', 'malt', 'malt', 'Cereal', 'Céréales', 'Malt', 'Malt\r'),
(54, 'Oats', 'Avoine', 'oat', 'avoine', 'Cereal', 'Céréales', 'Oats', 'Avoine\r'),
(55, 'Rice', 'Riz', 'rice', 'riz', 'Cereal', 'Céréales', 'Rice', 'Riz\r'),
(56, 'Corn', 'Maïs', 'corn; corn-flour; corn-starch; maize-flour', 'maïs ; maïs-farine ; maïs-amidon ; maïs-farine', 'Maize', 'Maïs', 'Corn', 'Maïs\r'),
(57, 'Corn Oil', 'Huile de maïs', 'corn oil', 'huile de maïs', 'Maize', 'Maïs', 'Corn Oil', 'Huile de maïs\r'),
(58, 'Popcorn', 'Popcorn', 'pop-corn', 'pop-maïs', 'Maize', 'Maïs', 'Popcorn', 'Popcorn\r'),
(59, 'Sweetcorn', 'Maïs doux', 'sweet-corn', 'sucré-maïs', 'Maize', 'Maïs', 'Sweetcorn', 'Maïs doux\r'),
(60, 'Butter', 'Beurre', 'butter', 'beurre', 'Dairy', 'Produits laitiers', 'Butter', 'Beurre\r'),
(61, 'Buttermilk', 'Babeurre', 'buttermilk', 'babeurre', 'Dairy', 'Produits laitiers', 'Buttermilk', 'Babeurre\r'),
(62, 'Cheese', 'Fromage', 'cheese', 'fromage', 'Dairy', 'Produits laitiers', 'Cheese', 'Fromage\r'),
(63, 'Blue Cheese', 'Fromage bleu', 'cheese-blue', 'fromage-bleu', 'Dairy', 'Produits laitiers', 'Blue Cheese', 'Fromage bleu\r'),
(64, 'Camembert Cheese', 'Fromage Camembert', 'cheese-camembert', 'fromage-camembert', 'Dairy', 'Produits laitiers', 'Camembert Cheese', 'Fromage Camembert\r'),
(65, 'Cheddar Cheese', 'Fromage Cheddar', 'cheese-cheddar', 'fromage-cheddar', 'Dairy', 'Produits laitiers', 'Cheddar Cheese', 'Fromage Cheddar\r'),
(66, 'Comte Cheese', 'Fromage Comte', 'cheese-comte', 'fromage-comte', 'Dairy', 'Produits laitiers', 'Comte Cheese', 'Fromage Comte\r'),
(67, 'Cottage Cheese', 'Fromage blanc', 'cheese-cottage', 'fromage-cottage', 'Dairy', 'Produits laitiers', 'Cottage Cheese', 'Fromage blanc\r'),
(68, 'Cream Cheese', 'Fromage à la crème', 'cheese-cream', 'fromage-crème', 'Dairy', 'Produits laitiers', 'Cream Cheese', 'Fromage à la crème\r'),
(69, 'Domiati Cheese', 'Fromage Domiati', 'cheese-white; cheese-domiati', 'fromage-blanc ; fromage-domiati', 'Dairy', 'Produits laitiers', 'Domiati Cheese', 'Fromage Domiati\r'),
(70, 'Emmental Cheese', 'Fromage Emmental', 'cheese-emmental', 'fromage-emmental', 'Dairy', 'Produits laitiers', 'Emmental Cheese', 'Fromage Emmental\r'),
(71, 'Feta Cheese', 'Fromage Feta', 'cheese-feta', 'fromage-feta', 'Dairy', 'Produits laitiers', 'Feta Cheese', 'Fromage Feta\r'),
(72, 'Goat Cheese', 'Fromage de chèvre', 'cheese-goat', 'fromage-chèvre', 'Dairy', 'Produits laitiers', 'Goat Cheese', 'Fromage de chèvre\r'),
(73, 'Gruyere Cheese', 'Fromage Gruyère', 'cheese-gruyere', 'fromage-gruyère', 'Dairy', 'Produits laitiers', 'Gruyere Cheese', 'Fromage Gruyère\r'),
(74, 'Limburger Cheese', 'Fromage Limburger', 'cheese-limburger', 'fromage-limburger', 'Dairy', 'Produits laitiers', 'Limburger Cheese', 'Fromage Limburger\r'),
(75, 'Mozzarella Cheese', 'Fromage Mozzarella', 'cheese-mozzarella', 'fromage-mozzarella', 'Dairy', 'Produits laitiers', 'Mozzarella Cheese', 'Fromage Mozzarella\r'),
(76, 'Munster Cheese', 'Fromage Munster', 'cheese-munster', 'fromage-munster', 'Dairy', 'Produits laitiers', 'Munster Cheese', 'Fromage Munster\r'),
(78, 'Parmesan Cheese', 'Fromage Parmesan', 'cheese-parmesan', 'fromage-parmesan', 'Dairy', 'Produits laitiers', 'Parmesan Cheese', 'Fromage Parmesan\r'),
(79, 'Provolone Cheese', 'Fromage Provolone', 'cheese-provolone', 'fromage-provolone', 'Dairy', 'Produits laitiers', 'Provolone Cheese', 'Fromage Provolone\r'),
(80, 'Romano Cheese', 'Fromage Romano', 'cheese-romano', 'fromage-romano', 'Dairy', 'Produits laitiers', 'Romano Cheese', 'Fromage Romano\r'),
(81, 'Roquefort Cheese', 'Fromage Roquefort', 'cheese-roquefort', 'fromage-roquefort', 'Dairy', 'Produits laitiers', 'Roquefort Cheese', 'Fromage Roquefort\r'),
(82, 'Russian Cheese', 'Fromage russe', 'cheese-russia', 'fromage-russe', 'Dairy', 'Produits laitiers', 'Russian Cheese', 'Fromage russe\r'),
(83, 'Sheep Cheese', 'Fromage de brebis', 'cheese-sheep', 'fromage-mouton', 'Dairy', 'Produits laitiers', 'Sheep Cheese', 'Fromage de brebis\r'),
(84, 'Swiss Cheese', 'Fromage suisse', 'cheese-swiss', 'fromage-suisse', 'Dairy', 'Produits laitiers', 'Swiss Cheese', 'Fromage suisse\r'),
(85, 'Tilsit Cheese', 'Fromage Tilsit', 'cheese-tilsit', 'fromage-tilsit', 'Dairy', 'Produits laitiers', 'Tilsit Cheese', 'Fromage Tilsit\r'),
(87, 'Ghee', 'Ghee', 'ghee', 'ghee', 'Dairy', 'Produits laitiers', 'Ghee', 'Ghee\r'),
(88, 'Milk', 'Lait', 'milk', 'lait', 'Dairy', 'Produits laitiers', 'Milk', 'Lait\r'),
(89, 'Milk Fat', 'Graisse de lait', 'milk-fat; cream', 'lait-gras ; crème', 'Dairy', 'Produits laitiers', 'Milk Fat', 'Graisse de lait\r'),
(90, 'Goat Milk', 'Lait de chèvre', 'goat-milk', 'lait de chèvre', 'Dairy', 'Produits laitiers', 'Goat Milk', 'Lait de chèvre\r'),
(91, 'Milk Powder', 'Poudre de lait', 'milk-powder', 'lait-poudre', 'Dairy', 'Produits laitiers', 'Milk Powder', 'Poudre de lait\r'),
(92, 'Sheep Milk', 'Lait de Mouton', 'sheep-milk', 'lait de brebis', 'Dairy', 'Produits laitiers', 'Sheep Milk', 'Lait de Mouton\r'),
(93, 'Skimmed Milk', 'Lait écrémé', 'milk-skimmed', 'lait écrémé', 'Dairy', 'Produits laitiers', 'Skimmed Milk', 'Lait écrémé\r'),
(94, 'Yogurt', 'Yogourt', 'yogurt; curd; yoghurt', 'yaourt ; caillé ; yaourt', 'Dairy', 'Produits laitiers', 'Yogurt', 'Yogourt\r'),
(95, 'Achilleas', 'Achilleas', 'achillea', 'achillea', 'Essential Oil', 'Huile essentielle', 'Achilleas', 'Achilleas\r'),
(96, 'Arar', 'Arar', 'arar', 'arar', 'Essential Oil', 'Huile essentielle', 'Arar', 'Arar\r'),
(97, 'Buchu', 'Buchu', 'buchu', 'buchu', 'Essential Oil', 'Huile essentielle', 'Buchu', 'Buchu\r'),
(98, 'Cajeput', 'Cajeput', 'cajeput', 'cajeput', 'Essential Oil', 'Huile essentielle', 'Cajeput', 'Cajeput\r'),
(99, 'Camphor', 'Camphre', 'camphor', 'camphre', 'Essential Oil', 'Huile essentielle', 'Camphor', 'Camphre\r'),
(100, 'Cascarilla', 'Cascarilla', 'cascarilla', 'cascarilla', 'Essential Oil', 'Huile essentielle', 'Cascarilla', 'Cascarilla\r'),
(101, 'Cedar', 'Cèdre', 'cedar', 'cèdre', 'Essential Oil', 'Huile essentielle', 'Cedar', 'Cèdre\r'),
(102, 'Chamomile', 'Camomille', 'chamomile; camomile', 'camomille ; camomille', 'Essential Oil', 'Huile essentielle', 'Chamomile', 'Camomille\r'),
(103, 'Citronella Oil', 'Huile de citronnelle', 'citronella-oil', 'huile de citronnelle', 'Essential Oil', 'Huile essentielle', 'Citronella Oil', 'Huile de citronnelle\r'),
(104, 'Citrus Peel Oil', 'Huile de zeste d\'agrumes', 'citrus-peel-oil', 'huile d\'écorce d\'agrume', 'Essential Oil', 'Huile essentielle', 'Citrus Peel Oil', 'Huile de zeste d\'agrumes\r'),
(105, 'Eucalyptus Oil', 'Huile d\'Eucalyptus', 'eucalyptus-oil', 'huile d\'eucalyptus', 'Essential Oil', 'Huile essentielle', 'Eucalyptus Oil', 'Huile d\'Eucalyptus\r'),
(106, 'Fir', 'Sapin', 'fir', 'sapin', 'Essential Oil', 'Huile essentielle', 'Fir', 'Sapin\r'),
(107, 'Geranium', 'Géranium', 'geranium', 'géranium', 'Essential Oil', 'Huile essentielle', 'Geranium', 'Géranium\r'),
(108, 'Grapefruit Peel Oil', 'Huile d\'écorce de pamplemousse', 'grapefruit-peel-oil', 'pamplemousse-écorce-huile', 'Essential Oil', 'Huile essentielle', 'Grapefruit Peel Oil', 'Huile d\'écorce de pamplemousse\r'),
(109, 'Grass', 'Herbe', 'grass', 'herbe', 'Essential Oil', 'Huile essentielle', 'Grass', 'Herbe\r'),
(110, 'Hops Oil', 'Huile de houblon', 'hops-oil', 'houblon-huile', 'Essential Oil', 'Huile essentielle', 'Hops Oil', 'Huile de houblon\r'),
(111, 'Hyacinth', 'Hyacinthe', 'hyacinth', 'jacinthe', 'Essential Oil', 'Huile essentielle', 'Hyacinth', 'Hyacinthe\r'),
(112, 'Hyssop Oil', 'Huile d\'hysope', 'hyssop-oil', 'huile d\'hysope', 'Essential Oil', 'Huile essentielle', 'Hyssop Oil', 'Huile d\'hysope\r'),
(113, 'Lemon Grass', 'Lemon Grass', 'lemon-grass', 'citron-herbe', 'Essential Oil', 'Huile essentielle', 'Lemon Grass', 'Lemon Grass\r'),
(114, 'Lemon Peel Oil', 'Huile d\'écorce de citron', 'lemon-peel-oil', 'citron-peel-huile', 'Essential Oil', 'Huile essentielle', 'Lemon Peel Oil', 'Huile d\'écorce de citron\r'),
(115, 'Lime Peel Oil', 'Huile d\'écorce de citron vert', 'lime-peel-oil', 'citron-peel-huile', 'Essential Oil', 'Huile essentielle', 'Lime Peel Oil', 'Huile d\'écorce de citron vert\r'),
(116, 'Lovage', 'Livèche', 'lovage', 'livèche', 'Essential Oil', 'Huile essentielle', 'Lovage', 'Livèche\r'),
(117, 'Mandarin Orange Peel Oil', 'Huile d\'écorce de mandarine', 'mandarin-peel-oil', 'mandarine-peel-huile', 'Essential Oil', 'Huile essentielle', 'Mandarin Orange Peel Oil', 'Huile d\'écorce de mandarine\r'),
(118, 'Mastic Gum', 'Mastic Gum', 'mastic', 'lentisque', 'Essential Oil', 'Huile essentielle', 'Mastic Gum', 'Mastic Gum\r'),
(119, 'Mentha Oil', 'Huile de menthe', 'mentha-oil', 'huile de menthe', 'Essential Oil', 'Huile essentielle', 'Mentha Oil', 'Huile de menthe\r'),
(120, 'Myrrh', 'Myrrhe', 'myrrh', 'myrrhe', 'Essential Oil', 'Huile essentielle', 'Myrrh', 'Myrrhe\r'),
(121, 'Neroli Oil', 'Huile de Néroli', 'neroli-oil', 'huile de néroli', 'Essential Oil', 'Huile essentielle', 'Neroli Oil', 'Huile de Néroli\r'),
(122, 'Orange Oil', 'Huile d\'orange', 'orange-oil', 'huile d\'orange', 'Essential Oil', 'Huile essentielle', 'Orange Oil', 'Huile d\'orange\r'),
(123, 'Orris', 'Orris', 'orris', 'orris', 'Essential Oil', 'Huile essentielle', 'Orris', 'Orris\r'),
(124, 'Clary Sage', 'Sauge sclarée', 'clary-sage', 'sauge sclarée', 'Essential Oil', 'Huile essentielle', 'Clary Sage', 'Sauge sclarée\r'),
(125, 'Red Sage', 'Sauge rouge', 'sage-red; sage-wild', 'sauge-rouge ; sauge-sauvage', 'Essential Oil', 'Huile essentielle', 'Red Sage', 'Sauge rouge\r'),
(126, 'Spanish Sage', 'Sauge espagnole', 'sage-spanish', 'sauge-espagnole', 'Essential Oil', 'Huile essentielle', 'Spanish Sage', 'Sauge espagnole\r'),
(127, 'Sandalwood', 'Bois de santal', 'sandalwood', 'bois de santal', 'Essential Oil', 'Huile essentielle', 'Sandalwood', 'Bois de santal\r'),
(128, 'Sea Buckthorns', 'Argousier', 'sea-buckthorn', 'mer-épicorne', 'Fruit', 'Fruit', 'Sea Buckthorns', 'Argousier\r'),
(129, 'Sweet Grass', 'Herbe douce', 'sweet-grass', 'doux-herbe', 'Essential Oil', 'Huile essentielle', 'Sweet Grass', 'Herbe douce\r'),
(130, 'Valerian', 'Valériane', 'valerian', 'valériane', 'Essential Oil', 'Huile essentielle', 'Valerian', 'Valériane\r'),
(131, 'Wattle', 'Acacia', 'wattle', 'acacia', 'Essential Oil', 'Huile essentielle', 'Wattle', 'Acacia\r'),
(132, 'Yarrow', 'Achillée millefeuille', 'yarrow', 'achillée millefeuille', 'Essential Oil', 'Huile essentielle', 'Yarrow', 'Achillée millefeuille\r'),
(133, 'Ylang-Ylang', 'Ylang-Ylang', 'ylang-ylang-oil', 'ylang-huile d\'ylang', 'Essential Oil', 'Huile essentielle', 'Ylang-Ylang', 'Ylang-Ylang\r'),
(134, 'Clam', 'Palourde', 'clam', 'palourde', 'Seafood', 'Fruits de mer', 'Clam', 'Palourde\r'),
(135, 'Crab', 'Crabe', 'crab', 'crabe', 'Seafood', 'Fruits de mer', 'Crab', 'Crabe\r'),
(136, 'Crayfish', 'Écrevisse', 'cray-fish', 'crayons-poisson', 'Seafood', 'Fruits de mer', 'Crayfish', 'Écrevisse\r'),
(137, 'Kelp', 'Varech', 'kelp', 'varech', 'Seafood', 'Fruits de mer', 'Kelp', 'Varech\r'),
(138, 'Krill', 'Krill', 'krill', 'krill', 'Seafood', 'Fruits de mer', 'Krill', 'Krill\r'),
(139, 'Lobster', 'Homard', '#lobster#', '#homard#', 'Seafood', 'Fruits de mer', 'Lobster', 'Homard\r'),
(140, 'Mollusc', 'Mollusque', 'mollusc; mussels', 'mollusque ; moules', 'Seafood', 'Fruits de mer', 'Mollusc', 'Mollusque\r'),
(141, 'Oyster', 'Huître', 'oyster', 'huître', 'Seafood', 'Fruits de mer', 'Oyster', 'Huître\r'),
(142, 'Prawn', 'Crevette', 'prawn', 'crevette', 'Seafood', 'Fruits de mer', 'Prawn', 'Crevette\r'),
(143, 'Scallop', 'Coquille Saint-Jacques', 'scallop', 'coquille Saint-Jacques', 'Seafood', 'Fruits de mer', 'Scallop', 'Coquille Saint-Jacques\r'),
(144, 'Shellfish', 'Mollusques et crustacés', 'shellfish', 'coquillage', 'Seafood', 'Fruits de mer', 'Shellfish', 'Mollusques et crustacés\r'),
(145, 'Shrimp', 'Crevette', 'shrimp', 'crevette', 'Seafood', 'Fruits de mer', 'Shrimp', 'Crevette\r'),
(146, 'Trassi', 'Trassi', 'shrimp-paste-dried; trassi', 'pâte de crevettes-séché ; trassi', 'Seafood', 'Fruits de mer', 'Trassi', 'Trassi\r'),
(147, 'Squid', 'Calmar', 'squid', 'calmar', 'Seafood', 'Fruits de mer', 'Squid', 'Calmar\r'),
(148, 'Bonito', 'Bonito', 'bonito', 'bonite', 'Fish', 'Poisson', 'Bonito', 'Bonito\r'),
(149, 'Caviar', 'Caviar', 'caviar', 'caviar', 'Fish', 'Poisson', 'Caviar', 'Caviar\r'),
(150, 'Codfish', 'Morue', 'codfish; cod', 'morue ; cabillaud', 'Fish', 'Poisson', 'Codfish', 'Morue\r'),
(151, 'Fish', 'Poisson', 'fish; pomfret', 'poisson ; pomfret', 'Fish', 'Poisson', 'Fish', 'Poisson\r'),
(152, 'Fatty Fish', 'Poisson gras', 'fish-fatty', 'poisson gras', 'Fish', 'Poisson', 'Fatty Fish', 'Poisson gras\r'),
(153, 'Lean Fish', 'Poisson maigre', 'fish-lean', 'poisson maigre', 'Fish', 'Poisson', 'Lean Fish', 'Poisson maigre\r'),
(154, 'Fish Oil', 'Huile de poisson', 'fish-oil', 'huile de poisson', 'Fish', 'Poisson', 'Fish Oil', 'Huile de poisson\r'),
(155, 'Smoked Fish', 'Poisson fumé', 'fish-smoked', 'poisson-fumé', 'Fish', 'Poisson', 'Smoked Fish', 'Poisson fumé\r'),
(156, 'Salmon', 'Saumon', '#salmon#', '#saumon', 'Fish', 'Poisson', 'Salmon', 'Saumon\r'),
(157, 'Artichoke', 'Artichauts', 'artichoke', 'artichaut', 'Flower', 'Fleur', 'Artichoke', 'Artichauts\r'),
(158, 'Champaca', 'Champaca', 'champaca', 'champaca', 'Flower', 'Fleur', 'Champaca', 'Champaca\r'),
(159, 'Jasmine', 'Jasmin', 'jasmine', 'jasmin', 'Flower', 'Fleur', 'Jasmine', 'Jasmin\r'),
(160, 'Lavendar', 'Lavande', 'lavendar', 'lavande', 'Flower', 'Fleur', 'Lavendar', 'Lavande\r'),
(161, 'Rose', 'Rose', 'rose', 'rose', 'Flower', 'Fleur', 'Rose', 'Rose\r'),
(162, 'Apple', 'Pomme', 'apple', 'pomme', 'Fruit', 'Fruits', 'Apple', 'Pomme\r'),
(163, 'Apple Sauce', 'Sauce aux pommes', '#apple-sauce#', '#pomme-sauce#', 'Fruit', 'Fruits', 'Apple Sauce', 'Sauce aux pommes\r'),
(164, 'Apricot', 'Abricot', 'apricot', 'abricot', 'Fruit', 'Fruits', 'Apricot', 'Abricot\r'),
(165, 'Avocado', 'Avocat', 'avocado', 'avocat', 'Fruit', 'Fruits', 'Avocado', 'Avocat\r'),
(166, 'Babaco', 'Babaco', 'babaco', 'babaco', 'Fruit', 'Fruits', 'Babaco', 'Babaco\r'),
(167, 'Banana', 'Banane', 'banana', 'banane', 'Fruit', 'Fruits', 'Banana', 'Banane\r'),
(168, 'Beli', 'Beli', 'beli', 'beli', 'Fruit', 'Fruits', 'Beli', 'Beli\r'),
(169, 'Byrsonima crassifolia', 'Byrsonima crassifolia', 'byrsonima crassifolia; changunga; nanche; serette', 'byrsonima crassifolia ; changunga ; nanche ; serette', 'Fruit', 'Fruits', 'Byrsonima crassifolia', 'Byrsonima crassifolia\r'),
(170, 'Cashew Apple', 'Pomme de cajou', 'cashew-apple', 'noix de cajou-pomme', 'Fruit', 'Fruits', 'Cashew Apple', 'Pomme de cajou\r'),
(171, 'Cherimoya', 'Cherimoya', 'cherimoya; custard apple', 'cherimoya ; pomme de garde', 'Fruit', 'Fruits', 'Cherimoya', 'Cherimoya\r'),
(172, 'Coconut', 'Noix de coco', 'coconut', 'noix de coco', 'Fruit', 'Fruits', 'Coconut', 'Noix de coco\r'),
(173, 'Currant', 'Cassis', 'currant', 'groseille', 'Fruit', 'Fruits', 'Currant', 'Cassis\r'),
(174, 'Black Currant', 'Groseille noire', 'currant-black', 'groseille noire', 'Fruit', 'Fruits', 'Black Currant', 'Groseille noire\r'),
(175, 'Red Currant', 'Groseille rouge', 'currant-red', 'groseille-rouge', 'Fruit', 'Fruits', 'Red Currant', 'Groseille rouge\r'),
(176, 'White Currant', 'Groseille blanche', 'currant-white', 'groseille-blanche', 'Fruit', 'Fruits', 'White Currant', 'Groseille blanche\r'),
(177, 'Dates', 'Dattes', 'date', 'datte', 'Fruit', 'Fruits', 'Dates', 'Dattes\r'),
(178, 'Durian', 'Durian', 'durian', 'durian', 'Fruit', 'Fruits', 'Durian', 'Durian\r'),
(179, 'Elderberry', 'Sureau', 'elder-berry', 'sureau-mûre', 'Fruit', 'Fruits', 'Elderberry', 'Sureau\r'),
(180, 'Feijoa', 'Feijoa', 'feijoa', 'feijoa', 'Fruit', 'Fruits', 'Feijoa', 'Feijoa\r'),
(181, 'Fig', 'Figue', 'fig', 'figue', 'Fruit', 'Fruits', 'Fig', 'Figue\r'),
(182, 'Grape', 'Raisin', 'grape; merlot', 'raisin ; merlot', 'Fruit', 'Fruits', 'Grape', 'Raisin\r'),
(183, 'Guava', 'Goyave', 'guava', 'goyave', 'Fruit', 'Fruits', 'Guava', 'Goyave\r'),
(184, 'Hogplum', 'Hogplum', 'hogplum', 'hogplum', 'Fruit', 'Fruits', 'Hogplum', 'Hogplum\r'),
(185, 'Jackfruit', 'Fruit du jacquier', 'jackfruit', 'jacquier', 'Fruit', 'Fruits', 'Jackfruit', 'Fruit du jacquier\r'),
(186, 'Kiwifruit', 'Kiwi', 'kiwi; kiwifruit', 'kiwi ; kiwi-fruit', 'Fruit', 'Fruits', 'Kiwifruit', 'Kiwi\r'),
(187, 'Litchi', 'Litchi', 'litchi; lychee; lichee', 'litchi ; litchi ; litchi', 'Fruit', 'Fruits', 'Litchi', 'Litchi\r'),
(188, 'Loquat', 'Loquat', 'loquat', 'loquat', 'Fruit', 'Fruits', 'Loquat', 'Loquat\r'),
(189, 'Malay Apple', 'Pomme malaise', 'malay-apple', 'malay-pomme', 'Fruit', 'Fruits', 'Malay Apple', 'Pomme malaise\r'),
(190, 'Mango', 'Mangue', 'mango; amchoor; amchur', 'mangue ; amchoor ; amchur', 'Fruit', 'Fruits', 'Mango', 'Mangue\r'),
(191, 'Melon', 'Melon', 'melon', 'melon', 'Fruit', 'Fruits', 'Melon', 'Melon\r'),
(192, 'Musk Melon', 'Melon musqué', 'musk-melon; honeydew', 'musc-melon ; miellat', 'Fruit', 'Fruits', 'Musk Melon', 'Melon musqué\r'),
(193, 'Naranjilla', 'Naranjilla', 'naranjilla', 'naranjilla', 'Fruit', 'Fruits', 'Naranjilla', 'Naranjilla\r'),
(194, 'Orange', 'Orange', 'orange', 'orange', 'Fruit', 'Fruits', 'Orange', 'Orange\r'),
(195, 'Bitter Orange', 'Orange amère', 'orange-bitter; orange-sour', 'orange-amère ; orange-aigre', 'Fruit', 'Fruits', 'Bitter Orange', 'Orange amère\r'),
(196, 'Papaya', 'Papaye', 'papaya', 'papaye', 'Fruit', 'Fruits', 'Papaya', 'Papaye\r'),
(197, 'Mountain Papaya', 'Papaye de montagne', 'Papaya-mountain', 'Papaye-montagne', 'Fruit', 'Fruits', 'Mountain Papaya', 'Papaye de montagne\r'),
(198, 'Passionfruit', 'Fruit de la passion', 'passionfruit', 'fruit de la passion', 'Fruit', 'Fruits', 'Passionfruit', 'Fruit de la passion\r'),
(199, 'Yellow Passionfruit', 'Fruit de la passion jaune', 'passionfruit-yellow', 'fruit de la passion-jaune', 'Fruit', 'Fruits', 'Yellow Passionfruit', 'Fruit de la passion jaune\r'),
(200, 'Pawpaw', 'Pawpaw', 'pawpaw', 'papaye', 'Fruit', 'Fruits', 'Pawpaw', 'Pawpaw\r'),
(201, 'Peach', 'Pêche', 'peach; nectarine', 'pêche ; nectarine', 'Fruit', 'Fruits', 'Peach', 'Pêche\r'),
(202, 'Pear', 'Poire', 'pear', 'poire', 'Fruit', 'Fruits', 'Pear', 'Poire\r'),
(203, 'Bartlett Pear', 'Poire Bartlett', 'pear-barlett', 'poire-barlett', 'Fruit', 'Fruits', 'Bartlett Pear', 'Poire Bartlett\r'),
(204, 'Prickly Pear', 'Figue de Barbarie', 'pear-prickly', 'poire à épines', 'Fruit', 'Fruits', 'Prickly Pear', 'Figue de Barbarie\r'),
(205, 'Pepino', 'Pepino', 'pepino; cucumber-sweet', 'pepino ; concombre-sucré', 'Fruit', 'Fruits', 'Pepino', 'Pepino\r'),
(206, 'Pineapple', 'Ananas', 'pineapple; ananas', 'ananas ; ananas', 'Fruit', 'Fruits', 'Pineapple', 'Ananas\r'),
(207, 'Plum', 'Prune', 'plum; prune', 'prune ; pruneau', 'Fruit', 'Fruits', 'Plum', 'Prune\r'),
(208, 'Plumcot', 'Plumcot', 'plumcot; aprium', 'plumcot ; aprium', 'Fruit', 'Fruits', 'Plumcot', 'Plumcot\r'),
(209, 'Pumpkin', 'Citrouille', 'pumpkin', 'citrouille ;', 'Fruit', 'Fruits', 'Pumpkin', 'Citrouille\r'),
(210, 'Quince', 'Coing', 'quince', 'coing', 'Fruit', 'Fruits', 'Quince', 'Coing\r'),
(211, 'Chinese Quince', 'Coing chinois', 'quince-chinese', 'coing-chinois', 'Fruit', 'Fruits', 'Chinese Quince', 'Coing chinois\r'),
(212, 'Raisin', 'Raisin', 'raisin; sultana', 'raisin sec ; sultanine', 'Fruit', 'Fruits', 'Raisin', 'Raisin\r'),
(213, 'Roseapple', 'Ananas', 'rose-apple; java-apple', 'rose-pomme ; java-pomme', 'Fruit', 'Fruits', 'Roseapple', 'Ananas\r'),
(214, 'Sapodilla', 'Sapodilla', 'sapodilla', 'sapodilla', 'Fruit', 'Fruits', 'Sapodilla', 'Sapodilla\r'),
(215, 'Soursop', 'Soursop', 'soursop', 'corossol', 'Fruit', 'Fruits', 'Soursop', 'Soursop\r'),
(216, 'Spineless Monkey Orange', 'Orange de singe sans épines', 'spineless-monkey-orange', 'spineless-monkey-orange', 'Fruit', 'Fruits', 'Spineless Monkey Orange', 'Orange de singe sans épines\r'),
(217, 'Starfruit', 'Starfruit', 'starfruit; carambola', 'carambole ; carambole', 'Fruit', 'Fruits', 'Starfruit', 'Starfruit\r'),
(218, 'Tamarind', 'Tamarinier', 'tamarind', 'tamarin', 'Fruit', 'Fruits', 'Tamarind', 'Tamarinier\r'),
(219, 'Woodapple', 'Ananas des bois', 'woodapple', 'ananas des bois', 'Fruit', 'Fruits', 'Woodapple', 'Ananas des bois\r'),
(221, 'Bilberry', 'Myrtille', 'bilberry', 'myrtille', 'Fruit', 'Fruits', 'Bilberry', 'Myrtille\r'),
(222, 'Blackberry', 'Mûre', 'blackberry', 'mûre', 'Fruit', 'Fruits', 'Blackberry', 'Mûre\r'),
(223, 'Blueberry', 'Myrtille', 'blueberry', 'myrtille', 'Fruit', 'Fruits', 'Blueberry', 'Myrtille\r'),
(224, 'Cherry', 'Cerise', 'cherry', 'cerise', 'Fruit', 'Fruits', 'Cherry', 'Cerise\r'),
(225, 'Bitter Cherry', 'Cerise amère', 'cherry-choke; cherry-bitter', 'cerise-framboise ; cerise-amère', 'Fruit', 'Fruits', 'Bitter Cherry', 'Cerise amère\r'),
(226, 'Sour Cherry', 'Cerise acide', 'cherry-morello; cherry-sour', 'cerise-morello ; cerise-aigre', 'Fruit', 'Fruits', 'Sour Cherry', 'Cerise acide\r'),
(227, 'Sweet Cherry', 'Cerise douce', 'cherry-sweet; cherry-wild', 'cerise-sucrée ; cerise-sauvage', 'Fruit', 'Fruits', 'Sweet Cherry', 'Cerise douce\r'),
(228, 'Cloudberry', 'Myrtille', 'cloudberry', 'mûre des marais', 'Fruit', 'Fruits', 'Cloudberry', 'Myrtille\r'),
(229, 'Cranberry', 'Canneberge', 'cranberry', 'canneberge', 'Fruit', 'Fruits', 'Cranberry', 'Canneberge\r'),
(230, 'Gooseberry', 'Groseille à maquereau', 'gooseberry', 'groseille à maquereau', 'Fruit', 'Fruits', 'Gooseberry', 'Groseille à maquereau\r'),
(231, 'Lingonberry', 'Lingonberry', 'lingonberry', 'airelle rouge', 'Fruit', 'Fruits', 'Lingonberry', 'Lingonberry\r'),
(232, 'Loganberry', 'Mûre de ronce', 'loganberry', 'mûre de Logan', 'Fruit', 'Fruits', 'Loganberry', 'Mûre de ronce\r'),
(233, 'Raspberry', 'Framboise', 'raspberry', 'framboise', 'Fruit', 'Fruits', 'Raspberry', 'Framboise\r'),
(234, 'Strawberry', 'Fraise', 'strawberry', 'fraise', 'Fruit', 'Fruits', 'Strawberry', 'Fraise\r'),
(235, 'Strawberry Jam', 'Confiture de fraises', 'strawberry-jam', 'confiture de fraises', 'Fruit', 'Fruits', 'Strawberry Jam', 'Confiture de fraises\r'),
(236, 'Bergamot', 'Bergamote', 'bergamot', 'bergamote', 'Fruit', 'Fruits', 'Bergamot', 'Bergamote\r'),
(237, 'Citrus Fruits', 'Agrumes', 'citrus', 'agrume', 'Fruit', 'Fruits', 'Citrus Fruits', 'Agrumes\r'),
(238, 'Grapefruit', 'Pamplemousse', 'grapefruit', 'pamplemousse', 'Fruit', 'Fruits', 'Grapefruit', 'Pamplemousse\r'),
(239, 'Kumquat', 'Kumquat', 'kumquat', 'kumquat', 'Fruit', 'Fruits', 'Kumquat', 'Kumquat\r'),
(240, 'Lemon', 'Citron', 'lemon', 'citron', 'Fruit', 'Fruits', 'Lemon', 'Citron\r'),
(241, 'Lime', 'Citron vert', 'lime', 'citron vert', 'Fruit', 'Fruits', 'Lime', 'Citron vert\r'),
(242, 'Mandarin Orange', 'Mandarine Orange', 'mandarin', 'mandarine', 'Fruit', 'Fruits', 'Mandarin Orange', 'Mandarine Orange\r'),
(243, 'Satsuma Orange', 'Orange Satsuma', 'mandarin-satsuma', 'mandarine-satsuma', 'Fruit', 'Fruits', 'Satsuma Orange', 'Orange Satsuma\r'),
(244, 'Tangerine', 'Mandarine', 'tangerine', 'mandarine', 'Fruit', 'Fruits', 'Tangerine', 'Mandarine\r'),
(245, 'Vanilla', 'Vanille', 'vanilla', 'vanille', 'Fruit', 'Fruits', 'Vanilla', 'Vanille\r'),
(246, 'Mushroom', 'Champignon', 'mushroom; porcini', 'champignon ; porcini', 'Fungus', 'Champignons', 'Mushroom', 'Champignon\r'),
(247, 'Truffle', 'Truffe', 'truffle', 'truffe', 'Fungus', 'Champignons', 'Truffle', 'Truffe\r'),
(248, 'Angelica', 'Angélique', 'angelica', 'angélique', 'Herb', 'Herb', 'Angelica', 'Angélique\r'),
(249, 'Artemisia', 'Artemisia', 'artemisia', 'artemisia', 'Herb', 'Herb', 'Artemisia', 'Artemisia\r'),
(250, 'Basil', 'Basilic', 'basil', 'basilic', 'Herb', 'Herb', 'Basil', 'Basilic\r'),
(251, 'Buckwheat', 'Sarrasin', 'buckwheat', 'sarrasin', 'Herb', 'Herb', 'Buckwheat', 'Sarrasin\r'),
(252, 'Calamus', 'Calamus', 'calamus', 'calamus', 'Herb', 'Herb', 'Calamus', 'Calamus\r'),
(253, 'Chervil', 'Cerfeuil', 'chervil', 'cerfeuil', 'Herb', 'Herbe', 'Chervil', 'Cerfeuil\r'),
(254, 'Coriander', 'Coriandre', 'coriander; cilantro', 'coriandre ; cilantro', 'Herb', 'Herbe', 'Coriander', 'Coriandre\r'),
(255, 'Cornmint', 'Millepertuis', 'cornmint', 'menthe des champs', 'Herb', 'Herbe', 'Cornmint', 'Millepertuis\r'),
(256, 'Dill', 'Aneth', 'dill', 'aneth', 'Herb', 'Herbe', 'Dill', 'Aneth\r'),
(257, 'Fennel', 'Fenouil', 'fennel', 'fenouil', 'Herb', 'Herbe', 'Fennel', 'Fenouil\r'),
(258, 'Fenugreek', 'Fenugrec', 'fenugreek; methi', 'fenugrec ; methi', 'Herb', 'Herbe', 'Fenugreek', 'Fenugrec\r'),
(259, 'Garlic', 'Ail', 'garlic', 'ail', 'Herb', 'Herbe', 'Garlic', 'Ail\r'),
(260, 'Lemon Balm', 'Mélisse', 'lemon-balm', 'citron-balme', 'Herb', 'Herbe', 'Lemon Balm', 'Mélisse\r'),
(261, 'Liqourice', 'Liqourice', 'liquorice', 'réglisse', 'Herb', 'Herbe', 'Liqourice', 'Liqourice\r'),
(262, 'Mint', 'Menthe', 'mint', 'menthe', 'Herb', 'Herbe', 'Mint', 'Menthe\r'),
(263, 'Rhubarb', 'Rhubarbe', 'rhubarb', 'rhubarbe', 'Herb', 'Herbe', 'Rhubarb', 'Rhubarbe\r'),
(264, 'Rosemary', 'Romarin', 'rosemary', 'romarin', 'Herb', 'Herbe', 'Rosemary', 'Romarin\r'),
(265, 'Sage', 'Sauge', 'sage', 'sauge', 'Herb', 'Herbe', 'Sage', 'Sauge\r'),
(266, 'Spearmint', 'Menthe verte', 'spearmint-oil', 'huile de menthe verte', 'Herb', 'Herbe', 'Spearmint', 'Menthe verte\r'),
(267, 'Scotch Spearmint', 'Menthe verte écossaise', 'spearmint-scotch', 'spearmint-scotch', 'Herb', 'Herbe', 'Scotch Spearmint', 'Menthe verte écossaise\r'),
(268, 'Tarragon', 'Estragon', 'tarragon', 'estragon', 'Herb', 'Herbe', 'Tarragon', 'Estragon\r'),
(269, 'Thyme', 'Thym', 'thyme', 'thym', 'Herb', 'Herbe', 'Thyme', 'Thym\r'),
(270, 'Beef', 'Bœuf', 'beef; steak; veal; sirloin', 'bœuf ; steak ; veau ; aloyau', 'Meat', 'Viande', 'Beef', 'Bœuf\r'),
(271, 'Beef Processed', 'Bœuf transformé', 'beef-processed', 'bœuf transformé', 'Meat', 'Viande', 'Beef Processed', 'Bœuf transformé\r'),
(272, 'Chicken', 'Poulet', 'chicken', 'poulet', 'Meat', 'Viande', 'Chicken', 'Poulet\r'),
(273, 'Frankfurter Sausage', 'Saucisse de Francfort', 'frankfurter-sausage', 'saucisse de Francfort', 'Dish', 'Plat', 'Frankfurter Sausage', 'Saucisse de Francfort\r'),
(274, 'Ham', 'Jambon', 'ham', 'jambon', 'Meat', 'Viande', 'Ham', 'Jambon\r'),
(275, 'Lamb', 'Agneau', 'lamb; keema', 'agneau ; keema', 'Meat', 'Viande', 'Lamb', 'Agneau\r'),
(276, 'Meat', 'Viande', 'meat', 'viande', 'Meat', 'Viande', 'Meat', 'Viande\r'),
(277, 'Mutton', 'Mouton', 'mutton', 'mouton', 'Meat', 'Viande', 'Mutton', 'Mouton\r'),
(278, 'Pork', 'Porc', 'pork; pig; bacon', 'porc ; cochon ; lard', 'Meat', 'Viande', 'Pork', 'Porc\r'),
(279, 'Sukiyaki', 'Sukiyaki', 'sukiyaki', 'sukiyaki', 'Meat', 'Viande', 'Sukiyaki', 'Sukiyaki\r'),
(280, 'Turkey', 'Dinde', 'turkey', 'dinde', 'Meat', 'Viande', 'Turkey', 'Dinde\r'),
(281, 'Almond', 'Amande', 'almond', 'amande', 'Nuts & Seed', 'Noix et graines', 'Almond', 'Amande\r'),
(282, 'Brazil Nut', 'Noix du Brésil', 'brazil-nut', 'brésilien-noisette', 'Nuts & Seed', 'Fruits à coque et graines', 'Brazil Nut', 'Noix du Brésil\r'),
(283, 'Cocoa', 'Cacao', 'cocoa', 'cacao', 'Nuts & Seed', 'Noix et graines', 'Cocoa', 'Cacao\r'),
(284, 'Beans', 'Haricots', 'bean', 'haricot', 'Legume', 'Légumineuses', 'Beans', 'Haricots\r'),
(285, 'Lima Beans', 'Haricot de Lima', 'bean-lima; bean-sieve; butter-bean', 'haricot-lima ; haricot-maïs ; haricot-beurre', 'Legume', 'Légumineuses', 'Lima Beans', 'Haricot de Lima\r'),
(286, 'Kidney Beans', 'Haricots rouges', 'bean-kidney; bean-red; bean-red-kidney; rajma', 'haricot-rognon ; haricot-rouge ; haricot-rouge-rognon ; rajma', 'Legume', 'Légumineuse', 'Kidney Beans', 'Haricots rouges\r'),
(287, 'Peanut', 'Cacahuète', 'peanut', 'arachide', 'Nuts & Seed', 'Noix et graines', 'Peanut', 'Cacahuète\r'),
(288, 'Peas', 'Pois', 'pea', 'pois', 'Legume', 'Légumineuse', 'Peas', 'Pois\r'),
(289, 'Soybean', 'Soja', 'soy-bean; soya-bean', 'soja-haricot ; soja-haricot', 'Legume', 'Légumineuse', 'Soybean', 'Soja\r'),
(290, 'Soybean Oil', 'Huile de soja', 'soy-bean-oil; soya-bean-oil', 'soja-huile de soja ; soja-huile de soja', 'Legume', 'Légumineuse', 'Soybean Oil', 'Huile de soja\r'),
(291, 'Soybean Sauce', 'Sauce de soja', 'soy-bean-sauce; soya-bean-sauce; soy-sauce; soya-sauce', 'soja-sauce de soja ; soja-sauce de soja ; soja-sauce de soja ; soja-sauce de soja', 'Plant', 'Plante', 'Soybean Sauce', 'Sauce de soja\r'),
(292, 'Filbert', 'Filbert', 'filbert', 'aveline', 'Nuts & Seed', 'Noix et graines', 'Filbert', 'Filbert\r'),
(293, 'Hazelnut', 'Noisette', 'hazel-nut', 'noisetier-noisette', 'Nuts & Seed', 'Noix et graines', 'Hazelnut', 'Noisette\r'),
(294, 'Macadamia Nut', 'Noix de Macadamia', 'macadamia-nut; bush-nut; hawaii-nut', 'macadamia-noisette ; brousse-noisette ; hawaii-noisette', 'Nuts & Seed', 'Noix et graines', 'Macadamia Nut', 'Noix de Macadamia\r'),
(296, 'Pecans', 'Noix de pécan', 'pecan', 'noix de pécan', 'Nuts & Seed', 'Noix et graines', 'Pecans', 'Noix de pécan\r'),
(297, 'Walnut', 'Noix', 'walnut', 'noix', 'Nuts & Seed', 'Noix et graines', 'Walnut', 'Noix\r'),
(298, 'Muskmallow', 'Mousseron', 'musk-mallow', 'musc-mallow', 'Nuts & Seed', 'Noix et graines', 'Muskmallow', 'Mousseron\r'),
(299, 'Sesame', 'Sésame', 'sesame', 'sésame', 'Nuts & Seed', 'Noix et graines', 'Sesame', 'Sésame\r'),
(300, 'Allium', 'Allium', 'allium', 'allium', 'Plant', 'Plante', 'Allium', 'Allium\r'),
(301, 'Alpinia', 'Alpinia', 'alpinia', 'alpinia', 'Plant', 'Plante', 'Alpinia', 'Alpinia\r'),
(302, 'Ceriman', 'Ceriman', 'ceriman', 'ceriman', 'Plant', 'Plante', 'Ceriman', 'Ceriman\r'),
(303, 'Chicory', 'Chicorée', 'chicory', 'chicorée', 'Plant', 'Plante', 'Chicory', 'Chicorée\r'),
(304, 'Hops', 'Houblon', 'hops', 'houblon', 'Plant', 'Plante', 'Hops', 'Houblon\r'),
(305, 'Laurel', 'Laurier', 'bay-laurel; bay-leaf', 'baie-laurel ; baie-feuille', 'Plant', 'Plante', 'Laurel', 'Laurier\r'),
(306, 'Myrtle', 'Myrte', 'myrtle', 'myrte', 'Plant', 'Plante', 'Myrtle', 'Myrte\r'),
(307, 'Olive', 'Olive', 'olive', 'olive', 'Plant', 'Plante', 'Olive', 'Olive\r'),
(308, 'Pine', 'Pin', 'pine', 'pin', 'Plant', 'Plante', 'Pine', 'Pin\r'),
(309, 'Sassafras', 'Sassafras', 'sassafras', 'sassafras', 'Plant', 'Plante', 'Sassafras', 'Sassafras\r'),
(310, 'Tea', 'Thé', 'tea', 'thé', 'Plant', 'Plante', 'Tea', 'Thé\r'),
(311, 'Fermented Tea', 'Thé fermenté', 'tea-fermented', 'thé fermenté', 'Plant', 'Plante', 'Fermented Tea', 'Thé fermenté\r'),
(312, 'Tobacco', 'Tabac', 'tobacco', 'tabac', 'Plant', 'Plante', 'Tobacco', 'Tabac\r'),
(313, 'Watercress', 'Cresson', 'watercress', 'cresson', 'Plant', 'Plante', 'Watercress', 'Cresson\r'),
(314, 'Creosote', 'Créosote', 'creosote', 'créosote', 'Plant', 'Plante', 'Creosote', 'Créosote\r'),
(315, 'Honey', 'Miel', 'honey', 'miel', 'Plant', 'Plante', 'Honey', 'Miel\r'),
(316, 'Macaroni', 'Macaroni', 'macaroni', 'macaroni', 'Plant', 'Plante', 'Macaroni', 'Macaroni\r'),
(317, 'Mustard Oil', 'Huile de moutarde', 'mustard-oil', 'huile de moutarde', 'Plant', 'Plante', 'Mustard Oil', 'Huile de moutarde\r'),
(318, 'Peanut Butter', 'Beurre d\'arachide', 'peanut-butter', 'cacahuète-beurre', 'Plant', 'Plante', 'Peanut Butter', 'Beurre d\'arachide\r'),
(319, 'Peanut Oil', 'Huile d\'arachide', 'peanut-oil', 'huile d\'arachide', 'Plant', 'Plante', 'Peanut Oil', 'Huile d\'arachide\r'),
(320, 'Rye', 'Seigle', 'rye', 'seigle', 'Cereal', 'Céréales', 'Rye', 'Seigle\r'),
(321, 'Storax', 'Storax', 'storax', 'storax', 'Plant', 'Plante', 'Storax', 'Storax\r'),
(322, 'Vinegar', 'Vinaigre', 'vinegar', 'vinaigre', 'Plant', 'Plante', 'Vinegar', 'Vinaigre\r'),
(323, 'Anise', 'Anis', 'anise; saunf', 'anis ; saunf', 'Spice', 'Épices', 'Anise', 'Anis\r'),
(324, 'Anise Hyssop', 'Anis hysope', 'anise-hyssop', 'anis-hysope', 'Spice', 'Épice', 'Anise Hyssop', 'Anis hysope\r'),
(325, 'Star Anise', 'Anis étoilé', 'anise-star', 'anis étoilé', 'Spice', 'Épice', 'Star Anise', 'Anis étoilé\r'),
(326, 'Caraway', 'Carvi', 'caraway', 'carvi', 'Spice', 'Épice', 'Caraway', 'Carvi\r'),
(327, 'Cardamom', 'Cardamome', 'cardamom; cardamon', 'cardamome ; cardamone', 'Spice', 'Épice', 'Cardamom', 'Cardamome\r'),
(328, 'Cassia', 'Cassia', 'cassia', 'cassia', 'Spice', 'Épice', 'Cassia', 'Cassia\r'),
(329, 'Celery', 'Céleri', 'celery', 'céleri', 'Spice', 'Épice', 'Celery', 'Céleri\r'),
(330, 'Cinnamon', 'Cannelle', 'cinnamon', 'cannelle', 'Spice', 'Épice', 'Cinnamon', 'Cannelle\r'),
(331, 'Clove', 'Clou de girofle', 'clove', 'clou de girofle', 'Spice', 'Épice', 'Clove', 'Clou de girofle\r'),
(332, 'Cumin', 'Cumin', 'cumin; jeera', 'cumin ; jeera', 'Spice', 'Épice', 'Cumin', 'Cumin\r'),
(333, 'Ginger', 'Gingembre', 'ginger', 'gingembre', 'Spice', 'Épice', 'Ginger', 'Gingembre\r'),
(334, 'Mace', 'Macis', 'mace', 'macis', 'Spice', 'Épice', 'Mace', 'Macis\r'),
(335, 'Marjoram', 'Marjolaine', 'marjoram', 'marjolaine', 'Spice', 'Épice', 'Marjoram', 'Marjolaine\r'),
(336, 'Nutmeg', 'Noix de muscade', 'nutmeg', 'noix de muscade', 'Spice', 'Épice', 'Nutmeg', 'Noix de muscade\r'),
(337, 'Oregano', 'Origan', 'oregano', 'origan', 'Spice', 'Épice', 'Oregano', 'Origan\r'),
(338, 'Parsley', 'Persil', 'parsley', 'persil', 'Spice', 'Épice', 'Parsley', 'Persil\r'),
(339, 'Pepper', 'Poivre', 'pepper; kaali-mirch', 'poivre ; kaali-mirch', 'Spice', 'Épice', 'Pepper', 'Poivre\r'),
(340, 'Saffron', 'Safran', 'saffron', 'safran', 'Spice', 'Épice', 'Saffron', 'Safran\r'),
(341, 'Turmeric', 'Curcuma', 'turmeric; tumeric', 'curcuma ; tumeric', 'Spice', 'Épice', 'Turmeric', 'Curcuma\r'),
(342, 'Green Beans', 'Haricots verts', 'bean-green; bean-field; bean-flageolet; bean-french; bean-garden; bean-haricot; bean-string; bean-snap', 'haricot vert ; champ de haricots ; haricot-flageolet ; haricot-français ; jardin de haricots ; haricot-haricot ; haricot-corde ; haricot-fraise', 'Vegetable', 'Légumes', 'Green Beans', 'Haricots verts\r'),
(343, 'Chive', 'Ciboulette', 'chive', 'ciboulette', 'Vegetable', 'Légumes', 'Chive', 'Ciboulette\r'),
(344, 'Endive', 'Endive', 'endive', 'endive', 'Vegetable', 'Végétale', 'Endive', 'Endive\r'),
(345, 'Leek', 'Poireau', 'leek', 'poireau', 'Vegetable', 'Végétale', 'Leek', 'Poireau\r'),
(346, 'Lettuce', 'Laitue', 'lettuce', 'laitue', 'Vegetable', 'Légumes', 'Lettuce', 'Laitue\r'),
(347, 'Okra', 'Gombo', 'okra; bhindi; lady-finger', 'okra ; bhindi ; lady-finger', 'Vegetable', 'Légumes', 'Okra', 'Gombo\r'),
(348, 'Onion', 'Oignon', 'onion', 'oignon', 'Vegetable', 'Légumes', 'Onion', 'Oignon\r'),
(349, 'Shallot', 'Échalote', 'shallot', 'échalote', 'Vegetable', 'Légumes', 'Shallot', 'Échalote\r'),
(350, 'Peppermint', 'Menthe poivrée', 'peppermint', 'menthe poivrée', 'Herb', 'Herbe', 'Peppermint', 'Menthe poivrée\r'),
(351, 'Broccoli', 'Brocoli', 'broccoli; broccolini', 'brocoli ; broccolini', 'Vegetable', 'Légumes', 'Broccoli', 'Brocoli\r'),
(352, 'Brussels Sprout', 'Chou de Bruxelles', 'brussels-sprout', 'chou de Bruxelles', 'Vegetable', 'Légumes', 'Brussels Sprout', 'Chou de Bruxelles\r'),
(353, 'Cabbage', 'Chou', 'cabbage', 'chou', 'Vegetable', 'Légumes', 'Cabbage', 'Chou\r'),
(354, 'Cauliflower', 'Chou-fleur', 'cauliflower', 'chou-fleur', 'Vegetable', 'Légumes', 'Cauliflower', 'Chou-fleur\r'),
(355, 'Horseradish', 'Raifort', 'horseradish', 'raifort', 'Vegetable', 'Légumes', 'Horseradish', 'Raifort\r'),
(356, 'Mustard', 'Moutarde', 'mustard', 'moutarde', 'Vegetable', 'Légumes', 'Mustard', 'Moutarde\r'),
(357, 'Radish', 'Radis', 'radish; daikon; mooli', 'radis ; daikon ; mooli', 'Vegetable', 'Légumes', 'Radish', 'Radis\r'),
(358, 'Turnip', 'Navet', 'turnip', 'navet', 'Vegetable', 'Légumes', 'Turnip', 'Navet\r'),
(359, 'Kohlrabi', 'Chou-rave', 'kohlrabi', 'chou-rave', 'Vegetable', 'Légumes', 'Kohlrabi', 'Chou-rave\r'),
(360, 'Rutabaga', 'Rutabaga', 'rutabaga', 'rutabaga', 'Vegetable', 'Légumes', 'Rutabaga', 'Rutabaga\r'),
(361, 'Wasabi', 'Wasabi', 'wasabi', 'wasabi', 'Vegetable', 'Légumes', 'Wasabi', 'Wasabi\r'),
(362, 'Capsicum', 'Capsicum', 'capsicum; paprika; pepper-bell; pepper-sweet', 'poivron ; paprika ; poivre en cloche ; poivre doux', 'Vegetable', 'Légumes', 'Capsicum', 'Capsicum\r'),
(363, 'Cherry Pepper', 'Poivron cerise', 'pimiento; cherry-pepper', 'pimiento ; cerise-poivre', 'Vegetable', 'Légumes', 'Cherry Pepper', 'Poivron cerise\r'),
(364, 'Tomato', 'Tomate', 'tomato', 'tomate', 'Vegetable', 'Légumes', 'Tomato', 'Tomate\r'),
(365, 'Chayote', 'Chayotte', 'chayote', 'chayotte', 'Vegetable', 'Légumes', 'Chayote', 'Chayotte\r'),
(366, 'Cucumber', 'Concombre', 'cucumber', 'concombre', 'Vegetable', 'Légumes', 'Cucumber', 'Concombre\r'),
(367, 'Beetroot', 'Betterave rouge', 'beetroot; beet', 'betterave ; betterave', 'Vegetable', 'Légumes', 'Beetroot', 'Betterave rouge\r'),
(368, 'Carrot', 'Carotte', 'carrot', 'carotte', 'Vegetable', 'Légumes', 'Carrot', 'Carotte\r'),
(369, 'Parsnip', 'Panais', 'parsnip', 'panais', 'Vegetable', 'Légumes', 'Parsnip', 'Panais\r'),
(370, 'Sweet Potato', 'Patate douce', 'potato-sweet', 'pomme de terre douce', 'Vegetable', 'Légumes', 'Sweet Potato', 'Patate douce\r'),
(371, 'Asparagus', 'Asperge', 'asparagus', 'asperge', 'Vegetable', 'Légumes', 'Asparagus', 'Asperge\r'),
(372, 'Cassava', 'Manioc', 'cassava', 'manioc', 'Vegetable', 'Légumes', 'Cassava', 'Manioc\r'),
(373, 'Potato', 'Pomme de terre', 'potato', 'pomme de terre', 'Vegetable', 'Légumes', 'Potato', 'Pomme de terre\r'),
(374, 'Fried Potato', 'Pommes de terre frites', 'potato-fried', 'pomme de terre-frite', 'Bakery', 'Boulangerie', 'Fried Potato', 'Pommes de terre frites\r'),
(375, 'Allspice', 'Piment de la Jamaïque', 'allspice; pimenta; myrtle-pepper', 'piment de la Jamaïque ; pimenta ; myrte-poivre', 'Spice', 'Épices', 'Allspice', 'Piment de la Jamaïque\r'),
(376, 'Asafoetida', 'Asafoetida', 'asafoetida; asafetida; hing; asant', 'asafoetida ; asafetida ; hing ; asant', 'Spice', 'Épice', 'Asafoetida', 'Asafoetida\r'),
(377, 'Ashgourd', 'Courge cendrée', 'ash-gourd; white-gourd', 'cendre-courgeoise ; blanc-courgeoise', 'Vegetable', 'Légumes', 'Ashgourd', 'Courge cendrée\r'),
(378, 'Bittergourd', 'Courge amère', 'bitter-gourd; squash-bitter', 'amer-courgeoise ; amer de courge', 'Vegetable', 'Légumes', 'Bittergourd', 'Courge amère\r'),
(379, 'Bottlegourd', 'Gourmandise', 'bottle-gourd', 'bouteille-courgeoise', 'Vegetable', 'Végétale', 'Bottlegourd', 'Gourmandise\r'),
(380, 'Canola Oil', 'Huile de canola', 'canola-oil', 'huile de canola', 'Essential Oil', 'Huile essentielle', 'Canola Oil', 'Huile de canola\r'),
(381, 'Carom Seed', 'Graine de carome', 'carom seed; ajwain', 'graine de carome ; ajwain', 'Spice', 'Épice', 'Carom Seed', 'Graine de carome\r'),
(382, 'Chard', 'Bette à carde', 'chard', 'blette', 'Vegetable', 'Végétal', 'Chard', 'Bette à carde\r'),
(383, 'Apple Cider Vinegar', 'Vinaigre de cidre de pomme', 'vinegar-cider; cider-vinegar', 'vinaigre-cidre ; cidre-vinaigre', 'Plant', 'Plante', 'Apple Cider Vinegar', 'Vinaigre de cidre de pomme\r'),
(384, 'Colocasia', 'Colocasia', 'colocasia', 'colocasia', 'Vegetable', 'Végétal', 'Colocasia', 'Colocasia\r'),
(385, 'Curry Leaf', 'Feuille de curry', 'curry-leaf', 'curry-feuille', 'Herb', 'Herbe', 'Curry Leaf', 'Feuille de curry\r'),
(386, 'Drumstick Leaf', 'Feuille de pilon', 'drumstick-leaf', 'feuille de pilon', 'Vegetable', 'Légumes', 'Drumstick Leaf', 'Feuille de pilon\r'),
(387, 'Eggplant', 'Aubergine', 'eggplant; aubergine; brinjal', 'aubergine ; aubergine ; brinjal', 'Vegetable', 'Légume', 'Eggplant', 'Aubergine\r'),
(388, 'Flaxseed', 'Graine de lin', 'flax-seed', 'lin-graine', 'Nuts & Seed', 'Noix et graines', 'Flaxseed', 'Graine de lin\r'),
(389, 'Jalapeno', 'Jalapeno', 'jalapeno', 'jalapeno', 'Spice', 'Épices', 'Jalapeno', 'Jalapeno\r'),
(390, 'Kenaf', 'Kenaf', 'kenaf', 'kenaf', 'Essential Oil', 'Huile essentielle', 'Kenaf', 'Kenaf\r'),
(391, 'Lotus', 'Lotus', 'lotus', 'lotus', 'Essential Oil', 'Huile essentielle', 'Lotus', 'Lotus\r'),
(392, 'Nigella Seed', 'Graine de Nigelle', 'nigella-seed; kalonji', 'graine de nigelle ; kalonji', 'Nuts & Seed', 'Noix et graines', 'Nigella Seed', 'Graine de Nigelle\r'),
(393, 'Kewda', 'Kewda', 'kewda; kewra', 'kewda ; kewra', 'Essential Oil', 'Huile essentielle', 'Kewda', 'Kewda\r'),
(394, 'Pomegranate', 'Grenade', 'pomegranate; anaardana', 'grenade ; anaardana', 'Fruit', 'Fruits', 'Pomegranate', 'Grenade\r'),
(395, 'Poppy Seed', 'Graine de pavot', 'poppy-seed', 'coquelicot ; graine', 'Spice', 'Épices', 'Poppy Seed', 'Graine de pavot\r'),
(396, 'Spinach', 'Epinards', 'spinach; palak', 'épinard ; palak', 'Vegetable', 'Végétal', 'Spinach', 'Epinards\r'),
(397, 'Turkey Berry', 'Baie de dinde', 'turkey-berry', 'dinde-berry', 'Vegetable', 'Légume', 'Turkey Berry', 'Baie de dinde\r'),
(398, 'Water Chestnut', 'Châtaigne d\'eau', 'water-chestnut; water-caltrop', 'eau-châtaigne ; eau-caltrop', 'Fruit', 'Fruit', 'Water Chestnut', 'Châtaigne d\'eau\r'),
(399, 'White Pepper', 'Poivre blanc', 'pepper-white', 'poivre blanc ;', 'Spice', 'Épice', 'White Pepper', 'Poivre blanc\r'),
(400, 'Garcinia Indica', 'Garcinia Indica', 'kokum; garnicia-indica', 'kokum ; garnicia-indica', 'Fruit', 'Fruit', 'Garcinia Indica', 'Garcinia Indica\r'),
(401, 'Cluster Bean', 'Haricot à grappes', 'bean-cluster; bean-guar', 'haricot-cluster ; haricot-guar', 'Legume', 'Légumineuse', 'Cluster Bean', 'Haricot à grappes\r'),
(402, 'Paneer', 'Paneer', 'paneer', 'paneer', 'Dairy', 'Produits laitiers', 'Paneer', 'Paneer\r'),
(403, 'Pigeon Pea', 'Pois d\'Angole', 'pea-pigeon; arhar; pea-no-eye; dal-toor; red-gram', 'pois-pigeon ; arhar ; pois-no-oeil ; dal-toor ; rouge-gramme', 'Legume', 'Légumineuse', 'Pigeon Pea', 'Pois d\'Angole\r'),
(404, 'Basmati Rice', 'Riz Basmati', 'basmati', 'basmati', 'Cereal', 'Céréales', 'Basmati Rice', 'Riz Basmati\r'),
(405, 'Ricotta Cheese', 'Fromage Ricotta', 'cheese-ricotta', 'fromage-ricotta', 'Dairy', 'Laitière', 'Ricotta Cheese', 'Fromage Ricotta\r'),
(408, 'Silver linden', 'Tilleul argenté', 'linden-silver', 'tilleul-argenté', 'Herb', 'Herbe', 'Silver linden', 'Tilleul argenté\r'),
(409, 'Redskin onion', 'Oignon à peau rouge', 'onion-red-skin; onion-red', 'oignon-rouge-peau ; oignon-rouge', 'Vegetable', 'Légumes', 'Redskin onion', 'Oignon à peau rouge\r'),
(410, 'Lemon verbena', 'Verveine citronnée', 'verbena-lemon', 'verveine-citron', 'Herb', 'Herbe', 'Lemon verbena', 'Verveine citronnée\r'),
(411, 'Cashew nut', 'Noix de cajou', 'cashew; cashew-nut', 'noix de cajou ; noix de cajou-noix', 'Nuts & Seed', 'Noix et graines', 'Cashew nut', 'Noix de cajou\r'),
(412, 'Burdock', 'Bardane', 'burdock', 'bardane', 'Vegetable', 'Légumes', 'Burdock', 'Bardane\r'),
(413, 'Borage', 'Bourrache', 'borage', 'bourrache', 'Herb', 'Herbe', 'Borage', 'Bourrache\r'),
(414, 'Capers', 'Câpres', 'caper', 'câpre', 'Herb', 'Herbe', 'Capers', 'Câpres\r'),
(415, 'Safflower', 'Carthame', 'saf-flower', 'saf-fleur', 'Herb', 'Herbe', 'Safflower', 'Carthame\r'),
(416, 'Chestnut', 'Châtaigne', 'chestnut', 'châtaigne', 'Nuts & Seed', 'Noix et graines', 'Chestnut', 'Châtaigne\r'),
(417, 'Chickpea', 'Pois chiche', 'pea-chick; besan; chickpea; kabuli chana; kala chana; garbanzo', 'pois chiche ; besan ; pois chiche ; kabuli chana ; kala chana ; garbanzo', 'Legume', 'Légumineuse', 'Chickpea', 'Pois chiche\r'),
(418, 'Pummelo', 'Pummelo', 'pummelo', 'pummelo', 'Fruit', 'Fruit', 'Pummelo', 'Pummelo\r');
INSERT INTO `ingredient` (`id`, `IngredientNameEN`, `IngredientNameFR`, `IngredientSynonymEN`, `IngredientSynonymFR`, `CategoryEN`, `CategoryFR`, `ContituentIngredientsEN`, `ContituentIngredientsFR`) VALUES
(419, 'Arabica coffee', 'Café Arabica', 'coffee-arabica', 'café-arabica', 'Beverage', 'Boisson', 'Arabica coffee', 'Café Arabica\r'),
(421, 'Japanese persimmon', 'Kaki japonais', 'persimmon-japanese', 'kaki-japonais', 'Fruit', 'Fruit', 'Japanese persimmon', 'Kaki japonais\r'),
(422, 'Black crowberry', 'Corneille noire', 'crowberry-black', 'camarine noire', 'Fruit', 'Fruit', 'Black crowberry', 'Corneille noire\r'),
(423, 'Rocket salad', 'Salade de roquette', 'salad-rocket', 'salade-racine', 'Herb', 'Herbe', 'Rocket salad', 'Salade de roquette\r'),
(424, 'Tartary buckwheat', 'Sarrasin de Tartarie', 'buckwheat-tartary', 'sarrasin-tartare', 'Cereal', 'Céréales', 'Tartary buckwheat', 'Sarrasin de Tartarie\r'),
(425, 'Black huckleberry', 'Myrtille noire', 'huckleberry-black', 'myrtille-noire', 'Fruit', 'Fruit', 'Black huckleberry', 'Myrtille noire\r'),
(426, 'Sunflower', 'Tournesol', 'sunflower', 'tournesol', 'Flower', 'Fleur', 'Sunflower', 'Tournesol\r'),
(427, 'Swamp cabbage', 'Chou des marais', 'cabbage-swamp', 'chou-champignon', 'Vegetable', 'Légume', 'Swamp cabbage', 'Chou des marais\r'),
(428, 'Grass pea', 'Pois de senteur', 'pea-grass', 'pois-herbe', 'Legume', 'Légumineuse', 'Grass pea', 'Pois de senteur\r'),
(429, 'Lentils', 'Lentilles', 'lentil', 'lentille', 'Legume', 'Légumineuse', 'Lentils', 'Lentilles\r'),
(430, 'Garden cress', 'Cresson de jardin', 'cress', 'cresson', 'Herb', 'Herbe', 'Garden cress', 'Cresson de jardin\r'),
(431, 'Mexican oregano', 'Origan mexicain', 'oregano-mexican', 'oregano-mexicain', 'Herb', 'Herbe', 'Mexican oregano', 'Origan mexicain\r'),
(432, 'Lupine', 'Lupin', 'lupine', 'lupin', 'Plant', 'Plante', 'Lupine', 'Lupin\r'),
(433, 'Medlar', 'Mèfle', 'medlar', 'néflier', 'Fruit', 'Fruit', 'Medlar', 'Mèfle\r'),
(434, 'Mulberry', 'Mûrier', 'mulberry', 'mûrier', 'Fruit', 'Fruit', 'Mulberry', 'Mûrier\r'),
(435, 'Black mulberry', 'Mûrier noir', 'mulberry-black', 'mûrier-noir', 'Fruit', 'Fruit', 'Black mulberry', 'Mûrier noir\r'),
(436, 'Evening primrose', 'Onagre', 'primrose', 'primevère', 'Herb', 'Herbe', 'Evening primrose', 'Onagre\r'),
(437, 'Millet', 'Millet', 'millet', 'millet', 'Legume', 'Légumineuse', 'Millet', 'Millet\r'),
(438, 'Scarlet bean', 'Haricot écarlate', 'bean-scarlet', 'haricot-scarlette', 'Legume', 'Légumineuse', 'Scarlet bean', 'Haricot écarlate\r'),
(439, 'Pistachio', 'Pistachier', 'pistachio', 'pistache', 'Nuts & Seed', 'Noix et graines', 'Pistachio', 'Pistachier\r'),
(440, 'Purslane', 'Pourpier', 'purslane', 'pourpier', 'Plant', 'Plante', 'Purslane', 'Pourpier\r'),
(441, 'Red raspberry', 'Framboise rouge', 'raspberry-red', 'framboise-rouge', 'Fruit', 'Fruit', 'Red raspberry', 'Framboise rouge\r'),
(442, 'Black raspberry', 'Framboise noire', 'raspberry-black', 'framboise-noire', 'Fruit', 'Fruit', 'Black raspberry', 'Framboise noire\r'),
(443, 'Sorrel', 'Oseille', 'sorrel', 'oseille', 'Herb', 'Herbe', 'Sorrel', 'Oseille\r'),
(444, 'Summer savory', 'Sariette d\'été', 'savory-summer', 'sarriette-été', 'Herb', 'Herbe', 'Summer savory', 'Sariette d\'été\r'),
(445, 'Winter savory', 'Sarriette d\'hiver', 'savory-winter', 'sarriette-hiver', 'Herb', 'Herbe', 'Winter savory', 'Sarriette d\'hiver\r'),
(446, 'Cherry tomato', 'Tomate cerise', 'cherry-tomato', 'cerise-tomate', 'Fruit', 'Fruit', 'Cherry tomato', 'Tomate cerise\r'),
(447, 'Rowanberry', 'Rowanberry', 'rowan-berry', 'sorbier-mûre', 'Fruit', 'Fruit', 'Rowanberry', 'Rowanberry\r'),
(448, 'Sorghum', 'Sorgho', 'sorghum', 'sorgho', 'Cereal', 'Céréale', 'Sorghum', 'Sorgho\r'),
(449, 'Dandelion', 'Pissenlit', 'dandelion', 'pissenlit', 'Flower', 'Fleur', 'Dandelion', 'Pissenlit\r'),
(450, 'Linden', 'Tilleul', 'linden', 'tilleul', 'Herb', 'Herbe', 'Linden', 'Tilleul\r'),
(451, 'Small leaf linden', 'Tilleul à petites feuilles', 'linden-leaf', 'feuille de tilleul', 'Plant', 'Plante', 'Small leaf linden', 'Tilleul à petites feuilles\r'),
(452, 'Wheat', 'Blé', 'wheat', 'blé', 'Cereal', 'Céréales', 'Wheat', 'Blé\r'),
(453, 'Sparkleberry', 'Amélanchier', 'sparkleberry', 'fraise des bois', 'Fruit', 'Fruit', 'Sparkleberry', 'Amélanchier\r'),
(454, 'Common verbena', 'Verveine commune', 'vverbena; vervain', 'verveine ; verveine', 'Herb', 'Herbe', 'Common verbena', 'Verveine commune\r'),
(455, 'Adzuki bean', 'Haricot Adzuki', 'bean-adzuki', 'haricot-adzuki', 'Legume', 'Légumineuse', 'Adzuki bean', 'Haricot Adzuki\r'),
(456, 'Gram bean', 'Haricot Gram', 'bean-gram', 'haricot-gramme', 'Legume', 'Légumineuse', 'Gram bean', 'Haricot Gram\r'),
(457, 'Mung bean', 'Haricot mungo', 'bean-mung; bean-moong', 'haricot-mung ; haricot-moong', 'Legume', 'Légumineuse', 'Mung bean', 'Haricot mungo\r'),
(458, 'Climbing bean', 'Haricot grimpant', 'bean-climbing', 'haricot grimpant', 'Legume', 'Légumineuse', 'Climbing bean', 'Haricot grimpant\r'),
(459, 'Muscadine grape', 'Raisin muscadine', 'grape-muscadine', 'raisin-muscadine', 'Fruit', 'Fruit', 'Muscadine grape', 'Raisin muscadine\r'),
(460, 'Bayberry', 'Myrtille', 'bayberry', 'baie de laurier', 'Fruit', 'Fruit', 'Bayberry', 'Myrtille\r'),
(461, 'Elliott\'s blueberry', 'Myrtille d\'Elliott', 'blueberry-elliot', 'myrtille-elliot', 'Fruit', 'Fruit', 'Elliott\'s blueberry', 'Myrtille d\'Elliott\r'),
(462, 'Canada blueberry', 'Myrtille du Canada', 'blueberry-canada', 'myrtille-canada', 'Fruit', 'Fruit', 'Canada blueberry', 'Myrtille du Canada\r'),
(463, 'Buffalo currant', 'Groseille à maquereau', 'currant-buffalo', 'groseille-buffle', 'Fruit', 'Fruit', 'Buffalo currant', 'Groseille à maquereau\r'),
(464, 'Deerberry', 'Raisin de cerf', 'deer-berry', 'cerf-mûre', 'Fruit', 'Fruit', 'Deerberry', 'Raisin de cerf\r'),
(465, 'Ginseng', 'Ginseng', 'ginseng', 'ginseng', 'Vegetable', 'Végétal', 'Ginseng', 'Ginseng\r'),
(466, 'Longan', 'Longan', 'longan', 'longan', 'Plant', 'Plante', 'Longan', 'Longan\r'),
(469, 'Rambutan', 'Ramboutan', 'rambutan', 'ramboutan', 'Fruit', 'Fruit', 'Rambutan', 'Ramboutan\r'),
(470, 'Red rice', 'Riz rouge', 'rice-red', 'riz-rouge', 'Cereal', 'Céréales', 'Red rice', 'Riz rouge\r'),
(472, 'Welsh onion', 'Oignon gallois', 'onion-welsh; onion-spring; scallion', 'oignon-gallois ; oignon-ressort ; échalote', 'Vegetable', 'Légume', 'Welsh onion', 'Oignon gallois\r'),
(473, 'Hard wheat', 'Blé dur', 'wheat-hard; spaghetti', 'blé-dur ; spaghetti', 'Cereal', 'Céréales', 'Hard wheat', 'Blé dur\r'),
(474, 'Triticale', 'Triticale', 'triticale', 'triticale', 'Cereal', 'Céréales', 'Triticale', 'Triticale\r'),
(475, 'Komatsuna', 'Komatsuna', 'komatsuna', 'komatsuna', 'Vegetable', 'Légumes', 'Komatsuna', 'Komatsuna\r'),
(476, 'Pak choy', 'Pak choy', 'pak-choy; pak-choi', 'pak-choy ; pak-choi', 'Vegetable', 'Légumes', 'Pak choy', 'Pak choy\r'),
(477, 'Jostaberry', 'Jostaberry', 'jostaberry', 'jostaberry', 'Fruit', 'Fruit', 'Jostaberry', 'Jostaberry\r'),
(478, 'Kai lan', 'Kai lan', 'kai-lan', 'kai-lan', 'Vegetable', 'Légume', 'Kai lan', 'Kai lan\r'),
(480, 'Pineappple sage', 'Sauge ananas', 'pineappple-sage', 'ananas-sauce', 'Herb', 'Herbe', 'Pineappple sage', 'Sauge ananas\r'),
(481, 'Skunk currant', 'Groseille à maquereau', 'currant-skunk', 'groseille-skunk', 'Fruit', 'Fruit', 'Skunk currant', 'Groseille à maquereau\r'),
(482, 'Breakfast cereal', 'Céréales pour petit-déjeuner', 'cereal', 'céréales', 'Cereal', 'Céréales', 'Breakfast cereal', 'Céréales pour petit-déjeuner\r'),
(484, 'Pasta', 'Pâtes', 'pasta', 'pâtes', 'Bakery', 'Boulangerie', 'Pasta', 'Pâtes\r'),
(485, 'Biscuit', 'Biscuit', 'biscuit', 'biscuit', 'Bakery', 'Boulangerie', 'Biscuit', 'Biscuit\r'),
(486, 'Sourdough', 'Sourdough', 'sourdough', 'levain', 'Cereal', 'Céréales', 'Sourdough', 'Sourdough\r'),
(487, 'Spirit', 'Esprit', 'spirit', 'esprit', 'Beverage Alcoholic', 'Boisson alcoolisée', 'Spirit', 'Esprit\r'),
(489, 'Abalone', 'Ormeau', 'abalone', 'ormeau', 'Fungus', 'Champignon', 'Abalone', 'Ormeau\r'),
(490, 'Abiyuch', 'Abiyuch', 'abiyuch', 'abiyuch', 'Plant', 'Plante', 'Abiyuch', 'Abiyuch\r'),
(491, 'Acerola', 'Acérola', 'acerola', 'acérola', 'Fruit', 'Fruit', 'Acerola', 'Acérola\r'),
(492, 'Acorn', 'Gland', 'acorn', 'gland', 'Nuts & Seed', 'Noix et graines', 'Acorn', 'Gland\r'),
(493, 'Winter squash', 'Courge d\'hiver', 'squash-winter; squash-yellow', 'courge-hiver ; courge-jaune', 'Fruit', 'Fruit', 'Winter squash', 'Courge d\'hiver\r'),
(494, 'Agar', 'Agar', 'agar', 'agar', 'Additive', 'Additif', 'Agar', 'Agar\r'),
(495, 'Red king crab', 'Crabe royal rouge', '#crab-red-king#', '#crabe-rouge', 'Seafood', 'Fruits de mer', 'Red king crab', 'Crabe royal rouge\r'),
(496, 'Alfalfa', 'Luzerne', 'alfalfa', 'luzerne', 'Herb', 'Herb', 'Alfalfa', 'Luzerne\r'),
(497, 'Amaranth', 'Amarante', 'amaranth', 'amarante', 'Herb', 'Herbe', 'Amaranth', 'Amarante\r'),
(498, 'Arrowhead', 'Tête de flèche', 'arrowhead', 'tête de flèche', 'Vegetable', 'Légumes', 'Arrowhead', 'Tête de flèche\r'),
(499, 'Arrowroot', 'Arrowroot (racine de flèche)', 'arrowroot', 'arrowroot', 'Vegetable', 'Légumes', 'Arrowroot', 'Arrowroot (racine de flèche)\r'),
(500, 'Atlantic herring', 'Hareng de l\'Atlantique', '#herring-atlantic#', '#hareng-atlantique#', 'Fish', 'Poisson', 'Atlantic herring', 'Hareng de l\'Atlantique\r'),
(501, 'Atlantic mackerel', 'Maquereau de l\'Atlantique', '#mackerel-atlantic#', '#mackerel-atlantique#', 'Fish', 'Poisson', 'Atlantic mackerel', 'Maquereau de l\'Atlantique\r'),
(502, 'Painted comber', 'Mérou de l\'Atlantique', 'comber', 'combe', 'Fish', 'Poisson', 'Painted comber', 'Mérou de l\'Atlantique\r'),
(503, 'Atlantic pollock', 'Loup atlantique', '#pollock-atlantic#', '#pollock-atlantique#', 'Fish', 'Poisson', 'Atlantic pollock', 'Loup atlantique\r'),
(504, 'Atlantic wolffish', 'Loup atlantique', 'wolffish', 'loup de mer', 'Fish', 'Poisson', 'Atlantic wolffish', 'Loup atlantique\r'),
(505, 'Bamboo shoots', 'Pousses de bambou', 'bamboo-shoot', 'pousse de bambou', 'Plant', 'Plante', 'Bamboo shoots', 'Pousses de bambou\r'),
(506, 'Striped bass', 'Bar rayé', 'bass', 'bar', 'Fish', 'Poisson', 'Striped bass', 'Bar rayé\r'),
(507, 'Beaver', 'Castor', 'beaver', 'castor', 'Meat', 'Viande', 'Beaver', 'Castor\r'),
(508, 'Beech nut', 'Noix de hêtre', 'nut-beech', 'noix-hêtre', 'Nuts & Seed', 'Noix et graines', 'Beech nut', 'Noix de hêtre\r'),
(509, 'Beluga whale', 'Béluga', '#whale-beluga#', '#baleine-béluga', 'Fish', 'Poisson', 'Beluga whale', 'Béluga\r'),
(510, 'Bison', 'Bison', 'bison', 'bison', 'Meat', 'Viande', 'Bison', 'Bison\r'),
(511, 'Black bear', 'Ours noir', '#bear-black#', '#ours-noir', 'Meat', 'Viande', 'Black bear', 'Ours noir\r'),
(512, 'Alaska blackfish', 'Poisson noir d\'Alaska', 'blackfish', 'poisson noir', 'Fish', 'Poisson', 'Alaska blackfish', 'Poisson noir d\'Alaska\r'),
(513, 'Northern bluefin tuna', 'Thon rouge du Nord', '#tuna-bluefin#', '#thon rouge#', 'Fish', 'Poisson', 'Northern bluefin tuna', 'Thon rouge du Nord\r'),
(514, 'Bluefish', 'Poisson bleu', 'bluefish', 'poisson bleu', 'Fish', 'Poisson', 'Bluefish', 'Poisson bleu\r'),
(515, 'Wild boar', 'Sanglier', 'boar', 'sanglier', 'Meat', 'Viande', 'Wild boar', 'Sanglier\r'),
(516, 'Bowhead whale', 'Baleine boréale', '#whale-bowhead#', '#whale-bowhead#', 'Fish', 'Poisson', 'Bowhead whale', 'Baleine boréale\r'),
(517, 'Breadfruit', 'Fruit à pain', 'breadfruit', 'fruit à pain', 'Fruit', 'Fruit', 'Breadfruit', 'Fruit à pain\r'),
(519, 'Rapini', 'Rapini', 'rapini', 'rapini', 'Vegetable', 'Légumes', 'Rapini', 'Rapini\r'),
(520, 'Brown bear', 'Ours brun', '#bear-brown#', '#Ours brun', 'Meat', 'Viande', 'Brown bear', 'Ours brun\r'),
(521, 'Buffalo', 'Buffle', '#buffalo#', '#buffalo#', 'Meat', 'Viande', 'Buffalo', 'Buffle\r'),
(522, 'Burbot', 'lotte', 'burbot', 'lotte', 'Fish', 'Poisson', 'Burbot', 'lotte\r'),
(523, 'Giant butterbur', 'Pétoncle géant', 'butterbur', 'pétasite', 'Plant', 'Plante', 'Giant butterbur', 'Pétoncle géant\r'),
(524, 'American butterfish', 'Papillon d\'Amérique', 'butterfish', 'papillon', 'Fish', 'Poisson', 'American butterfish', 'Papillon d\'Amérique\r'),
(525, 'Butternut', 'Butternut', 'butternut', 'butternut', 'Nuts & Seed', 'Noix et graines', 'Butternut', 'Butternut\r'),
(526, 'Butternut squash', 'Courge musquée', 'butternut-squash; butternut-pumpkin', 'courge musquée ; potiron musqué', 'Fruit', 'Fruits', 'Butternut squash', 'Courge musquée\r'),
(527, 'Cardoon', 'Cardon', 'cardoon', 'cardon', 'Plant', 'Plante', 'Cardoon', 'Cardon\r'),
(528, 'Caribou', 'Caribou', 'caribou', 'caribou', 'Meat', 'Viande', 'Caribou', 'Caribou\r'),
(529, 'Natal plum', 'Prune du Natal', 'plum-natal', 'prune-natale', 'Fruit', 'Fruits', 'Natal plum', 'Prune du Natal\r'),
(530, 'Carob', 'Caroube', 'carob', 'caroube', 'Plant', 'Plante', 'Carob', 'Caroube\r'),
(531, 'Common carp', 'Carpe commune', 'carp', 'carpe', 'Fish', 'Poisson', 'Common carp', 'Carpe commune\r'),
(532, 'Channel catfish', 'Chien-chat de rivière', '#catfish-channel#', '#carpe-canal#', 'Fish', 'Poisson', 'Channel catfish', 'Chien-chat de rivière\r'),
(533, 'Chia', 'Chia', 'chia', 'chia', 'Herb', 'Herbe', 'Chia', 'Chia\r'),
(534, 'Chinese chestnut', 'Châtaigne chinoise', 'chestnut-chinese', 'châtaigne-chinoise', 'Nuts & Seed', 'Noix et graines', 'Chinese chestnut', 'Châtaigne chinoise\r'),
(535, 'Garland chrysanthemum', 'Chrysanthème de Garlande', 'garland-chrysanthemum', 'guirlande-chrysanthème', 'Flower', 'Fleur', 'Garland chrysanthemum', 'Chrysanthème de Garlande\r'),
(536, 'Cisco', 'Cisco', '#cisco#', '#cisco#', 'Fish', 'Poisson', 'Cisco', 'Cisco\r'),
(537, 'Nuttall cockle', 'Coque de Nuttall', 'nuttall-cockle', 'châtaigne-cockle', 'Fish', 'Poisson', 'Nuttall cockle', 'Coque de Nuttall\r'),
(538, 'Common octopus', 'Pieuvre commune', '#octopus#', '#octopus#', 'Seafood', 'Fruits de mer', 'Common octopus', 'Pieuvre commune\r'),
(539, 'Corn salad', 'Salade de maïs', 'salad-corn', 'salade-maïs', 'Vegetable', 'Légume', 'Corn salad', 'Salade de maïs\r'),
(540, 'Cottonseed', 'Graine de coton', 'cotton-seed', 'coton-graine', 'Plant', 'Plante', 'Cottonseed', 'Graine de coton\r'),
(541, 'Catjang pea', 'Pois Catjang', 'pea-catjang', 'pois-catjang', 'Legume', 'Légumineuse', 'Catjang pea', 'Pois Catjang\r'),
(542, 'Squashberry', 'Courge', 'squash-berry', 'courge-mûre', 'Fruit', 'Fruit', 'Squashberry', 'Courge\r'),
(543, 'Atlantic croaker', 'Croak de l\'Atlantique', '#croaker-atlantic#', '#croaker-atlantique#', 'Fish', 'Poisson', 'Atlantic croaker', 'Croak de l\'Atlantique\r'),
(544, 'Cusk', 'Brosme', 'cusk', 'brosme', 'Fish', 'Poisson', 'Cusk', 'Brosme\r'),
(545, 'Cuttlefish', 'Seiche', 'cuttle-fish', 'seiche-poisson', 'Fish', 'Poisson', 'Cuttlefish', 'Seiche\r'),
(546, 'Mule deer', 'Cerf mulet', '#mule deer#', '#cerf-mulet#', 'Meat', 'Viande', 'Mule deer', 'Cerf mulet\r'),
(547, 'Devilfish', 'Poisson diable', 'devil-fish', 'diable-poisson', 'Fish', 'Poisson', 'Devilfish', 'Poisson diable\r'),
(548, 'Dock', 'Dock', 'dock', 'quai', 'Herb', 'Herbe', 'Dock', 'Dock\r'),
(549, 'Dolphin fish', 'Poisson dauphin', 'dolphin', 'dauphin', 'Fish', 'Poisson', 'Dolphin fish', 'Poisson dauphin\r'),
(550, 'Freshwater drum', 'Tambour d\'eau douce', 'drum', 'tambour', 'Fish', 'Poisson', 'Freshwater drum', 'Tambour d\'eau douce\r'),
(551, 'Wild duck', 'Canard sauvage', 'duck', 'canard', 'Meat', 'Viande', 'Wild duck', 'Canard sauvage\r'),
(552, 'Freshwater eel', 'Anguille d\'eau douce', 'eel', 'anguille', 'Fish', 'Poisson', 'Freshwater eel', 'Anguille d\'eau douce\r'),
(553, 'Elk', 'Elan', '#elk#', '#elk#', 'Meat', 'Viande', 'Elk', 'Elan\r'),
(554, 'Emu', 'Émeu', 'emu', 'émeu', 'Meat', 'Viande', 'Emu', 'Émeu\r'),
(555, 'Oregon yampah', 'Yampah de l\'Oregon', 'oregon-yampah', 'oregon-yampah', 'Plant', 'Plante', 'Oregon yampah', 'Yampah de l\'Oregon\r'),
(556, 'European anchovy', 'Anchois d\'Europe', '#anchovy-european#', '#anchovy-european#', 'Fish', 'Poisson', 'European anchovy', 'Anchois d\'Europe\r'),
(557, 'European chestnut', 'Châtaigne européenne', 'chestnut-european', 'châtaigne-européenne', 'Nuts & Seed', 'Noix et graines', 'European chestnut', 'Châtaigne européenne\r'),
(558, 'Turbot', 'Turbot', 'turbot', 'turbot', 'Fish', 'Poisson', 'Turbot', 'Turbot\r'),
(559, 'Fireweed', 'Épilobe', 'fireweed', 'épilobe', 'Herb', 'Herbe', 'Fireweed', 'Épilobe\r'),
(560, 'Florida pompano', 'Pompano de Floride', 'pompano', 'pompano', 'Fish', 'Poisson', 'Florida pompano', 'Pompano de Floride\r'),
(561, 'Ginkgo nuts', 'Noix de ginkgo', 'nut-ginkgo', 'noix-ginkgo', 'Nuts & Seed', 'Noix et graines', 'Ginkgo nuts', 'Noix de ginkgo\r'),
(562, 'Greylag goose', 'Oie cendrée', 'goose', 'oie', 'Meat', 'Viande', 'Greylag goose', 'Oie cendrée\r'),
(563, 'Greenland halibut', 'Flétan du Groenland', '#halibut-greenland#', '#flétan du Groenland', 'Fish', 'Poisson', 'Greenland halibut', 'Flétan du Groenland\r'),
(564, 'Groundcherry', 'Groseille à maquereau', 'groundcherry', 'cerisier de terre', 'Fruit', 'Fruit', 'Groundcherry', 'Groseille à maquereau\r'),
(565, 'Grouper', 'Mérou', 'grouper', 'mérou', 'Fish', 'Poisson', 'Grouper', 'Mérou\r'),
(566, 'Haddock', 'Haddock', '#haddock#', '#haddock#', 'Fish', 'Poisson', 'Haddock', 'Haddock\r'),
(567, 'Hippoglossus', 'Hippoglosse', 'hippoglossus', 'hippoglosse', 'Fish', 'Poisson', 'Hippoglossus', 'Hippoglosse\r'),
(568, 'Horse', 'Cheval', 'horse', 'cheval', 'Meat', 'Viande', 'Horse', 'Cheval\r'),
(569, 'Hyacinth bean', 'Haricot jacinthe', 'bean-hyacinth', 'haricot-hyacinthe', 'Legume', 'Légumineuse', 'Hyacinth bean', 'Haricot jacinthe\r'),
(570, 'Irish moss', 'Mousse d\'Irlande', 'moss-irish', 'mousse-irish', 'Seafood', 'Fruits de mer', 'Irish moss', 'Mousse d\'Irlande\r'),
(571, 'Pacific jack mackerel', 'Maquereau du Pacifique', '#mackerel-pacific#', '#mackerel-pacifique#', 'Fish', 'Poisson', 'Pacific jack mackerel', 'Maquereau du Pacifique\r'),
(572, 'Japanese chestnut', 'Châtaigne du Japon', 'chestnut-japanese', 'châtaigne-japonaise', 'Nuts & Seed', 'Noix et graines', 'Japanese chestnut', 'Châtaigne du Japon\r'),
(573, 'Jerusalem artichoke', 'Topinambour', 'artichoke-jerusalen', 'artichaut-jérusalen', 'Vegetable', 'Légumes', 'Jerusalem artichoke', 'Topinambour\r'),
(574, 'Jujube', 'Jujube', 'jujube', 'jujube', 'Fruit', 'Fruit', 'Jujube', 'Jujube\r'),
(575, 'Jute', 'Jute', 'jute', 'jute', 'Plant', 'Plante', 'Jute', 'Jute\r'),
(576, 'Kale', 'Chou frisé', 'kale', 'chou frisé', 'Vegetable', 'Légumes', 'Kale', 'Chou frisé\r'),
(577, 'King mackerel', 'Maquereau royal', '#mackerel-king#', '#maquereau-roi#', 'Fish', 'Poisson', 'King mackerel', 'Maquereau royal\r'),
(578, 'Lambsquarters', 'Chénopode blanc', 'lambsquarters', 'chénopode blanc', 'Plant', 'Plante', 'Lambsquarters', 'Chénopode blanc\r'),
(579, 'Leather chiton', 'Chiton cuir', 'chiton', 'chiton', 'Seafood', 'Fruits de mer', 'Leather chiton', 'Chiton cuir\r'),
(580, 'Common ling', 'Lingue commune', 'ling', 'lingue', 'Fish', 'Poisson', 'Common ling', 'Lingue commune\r'),
(581, 'Lingcod', 'Morue-lingue', 'lingcod', 'morue-lingue', 'Fish', 'Poisson', 'Lingcod', 'Morue-lingue\r'),
(582, 'White lupine', 'Lupin blanc', 'lupine-white', 'lupin-blanc', 'Plant', 'Plante', 'White lupine', 'Lupin blanc\r'),
(583, 'Malabar spinach', 'Epinard de Malabar', 'spinach-malabar', 'épinard-malabar', 'Vegetable', 'Légumes', 'Malabar spinach', 'Epinard de Malabar\r'),
(584, 'Mammee apple', 'Pomme Mammee', 'apple-mammee', 'pomme-mammee', 'Fruit', 'Fruit', 'Mammee apple', 'Pomme Mammee\r'),
(585, 'Purple mangosteen', 'Mangoustan pourpre', 'mangosteen', 'mangoustan', 'Fruit', 'Fruit', 'Purple mangosteen', 'Mangoustan pourpre\r'),
(586, 'Alpine sweetvetch', 'Mélilot des Alpes', 'sweetvetch', 'mélilot', 'Plant', 'Plante', 'Alpine sweetvetch', 'Mélilot des Alpes\r'),
(587, 'Milkfish', 'Milkfish', 'milk-fish', 'lait-poisson', 'Fish', 'Poisson', 'Milkfish', 'Milkfish\r'),
(588, 'Monkfish', 'Baudroie', 'monk-fish', 'moine-poisson', 'Fish', 'Poisson', 'Monkfish', 'Baudroie\r'),
(589, 'Moose', 'Orignal', 'moose', 'élan', 'Meat', 'Viande', 'Moose', 'Orignal\r'),
(590, 'Moth bean', 'Haricot papillon', 'bean-moth', 'mite à haricots', 'Legume', 'Légumineuse', 'Moth bean', 'Haricot papillon\r'),
(591, 'Mountain yam', 'Moutarde de montagne', 'yam-mountain', 'igname-montagne', 'Vegetable', 'Légume', 'Mountain yam', 'Moutarde de montagne\r'),
(592, 'Striped mullet', 'Mouton rayé', 'mullet', 'mulet', 'Fish', 'Poisson', 'Striped mullet', 'Mouton rayé\r'),
(593, 'Muskrat', 'Rat musqué', 'muskrat', 'rat musqué', 'Meat', 'Viande', 'Muskrat', 'Rat musqué\r'),
(594, 'New Zealand spinach', 'Epinard de Nouvelle-Zélande', 'spinach-new-zealand', 'épinard-new-zealand', 'Vegetable', 'Végétale', 'New Zealand spinach', 'Epinard de Nouvelle-Zélande\r'),
(595, 'Nopal', 'Nopal', 'nopal', 'nopal', 'Plant', 'Plante', 'Nopal', 'Nopal\r'),
(596, 'Ocean pout', 'Tacaud de mer', '#pout-ocean#', '#pout-ocean#', 'Fish', 'Poisson', 'Ocean pout', 'Tacaud de mer\r'),
(597, 'North Pacific giant octopus', 'Pieuvre géante du Pacifique Nord', '#octopus-pacific#', '#octopus-pacific#', 'Seafood', 'Fruits de mer', 'North Pacific giant octopus', 'Pieuvre géante du Pacifique Nord\r'),
(598, 'Ohelo berry', 'Ohelo berry', 'berry-ohelo', 'baie-ohelo', 'Fruit', 'Fruit', 'Ohelo berry', 'Ohelo berry\r'),
(600, 'Opossum', 'Opossum', 'opossum', 'opossum', 'Meat', 'Viande', 'Opossum', 'Opossum\r'),
(601, 'Ostrich', 'Autruche', 'ostrich', 'autruche', 'Meat', 'Viande', 'Ostrich', 'Autruche\r'),
(602, 'Spotted seal', 'Phoque tacheté', '#seal-spotted#', '#seal-spotted#', 'Seafood', 'Fruits de mer', 'Spotted seal', 'Phoque tacheté\r'),
(603, 'Pacific herring', 'Hareng du Pacifique', '#herring-pacific#', '#hareng-pacifique#', 'Fish', 'Poisson', 'Pacific herring', 'Hareng du Pacifique\r'),
(604, 'Pacific rockfish', 'Sébaste du Pacifique', 'rockfish', 'sébaste', 'Fish', 'Poisson', 'Pacific rockfish', 'Sébaste du Pacifique\r'),
(606, 'Common persimmon', 'Kaki commun', '#persimmon-common#', '#persimmon-common#', 'Fruit', 'Fruit', 'Common persimmon', 'Kaki commun\r'),
(607, 'Pheasant', 'Faisan', 'pheasant', 'faisan', 'Meat', 'Viande', 'Pheasant', 'Faisan\r'),
(608, 'Northern pike', 'Brochet du Nord', 'pike-northern', 'brochet-nord', 'Fish', 'Poisson', 'Northern pike', 'Brochet du Nord\r'),
(609, 'Pili nut', 'Noix de pili', 'nut-pili', 'noix-pili', 'Nuts & Seed', 'Noix et graines', 'Pili nut', 'Noix de pili\r'),
(610, 'Colorado pinyon', 'Pinsons du Colorado', 'pinyon', 'pinyon', 'Plant', 'Plantes', 'Colorado pinyon', 'Pinsons du Colorado\r'),
(611, 'Pitanga', 'Pitanga', 'pitanga', 'pitanga', 'Fruit', 'Fruit', 'Pitanga', 'Pitanga\r'),
(612, 'French plantain', 'Plantain français', 'plantain-french', 'plantain-français', 'Plant', 'Plante', 'French plantain', 'Plantain français\r'),
(613, 'American pokeweed', 'Pokeweed américain', 'pokeweed-american', 'pokeweed-american', 'Herb', 'Herbe', 'American pokeweed', 'Pokeweed américain\r'),
(614, 'Polar bear', 'Ours polaire', '#bear-polar#', '#ours-polaire', 'Meat', 'Viande', 'Polar bear', 'Ours polaire\r'),
(615, 'Prairie turnip', 'Navet des prairies', 'turnip-prairie', 'navet-prairie', 'Vegetable', 'Légume', 'Prairie turnip', 'Navet des prairies\r'),
(616, 'Quinoa', 'Quinoa', 'quinoa', 'quinoa', 'Cereal', 'Céréales', 'Quinoa', 'Quinoa\r'),
(617, 'European rabbit', 'Lapin d\'Europe', '#rabbit-european#', '#Lapin-européen#', 'Meat', 'Viande', 'European rabbit', 'Lapin d\'Europe\r'),
(618, 'Raccoon', 'Raton laveur', 'raccoon', 'raton laveur', 'Meat', 'Viande', 'Raccoon', 'Raton laveur\r'),
(619, 'Rainbow smelt', 'Éperlan arc-en-ciel', '#smelt-rainbow#', '#smelt-rainbow#', 'Fish', 'Poisson', 'Rainbow smelt', 'Éperlan arc-en-ciel\r'),
(620, 'Rainbow trout', 'Truite arc-en-ciel', '#trout-rainbow#', '#trout-arc-en-ciel#', 'Fish', 'Poisson', 'Rainbow trout', 'Truite arc-en-ciel\r'),
(621, 'Malabar plum', 'Prune de Malabar', 'plum-malabar', 'prune-malabar', 'Fruit', 'Fruit', 'Malabar plum', 'Prune de Malabar\r'),
(622, 'Rose hip', 'Cynorhodon', 'Rose-hip', 'Cynorhodon', 'Fruit', 'Fruit', 'Rose hip', 'Cynorhodon\r'),
(623, 'Roselle', 'Roselle', 'roselle', 'roselle', 'Herb', 'Herbe', 'Roselle', 'Roselle\r'),
(624, 'Orange roughy', 'Hoplostète orange', 'orange-roughy', 'orange-rugueux', 'Fish', 'Poisson', 'Orange roughy', 'Hoplostète orange\r'),
(625, 'Sablefish', 'Morue charbonnière', 'sable-fish', 'zibeline-poisson', 'Fish', 'Poisson', 'Sablefish', 'Morue charbonnière\r'),
(626, 'Pink salmon', 'Saumon rose', '#salmon-pink#', '#salmon-pink#', 'Fish', 'Poisson', 'Pink salmon', 'Saumon rose\r'),
(627, 'Chum salmon', 'Saumon kéta', '#salmon-chum#', '#salmon-chum#', 'Fish', 'Poisson', 'Chum salmon', 'Saumon kéta\r'),
(628, 'Coho salmon', 'Le saumon coho', '#salmon-coho#', '#saumon-coho#', 'Fish', 'Poisson', 'Coho salmon', 'Le saumon coho\r'),
(629, 'Sockeye salmon', 'Saumon sockeye', '#salmon-sockeye#', '#saumon-sockeye#', 'Fish', 'Poisson', 'Sockeye salmon', 'Saumon sockeye\r'),
(630, 'Chinook salmon', 'Le saumon Chinook', '#salmon-chinook#', '#saumon-chinook#', 'Fish', 'Poisson', 'Chinook salmon', 'Le saumon Chinook\r'),
(631, 'Atlantic salmon', 'Saumon atlantique', '#salmon-atlantic#', '#saumon-atlantique#', 'Fish', 'Poisson', 'Atlantic salmon', 'Saumon atlantique\r'),
(632, 'Salmonberry', 'Salmonberry', 'salmon-berry', 'saumon-mûre', 'Fruit', 'Fruit', 'Salmonberry', 'Salmonberry\r'),
(633, 'Common salsify', 'Salsifis commun', 'salsify', 'salsifis', 'Plant', 'Plante', 'Common salsify', 'Salsifis commun\r'),
(634, 'Spanish mackerel', 'Maquereau espagnol', '#mackerel-spanish#', '#mackerel-espagnol#', 'Fish', 'Poisson', 'Spanish mackerel', 'Maquereau espagnol\r'),
(635, 'Pacific sardine', 'Sardine du Pacifique', 'sardine', 'sardine', 'Fish', 'Poisson', 'Pacific sardine', 'Sardine du Pacifique\r'),
(636, 'Scup', 'Morue charbonnière', 'scup', 'scup', 'Fish', 'Poisson', 'Scup', 'Morue charbonnière\r'),
(637, 'Sea cucumber', 'Concombre de mer', 'cucumber-sea', 'concombre-mer', 'Seafood', 'Fruits de mer', 'Sea cucumber', 'Concombre de mer\r'),
(638, 'Steller sea lion', 'Otarie de Steller', 'sea-lion', 'oignon de mer', 'Seafood', 'Fruits de mer', 'Steller sea lion', 'Otarie de Steller\r'),
(639, 'Bearded seal', 'Phoque barbu', '#seal-bearded#', '#phoque barbu#', 'Seafood', 'Fruits de mer', 'Bearded seal', 'Phoque barbu\r'),
(640, 'Ringed seal', 'Phoque annelé', '#seal-ringed#', '#phoque annelé#', 'Seafood', 'Fruits de mer', 'Ringed seal', 'Phoque annelé\r'),
(641, 'Sea trout', 'Truite de mer', '#trout-sea#', '#trout-mer#', 'Fish', 'Poisson', 'Sea trout', 'Truite de mer\r'),
(642, 'Sesbania flower', 'Fleur de Sesbania', 'sesbania-flower', 'fleur de sesbania', 'Flower', 'Fleur', 'Sesbania flower', 'Fleur de Sesbania\r'),
(643, 'American shad', 'Alose savoureuse', 'shad', 'alose', 'Fish', 'Poisson', 'American shad', 'Alose savoureuse\r'),
(644, 'Shark', 'Requin', 'shark', 'requin', 'Fish', 'Poisson', 'Shark', 'Requin\r'),
(645, 'Sheefish', 'Poisson de fond', 'sheefish', 'sheefish', 'Fish', 'Poisson', 'Sheefish', 'Poisson de fond\r'),
(646, 'Sheepshead', 'Tête de mouton', 'sheepshead', 'tête de mouette', 'Fish', 'Poisson', 'Sheepshead', 'Tête de mouton\r'),
(647, 'Hedge mustard', 'Moutarde des haies', 'mustard-hedge', 'moutarde-hedge', 'Vegetable', 'Légumes', 'Hedge mustard', 'Moutarde des haies\r'),
(648, 'Snapper', 'Vivaneau', 'snapper', 'vivaneau', 'Fish', 'Poisson', 'Snapper', 'Vivaneau\r'),
(649, 'Spelt', 'Epeautre', 'spelt', 'épeautre', 'Cereal', 'Céréales', 'Spelt', 'Epeautre\r'),
(650, 'Spirulina', 'Spiruline', 'spirulina', 'spiruline', 'Additive', 'Additif', 'Spirulina', 'Spiruline\r'),
(651, 'Squab', 'Écureuil', 'squab', 'pigeonneau', 'Meat', 'Viande', 'Squab', 'Écureuil\r'),
(652, 'Squirrel', 'Écureuil', 'squirrel', 'écureuil', 'Meat', 'Viande', 'Squirrel', 'Écureuil\r'),
(653, 'Greater sturgeon', 'Grand esturgeon', '#sturgeon-greater#', '#esturgeon-gros', 'Fish', 'Poisson', 'Greater sturgeon', 'Grand esturgeon\r'),
(654, 'White sucker', 'Meunier noir', 'sucker-white', 'meunier-blanc', 'Fish', 'Poisson', 'White sucker', 'Meunier noir\r'),
(655, 'Pumpkinseed sunfish', 'Crapet sac-à-lait', 'sunfish', 'crapet-soleil', 'Fish', 'Poisson', 'Pumpkinseed sunfish', 'Crapet sac-à-lait\r'),
(656, 'Swordfish', 'Espadon', 'swordfish', 'espadon', 'Fish', 'Poisson', 'Swordfish', 'Espadon\r'),
(657, 'Taro', 'Taro', 'taro', 'taro', 'Vegetable', 'Légumes', 'Taro', 'Taro\r'),
(658, 'Teff', 'Teff', 'teff', 'teff', 'Herb', 'Herbe', 'Teff', 'Teff\r'),
(659, 'Tilefish', 'Tilefish', 'tile-fish', 'tuile-poisson', 'Fish', 'Poisson', 'Tilefish', 'Tilefish\r'),
(660, 'Mexican groundcherry', 'Cachemire mexicain', 'groundcherry-mexican', 'cerise de terre-mexicaine', 'Fruit', 'Fruit', 'Mexican groundcherry', 'Cachemire mexicain\r'),
(661, 'Towel gourd', 'Courge de serviette', 'gourd-towel', 'gourde-towel', 'Vegetable', 'Légumes', 'Towel gourd', 'Courge de serviette\r'),
(662, 'Salmonidae', 'Salmonidae', '#salmonidae#', '#salmonidae#', 'Fish', 'Poisson', 'Salmonidae', 'Salmonidae\r'),
(663, 'Walleye', 'Doré jaune', 'walleye', 'doré jaune', 'Fish', 'Poisson', 'Walleye', 'Doré jaune\r'),
(664, 'Alaska pollock', 'Colin d\'Alaska', '#pollock-alaska#', '#pollock-alaska#', 'Fish', 'Poisson', 'Alaska pollock', 'Colin d\'Alaska\r'),
(665, 'Whelk', 'Buccin', 'whelk', 'buccin', 'Seafood', 'Fruits de mer', 'Whelk', 'Buccin\r'),
(667, 'Broad whitefish', 'Corégone large', '#whitefish-broad#', '#poisson-blanc-rouge#', 'Fish', 'Poisson', 'Broad whitefish', 'Corégone large\r'),
(668, 'Whitefish', 'Corégone', '#whitefish#', '#poissonblanc#', 'Fish', 'Poisson', 'Whitefish', 'Corégone\r'),
(669, 'Whiting', 'Merlan', '#whiting#', '#whiting#', 'Fish', 'Poisson', 'Whiting', 'Merlan\r'),
(670, 'Wild rice', 'Riz sauvage', 'rice-wild', 'riz sauvage', 'Cereal', 'Céréales', 'Wild rice', 'Riz sauvage\r'),
(671, 'Tea leaf willow', 'Saule à feuilles de thé', 'tea-leaf-willow', 'thé-feuille de saule', 'Herb', 'Herbe', 'Tea leaf willow', 'Saule à feuilles de thé\r'),
(672, 'Winged bean', 'Haricot ailé', 'bean-winged', 'haricot à ailes', 'Legume', 'Légumineuse', 'Winged bean', 'Haricot ailé\r'),
(673, 'Yam', 'Igname', 'yam', 'igname', 'Vegetable', 'Légume', 'Yam', 'Igname\r'),
(674, 'Jicama', 'Jicama', 'jicama', 'jicama', 'Vegetable', 'Légume', 'Jicama', 'Jicama\r'),
(675, 'Yautia', 'Yautia', 'yautia', 'yautia', 'Plant', 'Plante', 'Yautia', 'Yautia\r'),
(676, 'Yellowfin tuna', 'Thon à nageoires jaunes', '#tuna-yellowfin#', '#tuna-yellowfin#', 'Fish', 'Poisson', 'Yellowfin tuna', 'Thon à nageoires jaunes\r'),
(677, 'Yellowtail amberjack', 'Sériole à queue jaune', 'amberjack', 'sériole', 'Fish', 'Poisson', 'Yellowtail amberjack', 'Sériole à queue jaune\r'),
(678, 'Pollock', 'Goberge', '#pollock#', '#pollock#', 'Fish', 'Poisson', 'Pollock', 'Goberge\r'),
(679, 'Albacore tuna', 'Thon albacore', '#tuna-albacore#', '#tuna-albacore#', 'Fish', 'Poisson', 'Albacore tuna', 'Thon albacore\r'),
(680, 'Atlantic halibut', 'Flétan de l\'Atlantique', '#halibut-atlantic#', '#halibut-atlantique#', 'Fish', 'Poisson', 'Atlantic halibut', 'Flétan de l\'Atlantique\r'),
(682, 'Smelt', 'Éperlan', '#smelt#', '#éperlan#', 'Fish', 'Poisson', 'Smelt', 'Éperlan\r'),
(683, 'Clupeinae', 'Clupeinae', 'clupeinae', 'clupeinae', 'Fish', 'Poisson', 'Clupeinae', 'Clupeinae\r'),
(684, 'Spiny lobster', 'Homard épineux', '#lobster-spiny#', '#lobster-spiny#', 'Seafood', 'Fruits de mer', 'Spiny lobster', 'Homard épineux\r'),
(685, 'Black-eyed pea', 'Poison aux yeux noirs', 'pea-black-eyed', 'pois noir-yeux', 'Legume', 'Légumineuses', 'Black-eyed pea', 'Poison aux yeux noirs\r'),
(686, 'Deer', 'Chevreuil', '#deer#', '#deer#', 'Meat', 'Viande', 'Deer', 'Chevreuil\r'),
(687, 'Percoidei', 'Percoidei', 'percoidei', 'percoidei', 'Fish', 'Poisson', 'Percoidei', 'Percoidei\r'),
(688, 'Perciformes', 'Perciformes', 'perciformes', 'perciformes', 'Fish', 'Poisson', 'Perciformes', 'Perciformes\r'),
(690, 'Rabbit', 'Lapin', '#rabbit#', '#lapin#', 'Meat', 'Viande', 'Rabbit', 'Lapin\r'),
(691, 'Beefalo', 'Bœuf', 'beefalo', 'bœuf', 'Meat', 'Viande', 'Beefalo', 'Bœuf\r'),
(693, 'Bivalvia', 'Bivalvia', 'bivalvia', 'bivalves', 'Seafood', 'Fruits de mer', 'Bivalvia', 'Bivalvia\r'),
(694, 'Flatfish', 'Poissons plats', 'flatfish', 'poisson plat', 'Fish', 'Poisson', 'Flatfish', 'Poissons plats\r'),
(695, 'Walrus', 'Morses', 'walrus', 'morse', 'Seafood', 'Fruits de mer', 'Walrus', 'Morses\r'),
(696, 'Alaska wild rhubarb', 'Rhubarbe sauvage d\'Alaska', 'rhubarb-alaska', 'rhubarbe-alaska', 'Plant', 'Plantes', 'Alaska wild rhubarb', 'Rhubarbe sauvage d\'Alaska\r'),
(697, 'Oriental wheat', 'Blé oriental', 'wheat-oriental', 'blé-oriental', 'Cereal', 'Céréales', 'Oriental wheat', 'Blé oriental\r'),
(698, 'Yardlong bean', 'Haricot long', 'bean-yardlong', 'haricot-long', 'Legume', 'Légumineuse', 'Yardlong bean', 'Haricot long\r'),
(699, 'Great horned owl', 'Grand-duc d\'Amérique', 'owl', 'hibou', 'Meat', 'Viande', 'Great horned owl', 'Grand-duc d\'Amérique\r'),
(700, 'Quail', 'Caille', 'quail', 'caille', 'Meat', 'Viande', 'Quail', 'Caille\r'),
(701, 'Boysenberry', 'Mûre de Boysen', 'boysenberry', 'mûre de Boysen', 'Fruit', 'Fruit', 'Boysenberry', 'Mûre de Boysen\r'),
(702, 'Rowal', 'Rowal', 'rowal', 'rollier', 'Plant', 'Plante', 'Rowal', 'Rowal\r'),
(703, 'Jew\'s ear', 'Oreille de juif', 'ear-jewâ€™s', 'oreille-jewâ€™s', 'Fungus', 'Champignon', 'Jew\'s ear', 'Oreille de juif\r'),
(704, 'Shiitake', 'Shiitake', 'shiitake', 'shiitake', 'Fungus', 'Champignon', 'Shiitake', 'Shiitake\r'),
(705, 'Purple laver', 'Lavande pourpre', 'laver', 'laver', 'Seafood', 'Fruits de mer', 'Purple laver', 'Lavande pourpre\r'),
(706, 'Wakame', 'Wakame', 'wakame', 'wakame', 'Seafood', 'Fruits de mer', 'Wakame', 'Wakame\r'),
(707, 'Enokitake', 'Enokitake', 'enokitake', 'enokitake', 'Fungus', 'Champignon', 'Enokitake', 'Enokitake\r'),
(708, 'Epazote', 'Epazote', 'epazote', 'épazote', 'Herb', 'Herbe', 'Epazote', 'Epazote\r'),
(709, 'Oyster mushroom', 'Pleurote en huître', 'mushroom-oyster', 'champignon-huître', 'Fungus', 'Champignon', 'Oyster mushroom', 'Pleurote en huître\r'),
(710, 'Cloud ear fungus', 'Champignon de l\'oreille des nuages', 'ear-cloud', 'oreille-nuage', 'Fungus', 'Champignon', 'Cloud ear fungus', 'Champignon de l\'oreille des nuages\r'),
(711, 'Maitake', 'Maitake', 'maitake', 'maitake', 'Fungus', 'Champignon', 'Maitake', 'Maitake\r'),
(712, 'Ostrich fern', 'Fougère d\'autruche', 'ostrich-fern', 'fougère-autruche', 'Plant', 'Plante', 'Ostrich fern', 'Fougère d\'autruche\r'),
(713, 'Spot croaker', 'Croque-mouche', '#croaker-spot#', '#croaker-spot#', 'Fish', 'Poisson', 'Spot croaker', 'Croque-mouche\r'),
(714, 'Sourdock', 'Sourdock', 'sourdock', 'sourdock', 'Herb', 'Herbe', 'Sourdock', 'Sourdock\r'),
(715, 'Tinda', 'Tinda', 'tinda', 'tinda', 'Vegetable', 'Végétal', 'Tinda', 'Tinda\r'),
(716, 'Atlantic menhaden', 'Menhaden de l\'Atlantique', 'menhaden', 'menhaden', 'Fish', 'Poisson', 'Atlantic menhaden', 'Menhaden de l\'Atlantique\r'),
(717, 'Agave', 'Agave', 'agave', 'agave', 'Plant', 'Plante', 'Agave', 'Agave\r'),
(718, 'Narrowleaf cattail', 'Quenouille à feuilles étroites', 'cattail', 'quenouille', 'Herb', 'Herbe', 'Narrowleaf cattail', 'Quenouille à feuilles étroites\r'),
(719, 'Jellyfish', 'Méduse', 'jellyfish', 'méduse', 'Seafood', 'Fruits de mer', 'Jellyfish', 'Méduse\r'),
(720, 'Anchovy', 'Anchois', '#anchovy#', '#anchovy#', 'Fish', 'Poisson', 'Anchovy', 'Anchois\r'),
(721, 'Blue whiting', 'Merlan bleu', '#whiting-blue#', '#blanc-bleu#', 'Fish', 'Poisson', 'Blue whiting', 'Merlan bleu\r'),
(722, 'Carp bream', 'Carpe d\'eau douce', 'bream', 'brème', 'Fish', 'Poisson', 'Carp bream', 'Carpe d\'eau douce\r'),
(723, 'Chanterelle', 'Chanterelle', 'chanterelle', 'chanterelle', 'Fungus', 'Champignon', 'Chanterelle', 'Chanterelle\r'),
(724, 'Sturgeon', 'Esturgeon', '#sturgeon#', '#Esturgeon#', 'Fish', 'Poisson', 'Sturgeon', 'Esturgeon\r'),
(725, 'Charr', 'Omble', 'charr', 'omble', 'Fish', 'Poisson', 'Charr', 'Omble\r'),
(726, 'Common dab', 'Limande commune', 'dab', 'limande', 'Fish', 'Poisson', 'Common dab', 'Limande commune\r'),
(727, 'Spiny dogfish', 'Aiguillat commun', 'dogfish', 'roussette', 'Fish', 'Poisson', 'Spiny dogfish', 'Aiguillat commun\r'),
(728, 'Anatidae', 'Anatidae', 'anatidae', 'anatidae', 'Meat', 'Viande', 'Anatidae', 'Anatidae\r'),
(729, 'Anguilliformes', 'Anguilliformes', 'anguilliformes', 'anguilliformes', 'Fish', 'Poisson', 'Anguilliformes', 'Anguilliformes\r'),
(730, 'True frog', 'Grenouille véritable', 'frog', 'grenouille', 'Meat', 'Viande', 'True frog', 'Grenouille véritable\r'),
(731, 'Garfish', 'Garfish', 'garfish', 'orphie', 'Fish', 'Poisson', 'Garfish', 'Garfish\r'),
(732, 'Gadiformes', 'Gadiformes', 'gadiformes', 'gadiformes', 'Fish', 'Poisson', 'Gadiformes', 'Gadiformes\r'),
(733, 'Mountain hare', 'Lièvre variable', 'hare', 'lièvre', 'Meat', 'Viande', 'Mountain hare', 'Lièvre variable\r'),
(734, 'Lake trout', 'Truite grise', '#trout-lake#', '#trout-lake#', 'Fish', 'Poisson', 'Lake trout', 'Truite grise\r'),
(735, 'Lemon sole', 'Limande sole', 'lemon-sole', 'semelle de citron', 'Fish', 'Poisson', 'Lemon sole', 'Limande sole\r'),
(736, 'Lumpsucker', 'Lumpsucker', 'lumpsucker', 'lombricomane', 'Fish', 'Poisson', 'Lumpsucker', 'Lumpsucker\r'),
(737, 'Scombridae', 'Scombridae', 'scombridae', 'scombridae', 'Fish', 'Poisson', 'Scombridae', 'Scombridae\r'),
(738, 'Norway haddock', 'Églefin de Norvège', '#haddock-norway#', '#haddock-norway#', 'Fish', 'Poisson', 'Norway haddock', 'Églefin de Norvège\r'),
(739, 'Norway pout', 'Tacaud norvégien', '#pout-norway#', '#pout-norway#', 'Fish', 'Poisson', 'Norway pout', 'Tacaud norvégien\r'),
(740, 'Oil palm', 'Palmier à huile', 'palm-oil', 'huile de palme', 'Plant', 'Plante', 'Oil palm', 'Palmier à huile\r'),
(741, 'Sago palm', 'Palme de sagou', 'palm-sago', 'palmier-sago', 'Plant', 'Plante', 'Sago palm', 'Palme de sagou\r'),
(742, 'Persimmon', 'Persimmon', '#persimmon#', '#persimmon#', 'Fruit', 'Fruit', 'Persimmon', 'Persimmon\r'),
(743, 'Pikeperch', 'Sandre', '#perch-pike#', '#perch-pike#', 'Fish', 'Poisson', 'Pikeperch', 'Sandre\r'),
(744, 'Pleuronectidae', 'Pleuronectidae', 'pleuronectidae', 'pleuronectidae', 'Fish', 'Poisson', 'Pleuronectidae', 'Pleuronectidae\r'),
(745, 'Rock ptarmigan', 'Lagopède des rochers', 'ptarmigan', 'lagopède', 'Meat', 'Viande', 'Rock ptarmigan', 'Lagopède des rochers\r'),
(746, 'Pacific ocean perch', 'Perche du Pacifique', '#perch-pacific#', '#perche-pacifique#', 'Fish', 'Poisson', 'Pacific ocean perch', 'Perche du Pacifique\r'),
(747, 'Black salsify', 'Salsifis noir', 'salsify-black', 'salsifis noir', 'Plant', 'Plante', 'Black salsify', 'Salsifis noir\r'),
(748, 'True seal', 'Phoque véritable', '#seal#', '#phoque#', 'Seafood', 'Fruits de mer', 'True seal', 'Phoque véritable\r'),
(749, 'Red algae', 'Algues rouges', 'algae-red', 'algue-rouge', 'Seafood', 'Fruits de mer', 'Red algae', 'Algues rouges\r'),
(750, 'Kombu', 'Kombu', 'kombu', 'kombu', 'Seafood', 'Fruits de mer', 'Kombu', 'Kombu\r'),
(751, 'Snail', 'Escargot', 'snail', 'escargot', 'Meat', 'Viande', 'Snail', 'Escargot\r'),
(752, 'True sole', 'Vraie sole', 'sole', 'sole', 'Fish', 'Poisson', 'True sole', 'Vraie sole\r'),
(753, 'Catfish', 'Poisson-chat', '#catfish#', '#poisson-chat', 'Fish', 'Poisson', 'Catfish', 'Poisson-chat\r'),
(754, 'Thistle', 'Chardon', 'thistle', 'chardon', 'Plant', 'Plantes', 'Thistle', 'Chardon\r'),
(755, 'Common Tuna', 'Thon commun', '#tuna#', '#thon', 'Fish', 'Poisson', 'Common Tuna', 'Thon commun\r'),
(756, 'Cetacea', 'Cetacea', 'cetacea', 'cétacés', 'Fish', 'Poisson', 'Cetacea', 'Cetacea\r'),
(757, 'Columbidae', 'Columbidae', 'columbidae', 'columbidae', 'Meat', 'Viande', 'Columbidae', 'Columbidae\r'),
(758, 'Conch', 'Conque', 'conch', 'conque', 'Fish', 'Poisson', 'Conch', 'Conque\r'),
(759, 'Berry wine', 'Vin de baies', 'berry-wine', 'baies-vin', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Berry wine', 'Vin de baies\r'),
(762, 'Vodka', 'Vodka', 'vodka', 'vodka', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Vodka', 'Vodka\r'),
(763, 'Ice cream', 'Glace', 'ice cream', 'crème glacée', 'Dish', 'Plat', 'Ice cream', 'Glace\r'),
(764, 'Vermouth', 'Vermouth', 'vermouth', 'vermouth', 'Beverage Alcoholic', 'Boissons alcoolisées', 'Vermouth', 'Vermouth\r'),
(765, 'Madeira wine', 'Vin de Madère', 'madeira', 'madère', 'Beverage Alcoholic', 'Boisson alcoolisée', 'Madeira wine', 'Vin de Madère\r'),
(766, 'Nougat', 'Nougat', 'nougat', 'nougat', 'Dish', 'Plat', 'Nougat', 'Nougat\r'),
(767, 'Toffee', 'Caramel', 'toffee', 'caramel', 'Dish', 'Plat', 'Toffee', 'Caramel\r'),
(768, 'Cake', 'Gâteau', 'cake', 'gâteau', 'Dish', 'Plat', 'Cake', 'Gâteau\r'),
(769, 'Pizza', 'Pizza', 'pizza', 'pizza', 'Dish', 'Plat', 'Pizza', 'Pizza\r'),
(770, 'Ymer', 'Ymer', 'ymer', 'ymer', 'Dairy', 'Produits laitiers', 'Ymer', 'Ymer\r'),
(772, 'Pastry', 'Pâtisserie', 'pastry', 'pâtisserie', 'Dish', 'Assiette', 'Pastry', 'Pâtisserie\r'),
(773, 'DragÌ©e', 'DragÌ©e', 'dragÌ©e', 'dragÌ©e', 'Dish', 'Plat', 'DragÌ©e', 'DragÌ©e\r'),
(774, 'Chewing gum', 'Chewing-gum', 'chewing gum', 'chewing-gum', 'Dish', 'Plat', 'Chewing gum', 'Chewing-gum\r'),
(775, 'Marzipan', 'Massepain', 'marzipan', 'pâte d\'amande', 'Dish', 'Plat', 'Marzipan', 'Massepain\r'),
(776, 'Salad dressing', 'Sauce salade', 'salad dressing', 'vinaigrette', 'Dish', 'Plat', 'Salad dressing', 'Sauce salade\r'),
(778, 'Salt', 'Sel', 'salt', 'sel', 'Additive', 'Additif', 'Salt', 'Sel\r'),
(780, 'Cream', 'Crème', 'cream', 'crème', 'Dairy', 'Produits laitiers', 'Cream', 'Crème\r'),
(781, 'Sugar', 'Sucre', 'sugar', 'sucre', 'Additive', 'Additif', 'Sugar', 'Sucre\r'),
(782, 'Sausage', 'Saucisse', 'sausage', 'saucisse', 'Dish', 'Vaisselle', 'Sausage', 'Saucisse\r'),
(783, 'Meatball', 'Boulette de viande', 'meatball', 'boulette de viande', 'Dish', 'Dish', 'Meatball', 'Boulette de viande\r'),
(784, 'Pate', 'Pate', 'pate', 'pâté', 'Dish', 'Vaisselle', 'Pate', 'Pate\r'),
(786, 'Meat bouillon', 'Bouillon de viande', 'bouillon', 'bouillon', 'Dish', 'Plat', 'Meat bouillon', 'Bouillon de viande\r'),
(788, 'Whey', 'Lactosérum', 'whey', 'petit-lait', 'Dairy', 'Produits laitiers', 'Whey', 'Lactosérum\r'),
(789, 'Casein', 'Caséine', 'casein', 'caséine', 'Additive', 'Additif', 'Casein', 'Caséine\r'),
(791, 'Leavening agent', 'Agent levant', 'leavening agent', 'agent levant', 'Additive', 'Additif', 'Leavening agent', 'Agent levant\r'),
(792, 'Marshmallow', 'Guimauve', 'marshmallow', 'guimauve', 'Bakery', 'Boulangerie', 'Marshmallow', 'Guimauve\r'),
(793, 'Gelatin', 'Gélatine', 'gelatin', 'gélatine', 'Additive', 'Additif', 'Gelatin', 'Gélatine\r'),
(794, 'Water', 'Eau', 'water; ice', 'eau ; glace', 'Additive', 'Additif', 'Water', 'Eau\r'),
(796, 'Milk Human', 'Lait Humain', 'human milk', 'lait humain', 'Dairy', 'Produits laitiers', 'Milk Human', 'Lait Humain\r'),
(799, 'Dumpling', 'Boulette', 'dumpling', 'boulette', 'Dish', 'Vaisselle', 'Dumpling', 'Boulette\r'),
(800, 'Soup', 'Soupe', 'soup', 'soupe', 'Dish', 'Plats', 'Soup', 'Soupe\r'),
(803, 'Syrup', 'Sirop', 'syrup', 'sirop', 'Additive', 'Additif', 'Syrup', 'Sirop\r'),
(805, 'Remoulade', 'Rémoulade', 'remoulade', 'rémoulade', 'Dish', 'Vaisselle', 'Remoulade', 'Rémoulade\r'),
(806, 'Chocolate spread', 'Pâte à tartiner au chocolat', 'chocolate spread', 'pâte à tartiner au chocolat', 'Plant', 'Plante', 'Chocolate spread', 'Pâte à tartiner au chocolat\r'),
(807, 'Fruit gum', 'Gomme aux fruits', 'fruit gum', 'gomme de fruit', 'Dish', 'Plats cuisinés', 'Fruit gum', 'Gomme aux fruits\r'),
(810, 'Meringue', 'Meringue', 'meringue', 'meringue', 'Bakery', 'Boulangerie', 'Meringue', 'Meringue\r'),
(814, 'Cocoa butter', 'Beurre de cacao', 'cocoa butter', 'beurre de cacao', 'Plant', 'Plante', 'Cocoa butter', 'Beurre de cacao\r'),
(815, 'Cocoa powder', 'Poudre de cacao', 'cocoa-powder', 'poudre de cacao', 'Plant', 'Usine', 'Cocoa powder', 'Poudre de cacao\r'),
(817, 'Chocolate', 'Chocolat', 'chocolate', 'chocolat', 'Plant', 'Usine', 'Chocolate', 'Chocolat\r'),
(818, 'Hot chocolate', 'Chocolat chaud', 'chocolate-hot; chocolate-heated', 'chocolat-chaud ; chocolat-chauffé', 'Beverage', 'Boissons', 'Hot chocolate', 'Chocolat chaud\r'),
(820, 'Kefir', 'Kéfir', 'kefir', 'kéfir', 'Dairy', 'Produits laitiers', 'Kefir', 'Kéfir\r'),
(822, 'Miso', 'Miso', 'miso', 'miso', 'Additive', 'Additif', 'Miso', 'Miso\r'),
(823, 'Tofu', 'Tofu', 'tofu', 'tofu', 'Plant', 'Usine', 'Tofu', 'Tofu\r'),
(824, 'Zwieback', 'Zwieback', 'zwieback', 'zwieback', 'Dish', 'Vaisselle', 'Zwieback', 'Zwieback\r'),
(825, 'Roe', 'Roe', 'roe', 'œufs de poisson', 'Fish', 'Poisson', 'Roe', 'Roe\r'),
(826, 'Cichlidae', 'Cichlidae', 'cichlidae', 'cichlidés', 'Fish', 'Poisson', 'Cichlidae', 'Cichlidae\r'),
(827, 'Icing', 'Glaçage', 'icing', 'glaçage', 'Additive', 'Additif', 'Icing', 'Glaçage\r'),
(828, 'Snack bar', 'Snack bar', 'snack bar', 'snack', 'Dish', 'Plats cuisinés', 'Snack bar', 'Snack bar\r'),
(829, 'Green turtle', 'Tortue verte', 'turtle', 'tortue', 'Meat', 'Viande', 'Green turtle', 'Tortue verte\r'),
(831, 'Burrito', 'Burrito', 'burrito', 'burrito', 'Dish', 'Plats cuisinés', 'Burrito', 'Burrito\r'),
(832, 'Hamburger', 'Hamburger', 'hamburger', 'hamburger', 'Dish', 'Plat', 'Hamburger', 'Hamburger\r'),
(834, 'Taco', 'Taco', 'taco', 'taco', 'Dish', 'Plat', 'Taco', 'Taco\r'),
(835, 'Tortilla', 'Tortilla', 'tortilla', 'tortilla', 'Dish', 'Assiette', 'Tortilla', 'Tortilla\r'),
(836, 'Nachos', 'Nachos', 'nachos', 'nachos', 'Dish', 'Assiette', 'Nachos', 'Nachos\r'),
(837, 'Salad', 'Salade', 'salad', 'salade', 'Dish', 'Assiette', 'Salad', 'Salade\r'),
(839, 'Dulce de leche', 'Dulce de leche', 'dulce de leche', 'dulce de leche', 'Dairy', 'Produits laitiers', 'Dulce de leche', 'Dulce de leche\r'),
(840, 'Topping', 'Garniture', 'topping', 'garniture', 'Additive', 'Additif', 'Topping', 'Garniture\r'),
(841, 'Sweet custard', 'Flan sucré', 'custard', 'crème anglaise', 'Dairy', 'Produits laitiers', 'Sweet custard', 'Flan sucré\r'),
(842, 'Egg roll', 'Roulé aux oeufs', 'egg roll', 'nems', 'Dish', 'Vaisselle', 'Egg roll', 'Roulé aux oeufs\r'),
(843, 'Heart of palm', 'Coeur de palmier', 'heart of palm', 'cœur de palmier', 'Vegetable', 'Légumes', 'Heart of palm', 'Coeur de palmier\r'),
(845, 'Potato chip', 'Chip de pomme de terre', 'potato-chip', 'pommes de terre-frites', 'Bakery', 'Boulangerie', 'Potato chip', 'Chip de pomme de terre\r'),
(846, 'Tortilla chip', 'Tortilla chip', 'tortilla-chip', 'tortilla-chip', 'Bakery', 'Boulangerie', 'Tortilla chip', 'Tortilla chip\r'),
(847, 'Corn chip', 'Chip de maïs', 'corn-chip', 'puce de maïs', 'Bakery', 'Boulangerie', 'Corn chip', 'Chip de maïs\r'),
(848, 'Hibiscus tea', 'Thé à l\'hibiscus', '#tea-hibiscus#', '#tea-hibiscus#', 'Beverage', 'Boisson', 'Hibiscus tea', 'Thé à l\'hibiscus\r'),
(849, 'Stew', 'Ragoût', 'stew', 'ragoût', 'Dish', 'Vaisselle', 'Stew', 'Ragoût\r'),
(850, 'Gelatin dessert', 'Dessert à la gélatine', 'gelatin', 'gélatine', 'Additive', 'Additif', 'Gelatin dessert', 'Dessert à la gélatine\r'),
(851, 'Junket', 'Junket', 'junket', 'junket', 'Dairy', 'Produits laitiers', 'Junket', 'Junket\r'),
(852, 'Falafel', 'Falafel', 'falafel', 'falafel', 'Dish', 'Plats cuisinés', 'Falafel', 'Falafel\r'),
(853, 'Frybread', 'Pain frit', '#fry-bread#', '#fry-bread#', 'Dish', 'Plat', 'Frybread', 'Pain frit\r'),
(855, 'Lasagna', 'Lasagnes', 'lasagna', 'lasagnes', 'Dish', 'Vaisselle', 'Lasagna', 'Lasagnes\r'),
(856, 'Morchella', 'Morchella', 'morchella', 'morchella', 'Fungus', 'Champignon', 'Morchella', 'Morchella\r'),
(857, 'Pancake', 'Pancake', 'pancake', 'crêpe', 'Dish', 'Vaisselle', 'Pancake', 'Pancake\r'),
(858, 'Pectin', 'Pectine', 'pectin', 'pectine', 'Additive', 'Additif', 'Pectin', 'Pectine\r'),
(859, 'Pudding', 'Pudding', 'pudding', 'pudding', 'Dish', 'Plats cuisinés', 'Pudding', 'Pudding\r'),
(860, 'Waffle', 'Gaufre', 'waffle', 'gaufre', 'Dish', 'Vaisselle', 'Waffle', 'Gaufre\r'),
(861, 'Soy milk', 'Lait de soja', 'soy-milk; soya-milk', 'soja-laiton ; soja-laiton', 'Beverage', 'Boisson', 'Soy milk', 'Lait de soja\r'),
(862, 'Meatloaf', 'Pain de viande', 'meat-loaf', 'viande-pain', 'Dish', 'Vaisselle', 'Meatloaf', 'Pain de viande\r'),
(863, 'Cocktail', 'Cocktail', 'cocktail', 'cocktail', 'Beverage', 'Boisson', 'Cocktail', 'Cocktail\r'),
(864, 'Couscous', 'Couscous', 'couscous', 'couscous', 'Dish', 'Assiette', 'Couscous', 'Couscous\r'),
(865, 'Bulgur', 'Boulgour', 'bulgur', 'boulgour', 'Cereal', 'Céréales', 'Bulgur', 'Boulgour\r'),
(867, 'Coffee mocha', 'Café moka', 'mocha', 'moka', 'Beverage', 'Boisson', 'Coffee mocha', 'Café moka\r'),
(868, 'Chimichanga', 'Chimichanga', 'chimichanga', 'chimichanga', 'Dish', 'Plat', 'Chimichanga', 'Chimichanga\r'),
(869, 'Semolina', 'Semoule', 'semolina; sooji', 'semoule ; sooji', 'Cereal', 'Céréales', 'Semolina', 'Semoule\r'),
(870, 'Tapioca pearl', 'Tapioca perlé', 'tapioca', 'tapioca', 'Vegetable', 'Légumes', 'Tapioca pearl', 'Tapioca perlé\r'),
(871, 'Tostada', 'Tostada', 'tostada', 'tostada', 'Dish', 'Assiette', 'Tostada', 'Tostada\r'),
(872, 'Quesadilla', 'Quesadilla', 'quesadilla', 'quesadilla', 'Dish', 'Plat', 'Quesadilla', 'Quesadilla\r'),
(873, 'Baked potato', 'Pomme de terre au four', 'potato-baked', 'pomme de terre au four', 'Dish', 'Plat', 'Baked potato', 'Pomme de terre au four\r'),
(874, 'Hot dog', 'Hot dog', 'hot-dog', 'hot-dog', 'Dish', 'Assiette', 'Hot dog', 'Hot dog\r'),
(875, 'Spread', 'Tartinade', 'spread', 'tartinade', 'Additive', 'Additif', 'Spread', 'Tartinade\r'),
(876, 'Enchilada', 'Enchilada', 'enchilada', 'enchilada', 'Dish', 'Dish', 'Enchilada', 'Enchilada\r'),
(880, 'Ketchup', 'Ketchup', '#ketchup#', '#ketchup#', 'Additive', 'Additif', 'Ketchup', 'Ketchup\r'),
(882, 'Adobo', 'Adobo', '#adobo#', '#adobo#', 'Dish', 'Vaisselle', 'Adobo', 'Adobo\r'),
(884, 'Horned melon', 'Melon cornu', 'melon-horned', 'corne de melon', 'Fruit', 'Fruit', 'Horned melon', 'Melon cornu\r'),
(885, 'Hushpuppy', 'Hushpuppy', 'hushpuppy', 'hushpuppy', 'Dish', 'Plats cuisinés', 'Hushpuppy', 'Hushpuppy\r'),
(886, 'Fruit juice', 'Jus de fruits', 'fruit juice', 'jus de fruits', 'Beverage', 'Boisson', 'Fruit juice', 'Jus de fruits\r'),
(887, 'Relish', 'Relish', 'relish', 'relish', 'Dish', 'Plats cuisinés', 'Relish', 'Relish\r'),
(889, 'Fruit salad', 'Salade de fruits', 'fruit salad', 'salade de fruits', 'Dish', 'Plat', 'Fruit salad', 'Salade de fruits\r'),
(890, 'Soy yogurt', 'Yogourt de soja', 'soy yogurt; soya yogurt', 'yogourt de soja ; yogourt de soja', 'Plant', 'Plante', 'Soy yogurt', 'Yogourt de soja\r'),
(893, 'Cold cut', 'Coupe froide', 'cold cut', 'coupe froide', 'Dish', 'Assiette', 'Cold cut', 'Coupe froide\r'),
(894, 'Mixed nuts', 'Noix mélangées', 'mixed nut', 'noix mélangées', 'Nuts & Seed', 'Noix et graines', 'Mixed nuts', 'Noix mélangées\r'),
(895, 'Babassu palm', 'Palmier babassu', 'palm-babassu', 'palme-babassu', 'Plant', 'Plante', 'Babassu palm', 'Palmier babassu\r');
INSERT INTO `ingredient` (`id`, `IngredientNameEN`, `IngredientNameFR`, `IngredientSynonymEN`, `IngredientSynonymFR`, `CategoryEN`, `CategoryFR`, `ContituentIngredientsEN`, `ContituentIngredientsFR`) VALUES
(897, 'Shea tree', 'Karité', 'shea', 'karité', 'Plant', 'Plante', 'Shea tree', 'Karité\r'),
(898, 'Oil-seed Camellia', 'Camélia oléagineux', 'camellia', 'camélia', 'Plant', 'Plante', 'Oil-seed Camellia', 'Camélia oléagineux\r'),
(899, 'Ucuhuba', 'Ucuhuba', 'ucuhuba', 'ucuhuba', 'Plant', 'Plante', 'Ucuhuba', 'Ucuhuba\r'),
(900, 'Phyllo dough', 'Pâte phyllo', '#phyllo#', '#phyllo#', 'Bakery', 'Boulangerie', 'Phyllo dough', 'Pâte phyllo\r'),
(901, 'Cooking oil', 'Huile de cuisson', '#cooking oil#', '#huile de cuisson#', 'Additive', 'Additif', 'Cooking oil', 'Huile de cuisson\r'),
(902, 'Pie crust', 'Croûte à tarte', '#pie-crust#', '#pie-croute#', 'Bakery', 'Boulangerie', 'Pie crust', 'Croûte à tarte\r'),
(904, 'Pie', 'Tarte', 'pie', 'tarte', 'Dish', 'Assiette', 'Pie', 'Tarte\r'),
(905, 'Shortening', 'Shortening', 'shortening', 'shortening', 'Additive', 'Additif', 'Shortening', 'Shortening\r'),
(906, 'Soy cream', 'Crème de soja', '#soy cream#', '#crème soja', 'Dish', 'Vaisselle', 'Soy cream', 'Crème de soja\r'),
(907, 'Ice cream cone', 'Cornet de crème glacée', 'ice cream cone', 'cornet de crème glacée', 'Dish', 'Assiette', 'Ice cream cone', 'Cornet de crème glacée\r'),
(908, 'Molasses', 'Mélasse', 'molasses; treacle', 'mélasse ; mélasse', 'Additive', 'Additif', 'Molasses', 'Mélasse\r'),
(909, 'Nance', 'Nance', 'nance', 'nance', 'Fruit', 'Fruit', 'Nance', 'Nance\r'),
(911, 'Natto', 'Natto', 'natto', 'natto', 'Dish', 'Vaisselle', 'Natto', 'Natto\r'),
(912, 'Ravioli', 'Ravioli', 'ravioli', 'ravioli', 'Dish', 'Assiette', 'Ravioli', 'Ravioli\r'),
(913, 'Scrapple', 'Scrapple', 'scrapple', 'scrapple', 'Dish', 'Plat', 'Scrapple', 'Scrapple\r'),
(915, 'Succotash', 'Succotash', 'succotash', 'succotash', 'Dish', 'Assiette', 'Succotash', 'Succotash\r'),
(916, 'Tamale', 'Tamale', 'tamale', 'tamale', 'Dish', 'Assiette', 'Tamale', 'Tamale\r'),
(917, 'Rice cake', 'Gâteau de riz', '#rice-cake#', '#rice-cake#', 'Dish', 'Assiette', 'Rice cake', 'Gâteau de riz\r'),
(918, 'Tree fern', 'Fougère arboricole', 'fern-tree', 'arbre de fougère', 'Plant', 'Plante', 'Tree fern', 'Fougère arboricole\r'),
(919, 'Evaporated milk', 'Lait évaporé', 'milk-evaporated', 'lait-évaporé', 'Dairy', 'Produits laitiers', 'Evaporated milk', 'Lait évaporé\r'),
(920, 'Flour', 'Farine', '#flour#', '#flour#', 'Cereal', 'Céréales', 'Flour', 'Farine\r'),
(921, 'Akutaq', 'Akutaq', 'akutaq', 'akutaq', 'Dish', 'Vaisselle', 'Akutaq', 'Akutaq\r'),
(923, 'Pita bread', 'Pain pita', '#bread-pita#', '#pain-pita#', 'Bakery', 'Boulangerie', 'Pita bread', 'Pain pita\r'),
(924, 'Focaccia', 'Focaccia', 'focaccia', 'focaccia', 'Bakery', 'Boulangerie', 'Focaccia', 'Focaccia\r'),
(925, 'Bagel', 'Bagel', 'bagel', 'bagel', 'Bakery', 'Boulangerie', 'Bagel', 'Bagel\r'),
(927, 'Piki bread', 'Pain piki', 'bread-piki', 'pain-piki', 'Bakery', 'Boulangerie', 'Piki bread', 'Pain piki\r'),
(928, 'French toast', 'Pain français', 'french toast', 'pain perdu', 'Bakery', 'Boulangerie', 'French toast', 'Pain français\r'),
(929, 'Oat bread', 'Pain d\'avoine', '#bread-oat#', '#pain-avoine', 'Bakery', 'Boulangerie', 'Oat bread', 'Pain d\'avoine\r'),
(930, 'Potato bread', 'Pain de pommes de terre', 'potato-bread', 'pain-pomme de terre', 'Bakery', 'Boulangerie', 'Potato bread', 'Pain de pommes de terre\r'),
(931, 'Cornbread', 'Pain de maïs', '#corn-bread#', '#maïs-pain#', 'Maize', 'Maize', 'Cornbread', 'Pain de maïs\r'),
(932, 'Corn grits', 'Gruaux de maïs', 'corn-grit', 'maïs-grain', 'Maize', 'Maïs', 'Corn grits', 'Gruaux de maïs\r'),
(933, 'Multigrain bread', 'Pain multigrains', '#bread-multigrain#', '#pain-multigrain#', 'Bakery', 'Boulangerie', 'Multigrain bread', 'Pain multigrains\r'),
(934, 'Rice bread', 'Pain de riz', 'bread-rice', 'pain-riz', 'Bakery', 'Boulangerie', 'Rice bread', 'Pain de riz\r'),
(935, 'Pan dulce', 'Pan dulce', 'pan dulce', 'pan dulce', 'Bakery', 'Boulangerie', 'Pan dulce', 'Pan dulce\r'),
(936, 'Raisin bread', 'Pain aux raisins', '#bread-raisin#', '#pain-raisin#', 'Bakery', 'Boulangerie', 'Raisin bread', 'Pain aux raisins\r'),
(937, 'Wonton wrapper', 'Papier Wonton', 'wonton wrapper', 'emballage wonton', 'Bakery', 'Boulangerie', 'Wonton wrapper', 'Papier Wonton\r'),
(938, 'Trail mix', 'Mélange de fruits et légumes', 'trail mix', 'mélange de sentiers', 'Dish', 'Vaisselle', 'Trail mix', 'Mélange de fruits et légumes\r'),
(939, 'Greenthread tea', 'Thé au fil vert', 'tea-greenthread', 'thé-fil de fer', 'Beverage', 'Boisson', 'Greenthread tea', 'Thé au fil vert\r'),
(941, 'Vegetable juice', 'Jus de légumes', 'vegetable juice', 'jus de légumes', 'Beverage', 'Boisson', 'Vegetable juice', 'Jus de légumes\r'),
(942, 'Horchata', 'Horchata', 'horchata', 'horchata', 'Beverage', 'Boisson', 'Horchata', 'Horchata\r'),
(943, 'Soft drink', 'Boisson gazeuse', 'soft drink', 'boisson gazeuse', 'Beverage', 'Boisson', 'Soft drink', 'Boisson gazeuse\r'),
(944, 'Milkshake', 'Milkshake', 'milkshake', 'milkshake', 'Beverage', 'Boisson', 'Milkshake', 'Milkshake\r'),
(945, 'Chocolate mousse', 'Mousse au chocolat', 'chocolate-mousse', 'mousse au chocolat', 'Bakery', 'Boulangerie', 'Chocolate mousse', 'Mousse au chocolat\r'),
(947, 'Pupusa', 'Pupusa', 'pupusa', 'pupusa', 'Dish', 'Vaisselle', 'Pupusa', 'Pupusa\r'),
(948, 'Empanada', 'Empanada', 'empanada', 'empanada', 'Dish', 'Assiette', 'Empanada', 'Empanada\r'),
(949, 'Arepa', 'Arepa', 'arepa', 'arepa', 'Dish', 'Assiette', 'Arepa', 'Arepa\r'),
(950, 'Ascidians', 'Ascidies', 'ascidians', 'ascidies', 'Seafood', 'Fruits de mer', 'Ascidians', 'Ascidies\r'),
(951, 'Gefilte fish', 'Poisson Gefilte', 'gefilte', 'gefilte', 'Dish', 'Assiette', 'Gefilte fish', 'Poisson Gefilte\r'),
(952, 'Yellow pond lily', 'Nénuphar jaune', 'yellow-pond lily', 'jaune-nénuphar', 'Plant', 'Plante', 'Yellow pond lily', 'Nénuphar jaune\r'),
(953, 'Fish burger', 'Burger de poisson', 'fish burger', 'burger de poisson', 'Dish', 'Plat', 'Fish burger', 'Burger de poisson\r'),
(955, 'Pot pie', 'Pâté en croûte', 'pot-pie', 'pot-tarte', 'Dish', 'Plat', 'Pot pie', 'Pâté en croûte\r'),
(956, 'Stuffing', 'Farce', 'stuffing', 'farce', 'Additive', 'Additif', 'Stuffing', 'Farce\r'),
(958, 'Fudge', 'Caramel', 'fudge', 'fudge', 'Bakery', 'Boulangerie', 'Fudge', 'Caramel\r'),
(959, 'Candy bar', 'Barre chocolatée', 'candy bar', 'barre chocolatée', 'Bakery', 'Boulangerie', 'Candy bar', 'Barre chocolatée\r'),
(960, 'Condensed milk', 'Lait condensé', 'milk-condensed', 'lait condensé', 'Dairy', 'Produits laitiers', 'Condensed milk', 'Lait condensé\r'),
(961, 'Margarine', 'Margarine', 'margarine', 'margarine', 'Additive', 'Additif', 'Margarine', 'Margarine\r'),
(963, 'Hummus', 'Houmous', 'hummus', 'houmous', 'Dish', 'Vaisselle', 'Hummus', 'Houmous\r'),
(964, 'Potato puffs', 'Feuilletés de pommes de terre', 'potato-puff', 'potato-puff', 'Dish', 'Vaisselle', 'Potato puffs', 'Feuilletés de pommes de terre\r'),
(965, 'Potato gratin', 'Gratin de pommes de terre', 'potato-gratin', 'gratin de pommes de terre', 'Dish', 'Vaisselle', 'Potato gratin', 'Gratin de pommes de terre\r'),
(967, 'Chinese bayberry', 'Laurier chinois', 'bayberry-chinese', 'bayberry-chinois', 'Fruit', 'Fruit', 'Chinese bayberry', 'Laurier chinois\r'),
(968, 'Green zucchini', 'Courgettes vertes', 'zucchini-green', 'courgette-verte', 'Vegetable', 'Légumes', 'Green zucchini', 'Courgettes vertes\r'),
(969, 'Zucchini', 'Courgettes', 'zucchini', 'courgette', 'Vegetable', 'Légumes', 'Zucchini', 'Courgettes\r'),
(970, 'Saskatoon berry', 'Amélanche', 'berry-saskatoon', 'baies-askatoon', 'Fruit', 'Fruit', 'Saskatoon berry', 'Amélanche\r'),
(971, 'Nanking cherry', 'Cerise de Nankin', 'cherry-nanking', 'cerise-nankin', 'Fruit', 'Fruit', 'Nanking cherry', 'Cerise de Nankin\r'),
(972, 'Japanese pumpkin', 'Courge japonaise', 'pumpkin-japanese', 'citrouille-japonaise', 'Fruit', 'Fruit', 'Japanese pumpkin', 'Courge japonaise\r'),
(977, 'Guinea hen', 'Poule de Guinée', 'guinea-hen', 'pintade-poule', 'Meat', 'Viande', 'Guinea hen', 'Poule de Guinée\r'),
(978, 'Cucurbita', 'Cucurbita', 'cucurbita', 'cucurbita', 'Vegetable', 'Légume', 'Cucurbita', 'Cucurbita\r'),
(979, 'Anise Oil', 'Huile d\'anis', '#anise-oil#', '#huile d\'anis', 'Fruit', 'Fruit', 'Anise Oil', 'Huile d\'anis\r'),
(980, 'Apple Juice', 'Jus de pomme', 'apple-juice', 'jus de pomme', 'Fruit', 'Fruit', 'Apple Juice', 'Jus de pomme\r'),
(981, 'Coconut Milk', 'Lait de coco', 'coconut-milk', 'lait de coco', 'Beverage', 'Boisson', 'Coconut Milk', 'Lait de coco\r'),
(982, 'Coconut Oil', 'Huile de noix de coco', 'coconut-oil', 'huile de noix de coco', 'Fruit', 'Fruit', 'Coconut Oil', 'Huile de noix de coco\r'),
(983, 'Hops Beer', 'Bière au houblon', 'beer-hops', 'bière-houblon', 'Beverage Alcoholic', 'Boisson alcoolisée', 'Hops Beer', 'Bière au houblon\r'),
(984, 'Lemon Juice', 'Jus de citron', 'lemon juice', 'jus de citron', 'Beverage', 'Boisson', 'Lemon Juice', 'Jus de citron\r'),
(985, 'Brown Rice', 'Riz brun', 'rice-brown', 'riz brun', 'Cereal', 'Céréales', 'Brown Rice', 'Riz brun\r'),
(986, 'Tomato Juice', 'Jus de tomate', '#tomato-juice#', '#tomato-juice#', 'Beverage', 'Boisson', 'Tomato Juice', 'Jus de tomate\r'),
(987, 'Tomato Paste', 'Pâte de tomate', '#tomato-paste#', '#pâte de tomate#', 'Vegetable', 'Légumes', 'Tomato Paste', 'Pâte de tomate\r'),
(988, 'Tomato Puree', 'Purée de tomate', '#tomato-puree#', '#Purée de tomate', 'Vegetable', 'Végétal', 'Tomato Puree', 'Purée de tomate\r'),
(989, 'Coriander Seed', 'Graines de coriandre', 'coriander-seed', 'graines de coriandre', 'Vegetable', 'Légumes', 'Coriander Seed', 'Graines de coriandre\r'),
(990, 'Lard', 'Lard', 'pork-fat; lard', 'graisse de porc ; saindoux', 'Meat', 'Viande', 'Lard', 'Lard\r'),
(991, 'Cured Ham', 'Jambon cru', '#ham-cured#', '#ham-cured#', 'Meat', 'Viande', 'Cured Ham', 'Jambon cru\r'),
(992, 'Cayenne', 'Cayenne', 'cayenne; chile powder; red chile; habanero; harissa; green chile; green chili; green chilli; green chily; red chily; red chili; red chilli', 'cayenne ; poudre de chile ; chile rouge ; habanero ; harissa ; chile vert ; chile vert ; chile vert ; chile vert ; chile vert ; chile rouge ; chile rouge ; chile rouge', 'Spice', 'Épice', 'Cayenne', 'Cayenne\r'),
(993, 'Yeast', 'Levure', 'yeast', 'levure', 'Fungus', 'Champignon', 'Yeast', 'Levure\r'),
(994, 'Tequila', 'Tequila', 'tequila', 'tequila', 'Beverage Alcoholic', 'Boisson Alcoolisée', 'Tequila', 'Tequila\r'),
(995, 'Sauerkraut', 'Choucroute', 'sauerkraut', 'choucroute', 'Vegetable', 'Végétal', 'Sauerkraut', 'Choucroute\r'),
(996, 'Baking Powder', 'Poudre à lever', 'baking powder; baking soda', 'poudre à pâte ; bicarbonate de soude', 'Additive', 'Additif', 'Baking Powder', 'Poudre à lever\r'),
(997, 'Monosodium Glutamate', 'Glutamate monosodique', 'monosodium glutamate', 'glutamate monosodique', 'Additive', 'Additif', 'Monosodium Glutamate', 'Glutamate monosodique\r'),
(998, 'Citric Acid', 'Acide citrique', 'citric acid', 'acide citrique', 'Additive', 'Additif', 'Citric Acid', 'Acide citrique\r'),
(999, 'Cooking Spray', 'Spray de cuisson', 'cooking spray', 'spray de cuisson', 'Additive', 'Additif', 'Cooking Spray', 'Spray de cuisson\r'),
(1000, 'Gelatin', 'Gélatine', 'gelatin', 'gélatine', 'Additive', 'Additif', 'Gelatin', 'Gélatine\r'),
(1001, 'Food Coloring', 'Colorant alimentaire', 'food coloring', 'colorant alimentaire', 'Additive', 'Additif', 'Food Coloring', 'Colorant alimentaire\r'),
(1002, 'Liquid Smoke', 'Fumée liquide', 'liquid smoke', 'fumée liquide', 'Additive', 'Additif', 'Liquid Smoke', 'Fumée liquide\r'),
(2000, 'Garam Masala', 'Garam Masala', 'garam masala', 'garam masala', 'Spice', 'Épices', 'black pepper, mace, cinnamon, clove, cardamom, nutmeg', 'poivre noir, macis, cannelle, clou de girofle, cardamome, noix de muscade\r'),
(2001, 'Ginger Garlic Paste', 'Gingembre Pâte d\'ail', 'ginger garlic paste', 'gingembre pâte d\'ail', 'Spice', 'Épices', 'ginger, garlic', 'gingembre, ail\r'),
(2002, 'Coriander Cumin Seed Powder', 'Poudre de graines de coriandre et de cumin', 'coriander cumin seed powder', 'poudre de graines de coriandre et de cumin', 'Spice', 'Épices', 'coriander, cumin', 'coriandre, cumin\r'),
(2003, 'Chaat Masala', 'Chaat Masala', 'chaat masala', 'chaat masala', 'Spice', 'Épice', 'asafoetida, mango, black salt, cayenne, garlic, ginger, sesame seed, black mustard seed oil, turmeric, coriander, bay laurel, anise, fennel', 'asafoetida, mangue, sel noir, cayenne, ail, gingembre, graines de sésame, huile de graines de moutarde noire, curcuma, coriandre, laurier, anis, fenouil\r'),
(2004, 'Sambar Powder', 'Poudre de Sambar', 'sambar powder', 'poudre de sambar', 'Spice', 'Épice', 'pigeon pea, coriander, chickpea, cumin, black pepper, cayenne, ginger, fenugreek, turmeric', 'pois d\'Angole, coriandre, pois chiche, cumin, poivre noir, cayenne, gingembre, fenugrec, curcuma\r'),
(2005, 'Chole Masala', 'Chole Masala', 'chole masala', 'chole masala', 'Spice', 'Épice', 'cayenne, garlic, ginger, roasted sesame seed, black mustard seed oil, turmeric, coriander bay laurel, anise, fennel', 'cayenne, ail, gingembre, graines de sésame grillées, huile de graines de moutarde noire, curcuma, laurier coriandre, anis, fenouil\r'),
(2006, 'Rasam Powder', 'Poudre de Rasam', 'rasam powder', 'poudre de rasam', 'Spice', 'Épice', 'cayenne, pigeon pea, cumin, coriander, black pepper, curry leaf', 'cayenne, pois d\'Angole, cumin, coriandre, poivre noir, feuille de curry\r'),
(2007, 'Tandoori Masala', 'Tandoori Masala', 'tandoori masala', 'masala tandoori', 'Spice', 'Épice', 'garlic, ginger, clove, nutmeg, mace, cumin, coriander, fenugreek, cinnamon, cardamom, black pepper', 'ail, gingembre, clou de girofle, noix de muscade, macis, cumin, coriandre, fenugrec, cannelle, cardamome, poivre noir\r'),
(2008, 'Curry Powder', 'Poudre de curry', 'curry powder', 'poudre de curry', 'Spice', 'Épice', 'cardamom, cayenne, cinnamon, clove, coriander, cumin, fennel, fenugreek, mace, nutmeg, black pepper, poppy seed, sesame seed, saffron, tamarind, turmeric', 'cardamome, cayenne, cannelle, clou de girofle, coriandre, cumin, fenouil, fenugrec, macis, noix de muscade, poivre noir, graines de pavot, graines de sésame, safran, tamarin, curcuma\r'),
(2009, 'Panch Pharon Seed', 'Graines de Pharon Panch', 'panch pharon seed', 'graine de pharon panch', 'Spice', 'Épice', 'fenugreek, nigella seed, cumin, black mustard seed oil', 'fenugrec, graine de nigelle, cumin, huile de graine de moutarde noire\r'),
(2010, 'Chicken Masala Powder', 'Poudre de poulet Masala', 'chicken masala powder', 'poudre de poulet masala', 'Spice', 'Épice', 'bay laurel, ginger, cinnamon, clove, black pepper, coriander, fennel, cayenne', 'baie de laurier, gingembre, cannelle, clou de girofle, poivre noir, coriandre, fenouil, cayenne\r'),
(2011, 'Goda Masala', 'Goda Masala', 'goda masala', 'goda masala', 'Spice', 'Épice', 'cardamom, cinnamon, clove, bay laurel, sesame seed, coriander, coconut, cassia, white pepper, black pepper', 'cardamome, cannelle, clou de girofle, baies de laurier, graines de sésame, coriandre, noix de coco, casse, poivre blanc, poivre noir\r'),
(2012, 'Jal Jeera Powder', 'Poudre de Jal Jeera', 'jal jeera powder', 'poudre de jal jeera', 'Spice', 'Épice', 'black salt, mango, cumin, citric acid, mint, black pepper, ginger, asafoetida', 'sel noir, mangue, cumin, acide citrique, menthe, poivre noir, gingembre, asafoetida\r'),
(2013, 'Ginger Garlic Coriander Leaves', 'Gingembre, ail, feuilles de coriandre', 'ginger garlic coriander leaf', 'gingembre ail feuille de coriandre', 'Spice', 'Épice', 'ginger, garlic, coriander', 'gingembre, ail, coriandre\r'),
(2014, 'Pulao Masala', 'Pulao Masala', 'pulao masala', 'pulao masala', 'Spice', 'Épice', 'black pepper, white pepper, clove, cumin, cinnamon, cardamom, coriander', 'poivre noir, poivre blanc, clou de girofle, cumin, cannelle, cardamome, coriandre\r'),
(2015, 'Dabeli Masala', 'Dabeli Masala', 'dabeli masala', 'dabeli masala', 'Spice', 'Épice', 'cayenne, coriander, cinnamon, clove, cumin', 'cayenne, coriandre, cannelle, clou de girofle, cumin\r'),
(2016, 'Cake Mix', 'Mélange de gâteaux', 'cake mix', 'mélange à gâteau', 'Dish', 'Plats', 'flour, wheat, sugar, baking powder, salt', 'farine, blé, sucre, levure chimique, sel\r'),
(2017, 'Half Half', 'Moitié-moitié', 'half half', 'moitié moitié', 'Dairy', 'Produits laitiers', 'milk, cream', 'lait, crème\r'),
(2018, 'Ale', 'Ale', 'ale', 'ale', 'Beverage Alcoholic', 'Boisson Alcoolisée', 'beer, hops-beer', 'bière, bière de houblon\r'),
(2019, 'Vermicelli', 'Vermicelles', 'vermicelli', 'vermicelle', 'Dish', 'Plat', 'flour, wheat, water, salt', 'farine, blé, eau, sel\r'),
(2020, 'Self Rising Flour', 'Farine auto levante', 'self rising flour', 'farine autoportante', 'Cereal', 'Céréales', 'flour, salt, baking-powder', 'farine, sel, levure chimique\r'),
(2021, 'Mayonnaise', 'Mayonnaise', 'mayonnaise', 'mayonnaise', 'Dish', 'Vaisselle', 'oil, egg, lemon-juice', 'huile, oeuf, jus de citron\r'),
(2022, 'Salt-Pepper', 'Sel et poivre', 'salt-pepper', 'sel et poivre', 'Additive', 'Additif', 'salt, pepper', 'sel, poivre\r'),
(2023, 'Croissant', 'Croissant', 'croissant', 'croissant', 'Dish', 'Vaisselle', 'flour, wheat, butter, yeast', 'farine, blé, beurre, levure\r'),
(2024, 'Puff Pastry', 'Pâte feuilletée', 'puff pastry', 'pâte feuilletée', 'Dish', 'Plat', 'flour, wheat, butter', 'farine, blé, beurre\r'),
(2025, 'Green Chutney', 'Chutney vert', 'green chutney', 'chutney vert', 'Spice', 'Épice', 'corriander, cumin, lemon-juice, cayenne', 'corriandre, cumin, jus de citron, cayenne\r'),
(2026, 'Cajun Seasoning', 'Assaisonnement Cajun', 'cajun seasoning', 'assaisonnement cajun', 'Spice', 'Épice', 'salt, cayenne, garlic, oregano, paprika, thyme, pepper, onion', 'sel, cayenne, ail, origan, paprika, thym, poivre, oignon\r'),
(2027, 'Vegetable Broth', 'Bouillon de légumes', 'vegetable broth', 'bouillon de légumes', 'Dish', 'Vaisselle', 'onion, carrot, celery, shallot, sage, mushroom', 'oignon, carotte, céleri, échalote, sauge, champignon\r'),
(2028, 'Tandoori Paste', 'Pâte Tandoori', 'tandoori paste', 'pâte tandoori', 'Dish', 'Plat', 'garlic, ginger, clove, nutmeg, mace, cumin, coriander, fenugreek, cinnamon, cardamom, black pepper, yogurt, lime-juice', 'ail, gingembre, clou de girofle, noix de muscade, macis, cumin, coriandre, fenugrec, cannelle, cardamome, poivre noir, yaourt, jus de citron vert\r'),
(2029, 'Mixed Vegetables', 'Légumes mélangés', 'mixed vegetables', 'mélange de légumes', 'Vegetable', 'Légumes', 'carrot, cauliflower, corn, pea, capsicum', 'carotte, chou-fleur, maïs, pois, poivron\r'),
(2030, 'Salad Creme', 'Crème de salade', 'salad creme', 'crème de salade', 'Dish', 'Plat', 'oil, egg, vinegar, water', 'huile, œuf, vinaigre, eau\r'),
(2031, 'Worcestershite Sauce', 'Sauce Worcestershite', 'worcestershire sauce', 'sauce worcestershire', 'Dish', 'Plat', 'cider-vinegar, soy-sauce, water, sugar, ginger, mustard, onion, garlic, cinnamon, pepper-black', 'vinaigre de cidre, sauce de soja, eau, sucre, gingembre, moutarde, oignon, ail, cannelle, poivre noir.\r'),
(2032, 'Coleslaw', 'Salade de chou', 'coleslaw', 'salade de chou', 'Dish', 'Plat', 'cabbage, vinegar, oil, salt, mayonnaise, egg, water', 'chou, vinaigre, huile, sel, mayonnaise, oeuf, eau\r'),
(2033, 'Vegetable Stock', 'Bouillon de légumes', 'vegetable stock', 'bouillon de légumes', 'Vegetable', 'Légumes', 'onion, carrot, celery, thyme, bayleaf, pepper, tomato, garlic', 'oignon, carotte, céleri, thym, laurier, poivre, tomate, ail.\r'),
(2034, 'Mascarpone', 'Mascarpone', 'mascarpone', 'mascarpone', 'Dish', 'Assiette', 'cheese-cream, lemon-juice, vinegar', 'crème de fromage, jus de citron, vinaigre\r'),
(2035, 'Fettuccine', 'Fettuccine', 'fettuccine', 'fettuccine', 'Dish', 'Plat', 'flour, wheat, eggs', 'farine, blé, oeufs\r'),
(2036, 'Italian Seasoning', 'Assaisonnement italien', 'italian style seasoning; italian herb seasoning; italian seasoning', 'assaisonnement à l\'italienne ; assaisonnement aux herbes italiennes ; assaisonnement à l\'italienne', 'Spice', 'Épices', 'basil, oregano, rosemary, thyme', 'basilic, origan, romarin, thym\r'),
(2037, 'Ketchup', 'Ketchup', 'ketchup', 'ketchup', 'Dish', 'Plat', 'ketchup, tomato, sugar, vinegar, salt, onion, allspice, coriander, clove, cumin, garlic, mustard', 'ketchup, tomate, sucre, vinaigre, sel, oignon, quatre-épices, coriandre, clou de girofle, cumin, ail, moutarde\r'),
(2038, 'Spinach Fettuccine', 'Fettuccine aux épinards', 'spinach fettuccine', 'fettuccine aux épinards', 'Dish', 'Plat', 'spinach, flour, wheat, eggs', 'épinards, farine, blé, œufs\r'),
(2039, 'Hoisin Sauce', 'Sauce Hoisin', 'hoisin sauce', 'sauce hoisin', 'Dish', 'Plat', 'soybean, cayenne, garlic', 'soja, cayenne, ail\r'),
(2040, 'Barbeque Sauce', 'Sauce barbecue', 'barbeque sauce', 'sauce barbecue', 'Dish', 'Assiette', 'vinegar, tomato, tomato-paste, tomato-puree, mayonnaise, liquid-smoke, onion, mustard, pepper, sugar', 'vinaigre, tomate, pâte de tomate, purée de tomate, mayonnaise, fumée liquide, oignon, moutarde, poivre, sucre\r'),
(2041, 'Sponge Cake', 'Gâteau éponge', 'sponge cake', 'gâteau éponge', 'Dish', 'Assiette', 'flour, wheat, butter, sugar, egg, baking-powder', 'farine, blé, beurre, sucre, œuf, poudre à lever\r'),
(2042, 'Baking Mix', 'Mélange à pâtisserie', 'baking mix', 'mélange à pâtisserie', 'Dish', 'Assiette', 'flour, wheat, baking-powder, yeast, sugar, salt', 'farine, blé, levure, levure chimique, sucre, sel\r'),
(2043, 'Hot Sauce', 'Sauce piquante', 'hot sauce', 'sauce piquante', 'Dish', 'Assiette', 'cayenne, salt, vinegar', 'cayenne, sel, vinaigre\r'),
(2044, 'Dried Mixed Fruit', 'Mélange de fruits séchés', 'dried mixed fruit', 'mélange de fruits secs', 'Fruit', 'Fruit', 'raisin, apricot, date, currant, fig', 'raisin sec, abricot, datte, groseille, figue\r'),
(2045, 'Candied Mixed Fruit', 'Mélange de fruits confits', 'candied mixed fruit', 'mélange de fruits confits', 'Fruit', 'Fruit', 'raisin, apricot, date, currant, fig, sugar', 'raisin sec, abricot, datte, groseille, figue, sucre\r'),
(2046, 'Gulkand', 'Gulkand', 'gulkand', 'gulkand', 'Dish', 'Assiette', 'rose, sugar', 'rose, sucre\r'),
(2047, 'Pancetta', 'Pancetta', 'pancetta', 'pancetta', 'Dish', 'Plat', 'pork, pepper, salt', 'porc, poivre, sel\r'),
(2048, 'Phyllo', 'Phyllo', 'phyllo', 'phyllo', 'Cereal', 'Céréales', 'phyllo, flour, wheat', 'phyllo, farine, blé\r'),
(2049, 'Pie Crust', 'Croûte à tarte', 'pie-crust', 'pâte à tarte', 'Cereal', 'Céréales', 'pie-crust, flour, wheat', 'pâte à tarte, farine, blé\r'),
(2050, 'Prosciutto', 'Prosciutto', 'prosciutto', 'prosciutto', 'Meat', 'Viande', 'ham, ham-cured', 'jambon, jambon cru\r'),
(2051, 'Creole Seasoning', 'Assaisonnement créole', 'creole seasoning', 'assaisonnement créole', 'Spice', 'Épice', 'onion, pepper, garlic, pepper-white, oregano, cayenne, basil, capsicum, thyme, salt', 'oignon, poivron, ail, poivre blanc, origan, cayenne, basilic, poivron, thym, sel\r'),
(2052, 'Chinese Five Spice Powder', 'Poudre de cinq épices chinoises', 'chinese five spice', 'cinq épices chinoises', 'Spice', 'Épice', 'cinnamon, anise-star, fennel, pepper, clove', 'cannelle, anis étoilé, fenouil, poivre, clou de girofle\r'),
(2053, 'Old Bay Seasoning', 'Assaisonnement Old Bay', 'old-bay seasoning', 'assaisonnement old bay', 'Spice', 'Épice', 'bay-laurel, celery, salt, mustard, pepper, ginger, capsicum, pepper-white, nutmeg, clove, allspice, cayenne, mace, cardamom, cinnamon', 'baie de laurier, céleri, sel, moutarde, poivre, gingembre, poivron, poivre blanc, noix de muscade, clou de girofle, piment de la Jamaïque, cayenne, macis, cardamome, cannelle...\r'),
(2054, 'Adobo Sauce', 'Sauce Adobo', 'adobo sauce', 'sauce adobo', 'Dish', 'Vaisselle', 'cayenne, garlic, cider-vinegar, salt, sugar, cumin', 'cayenne, ail, vinaigre de cidre, sel, sucre, cumin\r'),
(2055, 'Cracker Crumb', 'Miettes de biscuits', 'cracker crumb; graham cracker', 'chapelure de biscuit ; biscuit graham', 'Dish', 'Plat', 'wheat, sugar, honey', 'blé, sucre, miel\r'),
(2056, 'Flour', 'Farine', 'flour; atta', 'farine ; atta', 'Cereal', 'Céréales', 'flour, wheat', 'farine, blé\r'),
(2057, 'Ranch Dressing', 'Vinaigrette Ranch', 'dressing-ranch', 'vinaigrette-ranch', 'Spice', 'Épice', 'mayonnaise, garlic, cream, onion, chive, salt, parsley, pepper, dill', 'mayonnaise, ail, crème, oignon, ciboulette, sel, persil, poivre, aneth\r'),
(2058, 'Poultry Seasoning', 'Assaisonnement pour volaille', 'poultry seasoning', 'assaisonnement pour volaille', 'Spice', 'Épice', 'sage, thyme, marjoram, rosemary, nutmeg, pepper', 'sauge, thym, marjolaine, romarin, noix de muscade, poivre\r'),
(2059, 'Tahini', 'Tahini', 'tahini', 'tahini', 'Dish', 'Assiette', 'sesame, olive', 'sésame, olive\r'),
(2060, 'Herbe de Provence', 'Herbe de Provence', 'herbe de provence', 'herbe de provence', 'Spice', 'Épice', 'rosemary, lavendar, fennel, parsley, savory-winter, oregano, thyme, tarragon, basil, bay-laurel, marjoram', 'romarin, lavande, fenouil, persil, sarriette d\'hiver, origan, thym, estragon, basilic, laurier, marjolaine\r'),
(2061, 'Tart Shell', 'Coquille de tarte', 'tart shell', 'fond de tarte', 'Dish', 'Assiette', 'flour, wheat, sugar, salt, butter, shortening, water', 'farine, blé, sucre, sel, beurre, shortening, eau\r'),
(2062, 'Adobo', 'Adobo', 'adobo', 'adobo', 'Dish', 'Plat', 'cayenne, garlic, cider-vinegar, salt, sugar, cumin, chicken, adobo', 'cayenne, ail, vinaigre de cidre, sel, sucre, cumin, poulet, adobo\r'),
(2063, 'Tomato Puree', 'Purée de tomates', 'tomato-puree', 'purée de tomates', 'Vegetable', 'Légumes', 'tomato, tomato-puree', 'tomate, purée de tomate\r'),
(2064, 'Tomato Paste', 'Pâte de tomate', 'tomato-paste', 'purée de tomates', 'Vegetable', 'Légumes', 'tomato, tomato-paste', 'tomate, pâte de tomate\r'),
(2065, 'Bear', 'Ours', 'bear', 'ours', 'Meat', 'Viande', 'bear-black, bear-brown, bear-polar', 'ours-noir, ours-brun, ours-polaire\r'),
(2066, 'Whale', 'Baleine', 'whale', 'baleine', 'Fish', 'Poisson', 'whale-beluga, whale-bowhead', 'baleine-béluga, baleine-bowhead\r'),
(2067, 'Whitefish', 'Corégone', 'whitefish', 'corégone', 'Fish', 'Poisson', 'whitefish-broad, whitefish', 'corégone - large, corégone\r'),
(2068, 'Seal', 'Phoque', 'seal', 'phoque', 'Fish', 'Poisson', 'seal, seal-ringed, seal-spotted, seal-bearded', 'phoque, phoque annelé, phoque tacheté, phoque barbu\r'),
(2069, 'Tuna', 'Thon', 'tuna', 'thon', 'Fish', 'Poisson', 'tuna-albacore, tuna-bluefin, tuna-yellowfin, tuna', 'thon - germon, thon rouge, thon à nageoires jaunes, thon\r'),
(2070, 'Croaker', 'Croaçon', 'croaker', 'crochet', 'Fish', 'Poisson', 'croaker-atlantic, croaker-spot', 'croaker-atlantique, croaker tacheté\r'),
(2071, 'Mackerel', 'Maquereau', 'mackerel', 'maquereau', 'Fish', 'Poisson', 'mackerel-atlantic, mackerel-pacific, mackerel-king, mackerel-spanish', 'maquereau-atlantique, maquereau-pacifique, maquereau-roi, maquereau-espagnol\r'),
(2072, 'Pollock', 'Goberge', 'pollock', 'goberge', 'Fish', 'Poisson', 'pollock-atlantic, pollock-alaska, pollock', 'goberge-atlantique, goberge-alaska, goberge\r'),
(2073, 'Anchovy', 'Anchois', 'anchovy', 'anchois', 'Fish', 'Poisson', 'anchovy-european, anchovy', 'anchois-européen, anchois\r'),
(2074, 'Perch', 'Perche', 'perch', 'perche', 'Fish', 'Poisson', 'perch-pacific, perch-pike', 'perche-pacifique, perche-pike\r'),
(2075, 'Frybread', 'Frybread', 'fry-bread', 'alevins', 'Dish', 'Plat', 'frybread, flour, wheat', 'pain frit, farine, blé\r'),
(2076, 'Sturgeon', 'Esturgeon', 'sturgeon', 'esturgeon', 'Fish', 'Poisson', 'sturgeon-greater, sturgeon', 'esturgeon-gros, esturgeon\r'),
(2077, 'Hibiscus Tea', 'Thé à l\'hibiscus', 'tea-hibiscus', 'thé-hibiscus', 'Dish', 'Plat', 'tea-hibiscus, water, sugar', 'thé-hibiscus, eau, sucre\r'),
(2078, 'Deer', 'Cerf', 'deer; elk', 'cerf ; élan', 'Meat', 'Viande', 'mule deer, deer, elk, meat', 'cerf muletier, cerf, élan, viande\r'),
(2079, 'Octopus', 'Pieuvre', 'octopus', 'poulpe', 'Seafood', 'Fruits de mer', 'octopus, octopus-pacific', 'poulpe, poulpe-pacifique\r'),
(2080, 'Oat Bread', 'Pain d\'avoine', 'bread-oat', 'pain-avoine', 'Bakery', 'Boulangerie', 'bread-oat, oat, bread', 'pain-avoine, avoine, pain\r'),
(2081, 'Pita Bread', 'Pain de pita', 'bread-pita', 'pain-pita', 'Bakery', 'Boulangerie', 'bread-pita, bread', 'pain-pita, pain\r'),
(2082, 'Raisin Bread', 'Pain aux raisins', 'bread-raisin', 'pain-raisin', 'Bakery', 'Boulangerie', 'bread-raisin, bread, raisin', 'pain-raisin, pain, raisin sec\r'),
(2083, 'Multigrain Bread', 'Pain multigrains', 'bread-multigrain', 'pain-multigrain', 'Bakery', 'Boulangerie', 'bread-multigrain, flax-seed, bread, oat', 'pain-multigrains, graines de lin, pain, avoine\r'),
(2084, 'Cornbread', 'Pain de maïs', 'corn-bread', 'pain de maïs', 'Bakery', 'Boulangerie', 'cornbread, corn, bread', 'pain de maïs, maïs, pain\r'),
(2085, 'Pout', 'Pout', 'pout', 'pout', 'Fish', 'Poisson', 'pout-ocean, pout-norway', 'pout-océan, pout-norvège\r'),
(2086, 'Herring', 'Hareng', 'herring', 'hareng', 'Fish', 'Poisson', 'herring-pacific, herring-atlantic', 'hareng-pacifique, hareng-atlantique\r'),
(2087, 'Smelt', 'Éperlan', 'smelt', 'éperlan', 'Fish', 'Poisson', 'smelt-rainbow, smelt', 'éperlan - arc-en-ciel, éperlan\r'),
(2088, 'Crab', 'Crabe', 'crab; crabmeat', 'crabe ; chair de crabe', 'Seafood', 'Fruits de mer', 'crab, red king crab', 'crabe, crabe royal rouge\r'),
(2089, 'Cooking Oil', 'Huile de cuisson', 'cooking oil; vegetable oil', 'huile de cuisson ; huile végétale', 'Additive', 'Additif', 'cooking oil, sunflower', 'huile de cuisson, tournesol\r'),
(2090, 'Rice Cake', 'Gâteau de riz', 'rice-cake', 'gâteau de riz', 'Bakery', 'Boulangerie', 'rice-cake, rice, flour, wheat, ???vegetable-oil???', 'gâteau de riz, riz, farine, blé, huile végétale, etc.\r'),
(2091, 'Trout', 'Truite', 'trout', 'truite', 'Fish', 'Poisson', 'trout-sea, trout-lake, trout-rainbow', 'truite de mer, truite de lac, truite de l\'arc-en-ciel\r'),
(2092, 'Lobster', 'Homard', 'lobster', 'homard', 'Seafood', 'Fruits de mer', 'lobster, lobster-spiny', 'homard, homard épineux\r'),
(2093, 'Halibut', 'Flétan', 'halibut', 'flétan', 'Fish', 'Poisson', 'halibut-atlantic, halibut-greenland', 'flétan de l\'Atlantique, flétan du Groenland\r'),
(2094, 'Whiting', 'Merlan', 'whiting', 'merlan', 'Fish', 'Poisson', 'whiting, whiting-blue', 'merlan, merlan bleu\r'),
(2095, 'Buffalo', 'Buffle', 'buffalo', 'bison', 'Meat', 'Viande', 'buffalo, meat', 'bison, viande\r'),
(2096, 'Catfish', 'Poisson-chat', 'catfish', 'poisson-chat', 'Fish', 'Poisson', 'catfish-channel, catfish', 'poisson-chat - canal, poisson-chat\r'),
(2097, 'Salmon', 'Saumon', 'salmon; arctic char; cisco; salmonidae', 'saumon ; omble chevalier ; cisco ; salmonidés', 'Fish', 'Poisson', 'salmon-coho, salmon-atlantic, salmon-chinook, salmon-chun, salmon-pink, salmon-sockeye, salmon, salmonidae, cisco', 'saumon-coho, saumon-atlantique, saumon-chinook, saumon-chun, saumon-rose, saumon-sockeye, saumon, salmonidae, cisco\r'),
(2098, 'Rabbit', 'Lapin', 'rabbit', 'lapin', 'Meat', 'Viande', 'rabbit, rabbit-european, meat', 'lapin, lapin-européen, viande\r'),
(2099, 'Haddock', 'Haddock', 'haddock', 'églefin', 'Fish', 'Poisson', 'haddock, haddock-norway', 'haddock, haddock-norvège\r'),
(2100, 'Persimmon', 'Persimmon', 'persimmon', 'kaki', 'Fruit', 'Fruits', 'persimmon,persimmon-common', 'kaki, kaki commun\r'),
(2101, 'Apple Sauce', 'Sauce aux pommes', 'apple-sauce', 'compote de pommes', 'Dish', 'Plat', 'apple sauce, apple, lemon juice, sugar', 'compote de pommes, pomme, jus de citron, sucre\r'),
(2102, 'Cider Vinegar', 'Vinaigre de cidre', 'soy cream; soya cream', 'crème de soja ; crème de soja', 'Dish', 'Plat', 'soy cream, soy milk, cooking oil, agave, cider vinegar', 'crème de soja, lait de soja, huile de cuisson, agave, vinaigre de cidre\r');

-- --------------------------------------------------------

--
-- Structure de la table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unity_id` int(11) DEFAULT NULL,
  `recipes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `quantity`, `unity_id`, `recipes_id`) VALUES
(21, 'Fromage Comte', 100, 1, 36),
(22, 'Mate', 100, 1, 36),
(25, 'Mate', 100, 1, 38),
(26, 'Fromage Comte', 100, 1, 38),
(27, 'Fromage Munster', 100, 1, 38),
(28, 'Huile de zeste d\'agrumes', 100, 1, 38);

-- --------------------------------------------------------

--
-- Structure de la table `list`
--

CREATE TABLE `list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `forum_theme` int(11) DEFAULT NULL,
  `messages` text DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `recipes`
--

CREATE TABLE `recipes` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `pictures` varchar(255) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  `is_verified` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `recipes`
--

INSERT INTO `recipes` (`id`, `title`, `description`, `pictures`, `users_id`, `is_verified`) VALUES
(36, 'lasagne', 'faite vos lasagne en cherchant sur le web JPP', '26978903.png', 1, 1),
(38, 'hachi', 'okokokokokokok', '64676d5272e15_26978903.png', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `searchlist`
--

CREATE TABLE `searchlist` (
  `id` int(11) NOT NULL,
  `list_id` int(11) DEFAULT NULL,
  `ingredient_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `search_history`
--

CREATE TABLE `search_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ingredients` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `search_history`
--

INSERT INTO `search_history` (`id`, `user_id`, `ingredients`) VALUES
(1, 2, 'Salade de roquette,Gingembre,Salade de roquette,Pomme de terre,Pommes de terre frites,Chanterelle,Pain de pommes de terre,Fromage Comte,Fromage Munster,Fromage Munster,Mate'),
(5, 2, 'Mate,Fromage Comte,Fromage Munster,Huile de zeste d\'agrumes,Crevette,Crevette,Dattes,Bergamote'),
(6, 2, 'Mate,Fromage Comte,Fromage Munster'),
(9, 1, 'Mate,Fromage Comte,Fromage Munster'),
(10, 1, 'Mate,Fromage Comte,Fromage Munster'),
(11, 1, 'Mate,Fromage Comte,Fromage Munster'),
(12, 1, 'Café,Fromage Feta'),
(13, 1, 'Mate,Fromage Comte,Fromage Munster,Huile de zeste d\'agrumes,Crevette,Crevette,Dattes,Bergamote'),
(14, 1, 'Pain de blé entier'),
(15, 1, 'Mate'),
(16, 1, 'Fromage Munster,Fromage Comte,Mate'),
(17, 1, 'Mate,Fromage Comte,Fromage Munster,Huile de zeste d\'agrumes'),
(18, 1, 'Mate,Fromage Comte'),
(19, 1, 'Mate'),
(20, 1, 'Fromage Comte'),
(21, 1, 'Mate,Fromage Comte,Fromage Munster,Huile de zeste d\'agrumes,Crevette,Crevette,Dattes,Bergamote,Artemisia');

-- --------------------------------------------------------

--
-- Structure de la table `unity`
--

CREATE TABLE `unity` (
  `id` int(11) NOT NULL,
  `unite` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `unity`
--

INSERT INTO `unity` (`id`, `unite`) VALUES
(1, 'gramme'),
(2, 'kilogramme'),
(3, 'milligramme'),
(4, 'cuillère à café'),
(5, 'cuillère à soupe'),
(6, 'litre'),
(7, 'millilitre'),
(9, 'livre'),
(10, 'tasse');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `psw` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `recipes_id` int(11) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `nickname`, `birthday`, `psw`, `email`, `recipes_id`, `isAdmin`) VALUES
(1, 'admin', 'admin', 'admin', '2023-05-16', '$argon2i$v=19$m=65536,t=4,p=1$RmxsSi5LakdFdVUxRnVhaQ$TwRMrZkaVsTQ4PkGPn6w8RkGwSG2wNZ1HmOmhKoU0BA', 'truche@machin.com', NULL, 1),
(2, 'pasAdmin', 'pasAdmin', 'pasAdmin', '1999-11-11', '$argon2i$v=19$m=65536,t=4,p=1$MHZULmtZU0VxbGwzVXdpTA$NNgyoBpjjuT2HKwal2kUJEPilceVzbJEX3hD7Nedg94', 'pasAdmin@pasAdmin.fr', NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`),
  ADD KEY `recipes_id` (`recipes_id`);

--
-- Index pour la table `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Index pour la table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ingredientRecette` (`recipes_id`),
  ADD KEY `unity_id` (`unity_id`);

--
-- Index pour la table `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_theme` (`forum_theme`),
  ADD KEY `users_id` (`users_id`);

--
-- Index pour la table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Index pour la table `searchlist`
--
ALTER TABLE `searchlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `list_id` (`list_id`),
  ADD KEY `ingredient_id` (`ingredient_id`);

--
-- Index pour la table `search_history`
--
ALTER TABLE `search_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `unity`
--
ALTER TABLE `unity`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2103;

--
-- AUTO_INCREMENT pour la table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `list`
--
ALTER TABLE `list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `searchlist`
--
ALTER TABLE `searchlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `search_history`
--
ALTER TABLE `search_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `unity`
--
ALTER TABLE `unity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`recipes_id`) REFERENCES `recipes` (`id`);

--
-- Contraintes pour la table `forum`
--
ALTER TABLE `forum`
  ADD CONSTRAINT `forum_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `ingredients`
--
ALTER TABLE `ingredients`
  ADD CONSTRAINT `ingredients_ibfk_1` FOREIGN KEY (`unity_id`) REFERENCES `unity` (`id`),
  ADD CONSTRAINT `ingredients_ibfk_2` FOREIGN KEY (`recipes_id`) REFERENCES `recipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `list`
--
ALTER TABLE `list`
  ADD CONSTRAINT `list_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`forum_theme`) REFERENCES `forum` (`id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
