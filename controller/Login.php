<?php
    include '../bdd.php';
    $errorMsg = ''; // Variable pour stocker le message d'erreur

    if(!empty($_POST['nickname']) && !empty($_POST['psw']))
    {
        $request = $pdo->query('SELECT * FROM users WHERE nickname = "'.$_POST['nickname'].'"');
        $user = $request->fetch();

        if($user)
        {
            if(password_verify($_POST['psw'], $user['psw']))
            {
                session_start();
                $_SESSION['Login'] = true;
                $_SESSION['email'] = $user['email'];
                $_SESSION['isAdmin'] = $user['isAdmin'];
                $_SESSION['nickname'] = $user['nickname'];
                $_SESSION['id'] = $user['id'];
                $_SESSION['token']= bin2hex(random_bytes(32));
                header("location:../Index.php");
                exit;
            }
            else
            {
                header("location:Login.php?status=pswFalse");
                exit;
            }
        }
        else
        {
            header("location:Login.php?status=nmFalse");
            exit;
        }
    }
    else
    {
        if(isset($_GET['status']) && ($_GET['status'] == 'pswFalse' || $_GET['status'] == 'nmFalse')) {
            $errorMsg = "Pseudo ou mot de passe incorrect";
        }
    } 
    if(isset($_GET['status']))
    {   
        if($_GET['status'] === "passwordHasReset")
        {
            $errorMsg = "Votre mot de passe à été modifié";
        }
    }
include '../Header.php';

?>

<div class="form">
    <h1>Connexion</h1>
    <form method="POST" action="controller/Login.php">
        <label for="nickname">Pseudo :</label><br>
        <input type="text" name="nickname"><br>

        <label for="psw">Mot de passe :</label><br>
        <input type="password" name="psw"><br>

        <input type="submit" value="Envoyer" class="envoyer">
        <p class="error"><?php echo $errorMsg; ?></p> <!-- Affichage du message d'erreur -->
    </form>
    <p>Vous ne possédez pas de compte ? Inscrivez-vous en cliquant <a href="Sign_up.php">ici</a></p>
    <p>Mot de passe ou compte oublié ? Réinitialisez votre mot de passe ou récupérez votre compte <a href="ForgotPassword.php">ici</a></p>
</div>

<?php include '../Footer.php'?>
