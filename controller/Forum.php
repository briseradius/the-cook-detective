<?php
session_start();
include '../Header.php';
require_once "../bdd.php";
?>

<form method="post" action="controller/Forum.php">
  <label>Titre du forum:</label>
  <input type="text" name="thread">
  <input type="hidden" name="users_id" value="1"> <!-- Remplacez la valeur par l'ID de l'utilisateur actuel -->
  <input type="submit" name="create_forum" value="Créer un forum">
</form>


<?php
$userId = $_SESSION['id'];

// Traitement du formulaire de création de forum
if (isset($_POST['create_forum'])) {
    $thread = $_POST['thread'];
    $users_id = $_POST['users_id'];

    try {
        $stmt = $pdo->prepare("INSERT INTO forum (thread, users_id) VALUES (?, ?)");
        $stmt->execute([$thread, $users_id]);
        echo "Le forum a été créé avec succès.";
    } catch (PDOException $e) {
        echo "Une erreur s'est produite lors de la création du forum : " . $e->getMessage();
    }
}

// Traitement du formulaire de création de message (post)
if (isset($_POST['create_post'])) {
    $forum_id = $_POST['forum_id'];
    $messages = $_POST['messages'];
    $users_id = $_POST['users_id'];

    try {
        $stmt = $pdo->prepare("INSERT INTO post (forum_theme, messages, users_id) VALUES (?, ?, ?)");
        $stmt->execute([$forum_id, $messages, $users_id]);
        echo "Le message a été posté avec succès.";
    } catch (PDOException $e) {
        echo "Une erreur s'est produite lors de la création du message : " . $e->getMessage();
    }
}

// Traitement de la modification d'un message
if (isset($_POST['edit_post'])) {
    $post_id = $_POST['post_id'];
    $new_messages = $_POST['new_messages'];
    $users_id = $_POST['users_id'];

    try {
        $stmt = $pdo->prepare("UPDATE post SET messages = ? WHERE id = ? AND users_id = ?");
        $stmt->execute([$new_messages, $post_id, $users_id]);
        echo "Le message a été modifié avec succès.";
    } catch (PDOException $e) {
        echo "Une erreur s'est produite lors de la modification du message : " . $e->getMessage();
    }
}

// Traitement de la suppression d'un message
if (isset($_POST['delete_post'])) {
    $post_id = $_POST['post_id'];
    $users_id = $_POST['users_id'];

    try {
        $stmt = $pdo->prepare("DELETE FROM post WHERE id = ? AND users_id = ?");
        $stmt->execute([$post_id, $users_id]);
        echo "Le message a été supprimé avec succès.";
    } catch (PDOException $e) {
        echo "Une erreur s'est produite lors de la suppression du message : " . $e->getMessage();
    }
}

// Récupération de tous les forums
try {
    $stmt = $pdo->query("SELECT * FROM forum");
    $forums = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($forums as $forum) {
        $forum_id = $forum['id'];
        echo "<h2><a href='controller/ForumPost.php?forum_id=$forum_id'>" . $forum['thread'] . "</a></h2>";
    }
} catch (PDOException $e) {
    echo "Une erreur s'est produite lors de la récupération des forums : " . $e->getMessage();
}

include "../Footer.php";
?>
