<?php
require_once "../bdd.php";

if (isset($_GET['post_id'])) {
    $post_id = $_GET['post_id'];

    try {
        $stmt_post = $pdo->prepare("SELECT messages FROM post WHERE id = ?");
        $stmt_post->execute([$post_id]);
        $post = $stmt_post->fetch(PDO::FETCH_ASSOC);

        if ($post) {
            echo $post['messages'];
        } else {
            echo "Message non trouvé.";
        }
    } catch (PDOException $e) {
        echo "Une erreur s'est produite lors de la récupération du message : " . $e->getMessage();
    }
}
?>
