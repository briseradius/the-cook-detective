<?php 
session_start();
include '../Header.php';
include '../bdd.php';
?>

<form action="model/newRecipe.php" method="post" enctype="multipart/form-data">
  <div>
    <label for="title">Recette du :</label>
    <input type="text" id="title" name="title" placeholder="Nom de la recette" required>
  </div>
  <div class="">
    <label for="ingredients">Ingrédients :</label>
    <input type="text" id="searchIngredient" placeholder="Rechercher un ingrédient">
    <button class="customButtonRecipes" type="button" id="searchButton">Rechercher</button>
    <div class="flex row" id="ingredientSearch">
      <div class="select"></div>
      <input type="number" id="ingredientQuantity" placeholder="Quantité" min="0">
      <select class="selectUnity" id="ingredientUnit">
        <?php
          // Récupérer les unités depuis la base de données
          $sql = "SELECT * FROM unity";
          $stmt = $pdo->prepare($sql);
          $stmt->execute();
          $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

          // Générer les options du sélecteur
          foreach ($result as $row) {
            echo '<option value="' . htmlspecialchars($row["id"]) . '">' . htmlspecialchars($row["unite"]) . '</option>';
          }
        ?>
      </select>
      <div id="ingredientSelect"></div>
      <input type="hidden" name="hiddenIngredients" id="hiddenIngredients" value="">
      <button class="customButtonRecipes" type="button" id="addButton">Ajouter l'ingrédient</button>
    </div>
    <div id="ingredientsList"></div>
  </div>
  <div>
    <label for="description">Description :<br></label>
    <textarea class="recipesDesc" id="description" name="description" required></textarea>
  </div>
  <div>
    <label for="picture">Image :</label>
    <input type="file" id="picture" name="picture" accept="image/*" required>
  </div>
  <div>
    <input id='addRecipeButton' class="customButtonRecipes" type="submit" value="Ajouter la recette">
  </div>
</form>

<script defer>
  document.getElementById('searchButton').addEventListener('click', searchIngredient);
  document.getElementById('addButton').addEventListener('click', addIngredient);
  // Tableau pour stocker les ingrédients
  var ingredients = [];

  function searchIngredient() {
    var searchValue = document.getElementById('searchIngredient').value;
    var selectContainer = document.querySelector('.select');

    // Requête AJAX pour récupérer les ingrédients correspondant à la recherche
    fetch('model/search_ingredient.php?search=' + encodeURIComponent(searchValue))
      .then(response => response.json())
      .then(data => {
        // Réinitialiser le conteneur
        selectContainer.innerHTML = '';

        // Créer le select et l'ajouter au conteneur
        var selectElement = document.createElement('select');
        selectElement.id = 'ingredientSelectList';
        selectElement.className = 'selectUnity';

        // Parcourir les résultats et créer les options
        data.forEach(ingredient => {
          var option = document.createElement('option');
          option.value = ingredient;
          option.text = ingredient;
          selectElement.appendChild(option);
        });

        // Ajouter le select au conteneur
        selectContainer.appendChild(selectElement);
      })
      .catch(error => console.error(error));
  }

  function addIngredient() {
    var ingredientId = document.getElementById('ingredientSelectList').value;
    var ingredientName = document.getElementById('ingredientSelectList').options[document.getElementById('ingredientSelectList').selectedIndex].text;
    var ingredientQuantity = document.getElementById('ingredientQuantity').value;
    var ingredientUnit = document.getElementById('ingredientUnit').value;

    // Concaténer la quantité, l'unité et le nom de l'ingrédient
    var ingredient = {
      id: ingredientId,
      name: ingredientName,
      quantity: ingredientQuantity,
      unity: ingredientUnit
    };

    // Ajouter l'ingrédient au tableau
    ingredients.push(ingredient);

    // Afficher la liste des ingrédients
    displayIngredients();

    // Mettre à jour la valeur du champ caché avec le tableau d'ingrédients
    var hiddenIngredients = document.getElementById('hiddenIngredients');
    hiddenIngredients.value = JSON.stringify(ingredients);

    // Réinitialiser les champs
    document.getElementById('searchIngredient').value = '';
    document.getElementById('ingredientQuantity').value = '';
    document.getElementById('ingredientUnit').value = 'gramme';
    document.getElementById('ingredientSelect').innerHTML = '';
  }

  function displayIngredients() {
  var ingredientsListContainer = document.getElementById('ingredientsList');

  // Réinitialiser le conteneur
  ingredientsListContainer.innerHTML = '';

  // Parcourir le tableau des ingrédients et les afficher
  ingredients.forEach(function(ingredient) {
    // Rechercher l'unité associée à l'ID
    var unity = '';
    <?php
      // Récupérer les unités depuis la base de données
      $sql = "SELECT * FROM unity";
      $stmt = $pdo->prepare($sql);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach ($result as $row) {
        echo 'if (' . $row["id"] . ' == ingredient.unity) {
          unity = "' . htmlspecialchars($row["unite"]) . '";
        }';
      }
    ?>

    var ingredientItem = document.createElement('div');
    var ingredientText = ingredient.quantity + '' + unity + ' ' + ingredient.name;
    ingredientItem.textContent = ingredientText;
    ingredientsListContainer.appendChild(ingredientItem);
  });
}

</script>

<?php include '../Footer.php' ?>
