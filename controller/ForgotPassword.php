<?php session_start();
include 'Header.php';
include 'bdd.php';
if (!isset($_GET['status'])) {
    $errorMsg = "";
} else {
    switch ($_GET['status']) {
        case "mailError":
            $errorMsg = "Une erreur s'est produite lors de l'envoi de l'e-mail";
            break;
        case "mailUnset":
            $errorMsg = "Adresse e-mail non valide";
            break;
        case "mailSend":
            $errorMsg = "Un e-mail a été envoyé sur votre compte associé. Veuillez le consulter et suivre les instructions";
            break;
        default:
            $errorMsg = "";
            break;
    }
}
?>
<div class="form">
    <h1>Mot de passe oublié</h1>
    <form method="POST" action="MailForgotPassword.php">
        <label for="email">Adresse email :</label><br>
        <input type="email" name="email"><br>
        
        <input type="submit" value="Envoyer" class="envoyer">
    </form>
    <p class="error"><?php echo $errorMsg; ?></p> 
    <p>Retour à la page de <a href="Login.php">connexion</a></p>
</div>

<?php include 'Footer.php'?>
