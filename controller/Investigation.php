<?php session_start();
include '../Header.php';
include '../bdd.php';
?>
<div class="news">
    <div class="new">
        <h2>Investigation</h2>
        <?php 
        if (!empty($_SESSION['Login'])){?>
        <form id="ingredient-form" class="flex row">
            <input type="text" id="search_bar" class="" placeholder="Entrez les ingrédients" aria-label="Entrez les ingrédients que vous avez" aria-describedby="button-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary rounded-0" type="submit" id="button-addon2">Rechercher</button>
            </div>
        </form>
        <div class="flex row">
            <div id="ingredient-list"></div>
            <img class="imgInvest" src="image/ingredient_invest.jpeg" alt="food ingredients nouriture" max-width="500px" height="auto">
        </div><?php }
        else{ ?>
        <p>Pour accéder à la section "Investigation", veuillez vous connecter à votre compte ou créer un nouveau compte si vous n'en avez pas encore.</p>
        <p>L'inscription est rapide et gratuite ! En vous inscrivant, vous pourrez profiter de toutes les fonctionnalités de notre plateforme, y compris la possibilité de mener des enquêtes et de découvrir de nouvelles recettes passionnantes.</p>
        <p>Connectez-vous dès maintenant pour explorer l'univers du Cook Detective et plonger dans le monde de la cuisine avec une perspective d'enquête unique.</p>
        <p>Si vous ne possédez pas de compte, vous pouvez vous inscrire en cliquant sur le lien "Inscription" dans le menu de navigation ou <a href="controller/Sign_up.php">cliquez ici</a>.</p>

        <?php } ?>
    </div>
</div>

<script defer>
// Sélection des éléments de l'interface
var searchForm = document.getElementById("ingredient-form");
var searchInput = document.getElementById("search_bar");
var ingredientList = document.getElementById("ingredient-list");

// Gestionnaire d'événement pour la soumission du formulaire
searchForm.addEventListener("submit", function(event) 
{
  event.preventDefault(); // Empêche le rechargement de la page

  var searchTerm = searchInput.value.trim();

  // Vérifiez si le terme de recherche n'est pas vide
  if (searchTerm !== "") 
  {
    // AJAX methode fetch
    fetch("model/search.php?term=" + encodeURIComponent(searchTerm))
      .then(function(response) 
      {
        if (response.ok) 
        {
          return response.json();
        }
        throw new Error("Erreur lors de la requête AJAX");
      })
      .then(function(results) 
      {
        displaySearchResults(results);
      })
      .catch(function(error) 
      {
        console.log(error);
      });
  }
});

// Fonction pour afficher les résultats de la recherche
function displaySearchResults(results) 
{
  ingredientList.innerHTML = ""; // Efface les résultats précédents

  if (results.length === 0) 
  {
    ingredientList.innerHTML = "Aucun résultat trouvé.";
    return;
  }

  // Parcourir les résultats et créer des éléments pour chaque résultat
  results.forEach(function(result) 
  {
    var ingredientItem = document.createElement("div");
    ingredientItem.classList = "custom-div";
    ingredientItem.textContent = result.IngredientNameFR;

    var addButton = document.createElement("button");
    addButton.classList = "custom-button";
    addButton.textContent = "Ajouter à la liste";
    addButton.addEventListener("click", function() 
    {
      addToUserList(result.id);
    });

    ingredientItem.appendChild(addButton);
    ingredientList.appendChild(ingredientItem);
  });
}

// Fonction pour ajouter l'ingrédient à la liste de l'utilisateur
function addToUserList(ingredientId) 
{
  // Effectuer AJAX
  fetch("model/add-to-list.php", 
  {
    method: "POST",
    headers: 
    {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: "ingredientId=" + encodeURIComponent(ingredientId)
  })
    .then(function(response) 
    {
      if (response.ok) 
      {
        return response.json();
      }
      throw new Error("Erreur lors de la requête AJAX");
    })
    .then(function(response) 
    {
      if (response.success) 
      {
        // Mettre à jour le badge
        updateBadgeValue();
        // Afficher un message de confirmation
        console.log("Ingrédient ajouté à la liste avec succès.");
      } 
      else 
      {
        console.log("Erreur lors de l'ajout de l'ingrédient à la liste.");
      }
    })
    .catch(function(error) 
    {
      console.log(error);
    });
}

</script>

<?php include '../Footer.php'?>