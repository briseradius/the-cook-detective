<?php
session_start();
include '../Header.php';
require_once "../bdd.php";

// Vérifier si l'ID du forum est présent dans l'URL
if (isset($_GET['forum_id'])) {
    $forum_id = $_GET['forum_id'];

    // Récupérer les informations sur le forum
    try {
        $stmt_forum = $pdo->prepare("SELECT * FROM forum WHERE id = ?");
        $stmt_forum->execute([$forum_id]);
        $forum = $stmt_forum->fetch(PDO::FETCH_ASSOC);

        if ($forum) {
            echo "<h2>Forum: " . $forum['thread'] . "</h2>";

            // Récupérer les messages (posts) pour ce forum
            $stmt_posts = $pdo->prepare("SELECT * FROM post WHERE forum_theme = ?");
            $stmt_posts->execute([$forum_id]);
            $posts = $stmt_posts->fetchAll(PDO::FETCH_ASSOC);

            if (count($posts) > 0) {
                // Afficher les messages (posts)
                foreach ($posts as $post) {
                    echo "<div>";
                    echo "<p>" . $post['messages'] . "</p>";

                    // Afficher les boutons de modification et de suppression uniquement pour les messages de l'utilisateur actuel
                    if ($post['users_id'] == $_SESSION['id']) {
                        echo "<form method='post' action='controller/Forum.php'>";
                        echo "<input type='hidden' name='post_id' value='" . $post['id'] . "'>";
                        echo "<input type='hidden' name='users_id' value='" . $_SESSION['id'] . "'>";

                        // Vérifier si le formulaire de modification doit être affiché
                        if (isset($_GET['edit']) && $_GET['edit'] == $post['id']) {
                            echo "<textarea class='forum' name='new_messages'>" . $post['messages'] . "</textarea>";
                            echo "<input type='submit' name='edit_post_submit' value='Valider'>";
                        } else {
                            echo "<input type='hidden' name='new_messages' value='" . $post['messages'] . "'>";
                            echo "<button type='submit' name='edit_post'>Modifier</button>";
                        }

                        echo "<input type='submit' name='delete_post' value='Supprimer'>";
                        echo "</form>";
                    }

                    echo "</div>";
                }
            } else {
                echo "<p>Aucun message dans ce forum.</p>";
            }

            // Formulaire pour créer un nouveau message (post) dans ce forum
            echo "<form method='post' action='controller/Forum.php'>";
            echo "<input type='hidden' name='forum_id' value='" . $forum_id . "'>";
            echo "<input type='hidden' name='users_id' value='" . $_SESSION['id'] . "'>";
            echo "<textarea id='forum' name='messages'></textarea>";
            echo "<input type='submit' name='create_post' value='Poster'>";
            echo "</form>";
        } else {
            echo "<p>Forum non trouvé.</p>";
        }
    } catch (PDOException $e) {
        echo "Une erreur s'est produite lors de la récupération des messages du forum : " . $e->getMessage();
    }
} else {
    echo "<p>Identifiant du forum non spécifié.</p>";
}

include "../Footer.php";
?>
