<?php include '../Header.php'?>

<div class="form">
<h1>Inscription</h1>
    <form method="POST" action="model/newUser.php">
            <label for="userFirstName">*Prenom :</label><br>
            <input type="text" name="userFirstName" required><br>

            <label for="userLastName">*Nom :</label><br>
            <input type="text" name="userLastName" required><br>

            <label for="nickname">*Pseudo :</label><br>
            <input type="text" name="nickname" required><br>
            
            <label for="age">*Date de naissance :</label><br>
            <input type="date" name="age" required><br>
            
            <label for="email">*Adresse email :</label><br>
            <input type="email" name="email" required><br>
            
            <label for="psw">*Mot de passe :</label><br>
            <input type="password" name="psw" required><br>
            
            <label for="confirm_psw">*Confirmer le mot de passe :</label><br>
            <input type="password" name="confirm_psw" required><br>
            
            <input type="submit" value="Envoyer" class="envoyer">
    </form>
    <p>Vous possedez déjà un compte ? Connectez-vous en cliquant <a href="controller/Login.php">ici</a></p>
</div>


<?php include '../Footer.php'?>