<?php
include "../bdd.php";

// Récupération du terme de recherche
$searchTerm = $_GET['term'];

// Requête préparée pour rechercher les ingrédients correspondants
$stmt = $pdo->prepare("SELECT * FROM ingredient WHERE IngredientNameFR LIKE ?");
$stmt->execute(["%$searchTerm%"]);

// Tableau pour stocker les résultats
$results = [];

// Parcours des résultats de la requête
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $results[] = $row;
}

// Renvoi des résultats au format JSON
header('Content-Type: application/json');
echo json_encode($results);
?>
