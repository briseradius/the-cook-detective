<?php
session_start();
include '../Header.php';
include "../bdd.php";
try 
{
    // ID de l'utilisateur en cours de session
    $userId = $_SESSION['id'];

    // Requête SQL pour récupérer l'historique de recherche de l'utilisateur
    $sql = "SELECT * FROM search_history WHERE user_id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$userId]);

    // Vérification des résultats de la requête
    if ($stmt->rowCount() > 0) {
        echo "<h2>Mes historiques de recherche :</h2>";
        echo "<ul>";
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $searchId = $row['id'];
            $ingredientsList = $row['ingredients'];

            // Afficher chaque recherche avec un bouton de recherche par ingrédient
            echo "<li>";
            echo $ingredientsList;
            echo "<br> <a class='historyLink' href='model/search_by_ingredient.php?searchId=$searchId'>Rechercher</a>";
            echo " <a class='historyLink' href='model/deleteHistory.php?searchId=$searchId'>Supprimer</a>";
            echo "</li>";
        }
        echo "</ul>";
    } 
    else 
    {
        echo "Aucun historique de recherche trouvé.";
    }
} 
catch (PDOException $e) 
{
    echo "Erreur lors de la connexion à la base de données : " . $e->getMessage();
}

include '../Footer.php';
?>
