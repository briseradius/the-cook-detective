<?php 
include "../bdd.php";

if (!empty($_POST['email']) && !empty($_POST['psw']) && !empty($_POST['userFirstName']) && !empty($_POST['userLastName']) && !empty($_POST['nickname']))
{
    if ($_POST['psw'] === $_POST['confirm_psw'])
    {   
        $mail = $_POST['email'];
        $verifUser = $pdo->query('SELECT COUNT(*) FROM users WHERE email= "'.$mail.'"');
        $verifUserNbs = $verifUser->fetch();

        if ($verifUserNbs[0] == 0)
        {
            try
            {
                $psw = password_hash($_POST['psw'], PASSWORD_ARGON2I);
                $role = '';
                $addU = $pdo->prepare("INSERT INTO users (firstname, lastname, nickname, birthday,  psw, email) VALUES (?, ?, ?, ?, ?, ?)");
                $addUser = $addU->execute(array($_POST['userFirstName'], $_POST['userLastName'], $_POST['nickname'], $_POST['age'], $psw, $mail));

                header("location:../Index.php?status=Ok&message=Enregistrement reussi");
                exit;
            }
            catch (PDOException $erreur)
            {
                echo "une erreur est survenue: " .$erreur->getMessage();
            }
        }
        else
        {
            echo "le mail choisi est déjà utilisé veuillez en sélectionner un autre";
            header("location:../controller/Sign_up.php?status=erreur&message=le mail déjà choisi");
        }
    }    
    else
    {
        echo "Les mots de passe ne correspondent pas";
    }
}
?>
