<?php include "../bdd.php";

    $errorMsg = ''; 

    if(!empty($_POST))
    {
        // Vérification de l'adresse e-mail et envoi de l'e-mail avec le lien pour réinitialiser le mot de passe
        $email = $_POST['email'];
        
        $request = $pdo->prepare('SELECT * FROM users WHERE email = :email');
        $request->execute(['email' => $email]);
        $user = $request->fetch();

        if($user)
        {
            // Génération d'un token unique pour le lien de réinitialisation du mot de passe
            $token = bin2hex(random_bytes(32));
            
            // Enregistrement du token dans la base de données pour cet utilisateur
            $update = $pdo->prepare('UPDATE users SET reset_token = :token WHERE id = :id');
            $update->execute(['token' => $token, 'id' => $user['id']]);
            
            // Envoi de l'e-mail avec le lien de réinitialisation du mot de passe
            $to = $email;
            $subject = 'Réinitialisation du mot de passe';
            $message = 'Bonjour '.$user['nickname'].',<br><br>';
            $message .= 'Veuillez cliquer sur le lien suivant pour réinitialiser votre mot de passe :<br>';
            $message .= '<a href="https://TheCookDetective/reset_password.php?token='.$token.'">Réinitialiser le mot de passe</a><br>';
            $message .= 'Si vous n\'avez pas demandé de réinitialisation de mot de passe, veuillez ignorer cet e-mail.<br><br>';
            $message .= 'Cordialement,<br>';
            $message .= 'Toute l\'equipe de THE COOK DETECTIVE vous remercie de votre confiance';
            
            $headers = 'From: adresse mail du site' . "\r\n" .
                       'Reply-To: adresse mail du site' . "\r\n" .
                       'Content-Type: text/html; charset=UTF-8' . "\r\n" .
                       'X-Mailer: PHP/' . phpversion();
            
            if(mail($to, $subject, $message, $headers)) {
                //var_dump($token);
                header("location: ForgotPassword.php?status=mailSend");
            } 
            else 
            {
                header("location: ForgotPassword.php?status=mailError");
            }
        }
        else
        {
            header("location: ForgotPassword.php?status=mailUnset");
        }
    }
?>