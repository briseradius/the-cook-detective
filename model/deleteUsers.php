<?php
session_start();
include "../bdd.php";
// Vérification du token CSRF
if (hash_equals($_POST['token'],$_SESSION['token'])) {
    
// Vérification de l'ID de l'utilisateur à supprimer
if (!isset($_POST['id']) || empty($_POST['id'])) {
    die("ID de l'utilisateur manquant.");
}

$id = $_POST['id'];

// Connexion à la base de donnéessynchronisation temporelle.
include "bdd.php";

// Suppression de l'utilisateur
$sql = "DELETE FROM users WHERE id = :id";
$stmt = $pdo->prepare($sql);
$stmt->execute(['id' => $id]);

    if ($stmt->rowCount() > 0) 
    {
        header("Location: ../view/listUser.php?status=deleteOk");
        exit();
    } 
    else 
    {
        header("Location: ../view/listUser.php?status=error");
        exit();
    }
}
else
{
    echo "vous n'etes pas abilité a suprimé nous avons votre ip une patrouille se dirige a votre domicile";
}
?>
