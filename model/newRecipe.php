<?php
session_start();
include '../Header.php';
include "../bdd.php";
//var_dump($_POST); 
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
  try 
  {
    // Récupérer les données du formulaire
    $title = $_POST["title"];
    $description = $_POST["description"];
    $picture = $_FILES["picture"]["name"];
    $userId = $_SESSION['id'];

    // Vérifier le format de l'image
    $allowedFormats = ['image/jpeg', 'image/png', 'image/jpg', 'image/bmp', 'image/webp', 'image/gif'];
    $imageInfo = getimagesize($_FILES["picture"]["tmp_name"]);
    $fileType = $imageInfo['mime'];

    if (!in_array($fileType, $allowedFormats)) {
      throw new Exception("Le format de l'image n'est pas valide. Veuillez choisir une image au format JPEG, PNG ou GIF.");
    }

    // Générer un nom unique pour le fichier téléchargé
    $uniqueFileName = uniqid() . '_' . $picture;

    // Insérer la recette dans la table "recipes"
    $stmt = $pdo->prepare("INSERT INTO recipes (title, description, pictures, users_id) VALUES (?, ?, ?, ?)");
    $stmt->execute([$title, $description, $uniqueFileName, $userId]);

    // Récupérer l'ID de la recette insérée
    $recipeId = $pdo->lastInsertId();

    // Insérer les ingrédients dans la table "ingredients"
    $hiddenIngredients = $_POST["hiddenIngredients"];
    $ingredients = json_decode($hiddenIngredients, true);

    foreach ($ingredients as $ingredient) 
    {
      $name = $ingredient["name"];
      $quantity = $ingredient["quantity"];
      $unityId = $ingredient["unity"];

      $stmt = $pdo->prepare("INSERT INTO ingredients (name, quantity, unity_id, recipes_id) VALUES (?, ?, ?, ?)");
      $stmt->execute([$name, $quantity, $unityId, $recipeId]);
    }

    // Déplace le fichier image vers le répertoire de destination
    $targetDir = "../uploads/";
    $targetFilePath = $targetDir . $uniqueFileName;
    move_uploaded_file($_FILES["picture"]["tmp_name"], $targetFilePath);

    // Rediriger l'utilisateur vers une page de succès ou afficher un message de succès
    echo "Recette ajoutée avec succès !";
  } 
  catch (PDOException $e) 
  {
    // Gérer les erreurs de la base de données
    echo "Une erreur s'est produite lors de l'ajout de la recette : " . $e->getMessage();
  } 
  catch (Exception $e) 
  {
    // Gérer les autres erreurs
    echo "Une erreur s'est produite : " . $e->getMessage();
  }
}
?>
