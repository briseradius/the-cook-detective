<?php
session_start();
include '../Header.php';
include "../bdd.php";
try 
{
    // ID de l'utilisateur en cours de session
    $userId = $_SESSION['id'];

    // Requête SQL pour récupérer les ingrédients de l'utilisateur depuis la table list avec les informations de la table ingredient
    $sql = "SELECT i.IngredientNameFR
            FROM list l
            JOIN ingredient i ON l.name = i.id
            WHERE l.user_id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$userId]);

    // Tableau pour stocker les noms d'ingrédients
    $listeIngredients = [];

    // Vérification des résultats de la requête
    if ($stmt->rowCount() > 0) 
    {
        // Récupérer les noms d'ingrédients dans un tableau
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $listeIngredients[] = $row['IngredientNameFR'];
        }

        // Afficher les noms d'ingrédients
        echo "<h2>Liste des ingrédients :</h2>";
        echo "<ul>";
        foreach ($listeIngredients as $ingredient) {
            echo "<li>" . $ingredient . "</li>";
        }
        echo "</ul>";

        // Convertir le tableau en une chaîne de caractères séparée par des virgules
        $ingredientsList = implode(',', $listeIngredients);

        // Insérer les ingrédients dans la table search_history
        $insertSql = "INSERT INTO search_history (user_id, ingredients) VALUES (?, ?)";
        $insertStmt = $pdo->prepare($insertSql);
        $insertStmt->execute([$userId, $ingredientsList]);

        // Récupérer l'ID de la dernière insertion
        $searchId = $pdo->lastInsertId();

        // Supprimer les ingrédients de la table list
        $deleteSql = "DELETE FROM list WHERE user_id = ?";
        $deleteStmt = $pdo->prepare($deleteSql);
        $deleteStmt->execute([$userId]);

        echo "<h4><a href='model/search_by_ingredient.php?searchId=$searchId'>Rechercher</a></h4>";
    } 
    else 
    {
        echo "Aucun ingrédient trouvé.";
    }
} 
catch (PDOException $e) 
{
    echo "Erreur lors de la connexion à la base de données : " . $e->getMessage();
}

echo "<h4><a href='model/search_history.php'>Historique de recherche</a></h4>";

include '../Footer.php';
?>
