<?php
include "../bdd.php";
session_start();
// Récupération de l'ID de l'ingrédient et de l'ID de l'utilisateur à partir de la session en cours
$ingredientId = $_POST['ingredientId'];
$userId = $_SESSION['id'];

// Requête préparée pour ajouter l'ingrédient à la liste de l'utilisateur
$stmt = $pdo->prepare("INSERT INTO `list` (`name`, `user_id`) VALUES (?, ?)");
$stmt->execute([$ingredientId, $userId]);

if ($stmt->rowCount() > 0) {
    $response = array("success" => true);
} else {
    $response = array("success" => false);
}

// Renvoi de la réponse au format JSON
header('Content-Type: application/json');
echo json_encode($response);
?>
