<?php
include "../bdd.php";

session_start();
$userId = $_SESSION['id'];

// Requête SQL pour compter le nombre d'entrées avec le même user_id en utilisant une requête préparée
$stmt = $pdo->prepare("SELECT COUNT(*) as count FROM `list` WHERE `user_id` = ?");
$stmt->execute([$userId]);
$result = $stmt->fetch();

// Vérification du résultat de la requête
if ($result) 
{
    // Récupération du nombre d'entrées
    $count = $result['count'];
    // Retourner la valeur du compteur au format JSON
    echo json_encode($count);
} 
else 
{
    // En cas d'erreur, retourner une valeur par défaut ou une erreur spécifique
    echo json_encode(0);
}
?>
