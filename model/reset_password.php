<?php
include "../bdd.php";

$errorMsg = '';

if (!empty($_POST)) {
    $token = $_POST['token'];
    $password = $_POST['password'];
    $confirmPassword = $_POST['confirm_password'];

    // Vérification du token
    $request = $pdo->prepare('SELECT * FROM users WHERE reset_token = :token');
    $request->execute(['token' => $token]);
    $user = $request->fetch();

    if ($user) {
        // Vérification du mot de passe et de sa confirmation
        if ($password === $confirmPassword) {
            // Hachage du mot de passe avec Argon2i
            $hashedPassword = password_hash($password, PASSWORD_ARGON2I);
            // Mise à jour du mot de passe dans la base de données
            $update = $pdo->prepare('UPDATE users SET password = :password, reset_token = NULL WHERE id = :id');
            $update->execute(['password' => $hashedPassword, 'id' => $user['id']]);
            header("location: controller/login.php?staus=passwordHasReset");
        } else {
            $errorMsg = "Les mots de passe ne correspondent pas.";
        }
    } else {
        $errorMsg = "authentification echoué veillez recommencer la procedure et utiliser le lien du mail.";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Réinitialisation du mot de passe</title>
</head>
<body>
    <h1>Réinitialisation du mot de passe</h1>
    <?php if ($errorMsg !== ''): ?>
        <p><?php echo $errorMsg; ?></p>
    <?php endif; ?>
    <form method="POST" action="reset_password.php">
        <input type="hidden" name="token" value="<?php echo $_GET['token']; ?>">
        <label for="password">Nouveau mot de passe :</label>
        <input type="password" name="password" required>
        <label for="confirm_password">Confirmez le nouveau mot de passe :</label>
        <input type="password" name="confirm_password" required>
        <input type="submit" value="Réinitialiser le mot de passe">
    </form>
</body>
</html>
