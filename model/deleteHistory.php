<?php
include "../bdd.php";

// Récupérer les ingrédients à partir de $_GET['searchId']
$searchId = isset($_GET['searchId']) ? $_GET['searchId'] : null;

// Vérifier si l'ID de recherche est présent et est un entier positif
if (!is_numeric($searchId) && $searchId <= 0) {
    die("ID de recherche invalide.");
  }

// Requête SQL pour supprimer l'entrée d'historique de recherche correspondante
$sql = "DELETE FROM search_history WHERE id = ?";
$stmt = $pdo->prepare($sql);
$result = $stmt->execute([$searchId]);

// Vérification du succès de la suppression
if ($result) 
{
    header("location:search_history.php?status=Ok&message=suppression reussi");
} 
else 
{
    echo "Erreur lors de la suppression de l'historique de recherche.";
}
?>
<?php
include "bdd.php";
session_start();

// Valider et filtrer l'ID de recherche
$searchId = filter_input(INPUT_GET, 'searchId', FILTER_VALIDATE_INT);

if ($searchId) 
{
    // Vérifier l'ID de l'utilisateur
    $userId = $_SESSION['id'];

    // Requête SQL pour supprimer l'entrée d'historique de recherche correspondante pour l'utilisateur spécifique
    $sql = "DELETE FROM search_history WHERE id = ? AND user_id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$searchId, $userId]);

    // Vérification du succès de la suppression
    if ($stmt->rowCount() > 0) 
    {
        header("location:search_history.php?status=Ok&message=Suppression réussie");
        exit();
    } 
    else 
    {
        header("location:search_history.php?status=error&message=Erreur lors de la suppression de l'historique de recherche");
        exit();
    }
} 
else 
{
    header("location:search_history.php?status=error&message=ID de recherche invalide");
    exit();
}
?>
