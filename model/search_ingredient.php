<?php
include "../bdd.php";

// Récupération du terme de recherche
$searchTerm = $_GET['search'];

// Requête préparée pour rechercher les ingrédients correspondants
$stmt = $pdo->prepare("SELECT * FROM ingredient WHERE IngredientNameFR LIKE ?");
$stmt->execute(["%$searchTerm%"]);

// Construction de la liste déroulante des ingrédients correspondants
$ingredients = [];
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  $ingredientName = $row['IngredientNameFR'];
  $ingredients[] = $ingredientName;
}

echo json_encode($ingredients);
?>
