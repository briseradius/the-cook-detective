<?php
session_start();
include '../Header.php';
include "../bdd.php";

// Vérifier la connexion
if (!$pdo) {
  die("Erreur de connexion à la base de données : " . mysqli_connect_error());
}

// Récupérer les ingrédients à partir de $_GET['searchId']
$searchId = isset($_GET['searchId']) ? $_GET['searchId'] : null;

// Vérifier si l'ID de recherche est présent et est un entier positif
if (!is_numeric($searchId) || $searchId <= 0) {
  die("ID de recherche invalide.");
}

// Requête SQL pour récupérer les ingrédients associés à l'ID
$stmt = $pdo->prepare("SELECT ingredients FROM search_history WHERE id = ?");
$stmt->execute([$searchId]);
$searchIngredientsRow = $stmt->fetch(PDO::FETCH_ASSOC);

if ($searchIngredientsRow) {
  $searchIngredients = $searchIngredientsRow['ingredients'];
  $searchIngredientsArray = explode(',', $searchIngredients);

  // Requête SQL pour rechercher les recettes correspondantes
  $sql = "SELECT r.id, r.title, r.pictures, r.is_verified
          FROM recipes r
          JOIN ingredients i ON r.id = i.recipes_id
          WHERE r.id NOT IN (
              SELECT DISTINCT recipes_id
              FROM ingredients
              WHERE name NOT IN (" . implode(',', array_fill(0, count($searchIngredientsArray), '?')) . ")
          )
          GROUP BY r.id";

  $stmt = $pdo->prepare($sql);
  $stmt->execute($searchIngredientsArray);
  $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

  // Afficher les résultats
  foreach ($results as $recipe) {
    if ($recipe['is_verified']) {
      echo '<div class="card">';
      echo '<img src="uploads/' . $recipe['pictures'] . '" alt="Photo de la recette">';
      echo '<h2>' . $recipe['title'] . '</h2>';
      echo '<h3>Ingrédients :</h3>';

      $stmt = $pdo->prepare("SELECT name FROM ingredients WHERE recipes_id = ?");
      $stmt->execute([$recipe['id']]);
      $ingredients = $stmt->fetchAll(PDO::FETCH_COLUMN);

      echo '<ul>';
      foreach ($ingredients as $ingredient) {
        echo '<li>' . $ingredient . '</li>';
      }
      echo '</ul>';

      echo '<a href="usersViewRecipes.php?id=' . $recipe['id'] . '">Consulter</a>';
      echo '</div>';
    }
  }
} else {
  echo "Aucun résultat trouvé.";
}

include '../Footer.php';
?>
