<?php
include '../bdd.php';

// Vérifier si un ID de commentaire est passé en paramètre
if (!isset($_GET['comment-id'])) {
  header("Location: ../view/usersCheckRecipes.php"); // Rediriger vers la page de liste des recettes si aucun ID de commentaire n'est spécifié
  exit();
}

// Récupérer l'ID du commentaire à supprimer
$commentId = $_GET['comment-id'];

// Mettre à jour la colonne isDelete du commentaire dans la base de données
$updateQuery = $pdo->prepare("UPDATE comments SET isDelete = 1 WHERE id = ?");
$updateQuery->execute([$commentId]);

// Rediriger vers la page précédente (ou une autre page appropriée)
header("Location: ../view/usersViewRecipes.php?id=" . $_GET['recipe-id']);
exit();
?>
