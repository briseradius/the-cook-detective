<?php 
 // Démarre la mise en tampon de sortie
ob_start();
include 'bdd.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="TCD" content="Author: J.G. Lours">
    <base href="http://127.0.0.1/cook_detective2/the-cook-detective/" />
    <link id="cssLink" rel="stylesheet" href="style.css">
    <script src="main.js" defer></script>
    <title>The Cook Detective</title>
</head>
<body>

<!-- header -->
<div class="overlay_menu" onclick="closeMenuMobile()"></div>
<div class="flex row header">
    <div class="flex row logo">
        <img id="detectiveImage" src="image/image-removebg-preview.png" alt="nouriture food detective cook"></img>
        <h2>The <br>Cook <br>Detective</h2>
    </div>

    <div class="flex row navbar">
        <div class="navbar_close" onclick="closeMenuMobile()">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </div>
        <?php
        // Vérification de la session de connexion
        if (empty($_SESSION['Login'])) { ?>
            <li><a href="Index.php">Accueil</a></li>
            <li><a href="controller/Investigation.php">Investigation</a></li>                      
            <li><a href="view/recettes.php">Recettes</a></li> 
            <li><a href="controller/Sign_up.php">Inscription</a></li>
            <li><a href="controller/Login.php">Connexion</a></li>
            <li class="HF">&#9794;<label class="switch"><input type="checkbox" onclick="toggleStyle()"/><span></span></label>&#9792;</li>
<!-- <input type="checkbox" class="custom-button" >  -->
        <?php }
        // Utilisateur connecté
        if (!empty($_SESSION['Login'])) { ?>
            <li><a href="Index.php">Accueil</a></li>                                                 
            <li><a href="controller/Investigation.php">Investigation</a></li>                      
            <li><a href="view/usersCheckRecipes.php">Recettes</a></li> 
            <li><a href="controller/addRecipeForm.php">Vos Recettes</a></li> 
            <li><a href="model/Logout.php">Déconnexion</a></li>   
        <?php } 
        // Vérification de l'administrateur
        if (!empty($_SESSION['isAdmin'])) { ?>
            <li><a href="view/adminCheckRecipes.php">A Verifier</a></li>
            <li><a href="view/listUser.php">Utilisateurs</a></li>
        <?php } ?>
    </div>
   
    <div class="navbar_burger" onclick="openMenuMobile()">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 5.25h16.5m-16.5 4.5h16.5m-16.5 4.5h16.5m-16.5 4.5h16.5" />
        </svg>
    </div>
</div>

<?php
if (!empty($_SESSION['Login'])) {
    $userId = $_SESSION['id'];

    // Récupération du nombre d'entrées dans la table 'list' pour l'utilisateur connecté
    $stmt = $pdo->prepare("SELECT COUNT(*) as count FROM `list` WHERE `user_id` = ?");
    $stmt->execute([$userId]);
    $result = $stmt->fetch();

    if ($result) {
        $count = $result['count'];
    } else {
        $count = 0;
    }
    ?>
    <div id="listFix">
        <a href="model/list.php"><img src="image/list.png" width="55px" height="auto" alt="ingredients list parchemin"></a>
        <div id="badge"><?= htmlspecialchars($count) ?></div>
    </div>
<?php }?>
<script>
    function updateBadgeValue() 
    {
        // Appel à un fichier PHP via une requête AJAX pour récupérer la valeur du badge en temps réel
        fetch("model/getBadgeValue.php")
        .then(function(response) {
            if (response.ok) {
                return response.json();
            }
            throw new Error("Erreur lors de la récupération de la valeur du badge.");
        })
        .then(function(data) {
            document.getElementById("badge").textContent = data;
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    function toggleStyle() 
    {
        var cssLink = document.getElementById("cssLink");
        var detectiveImage = document.getElementById("detectiveImage");
        var indexDetectiveImage = document.getElementById("indexDetectiveImage");
        if (cssLink.getAttribute("href") === "style.css") 
        {
            cssLink.setAttribute("href", "stylew.css");
            detectiveImage.setAttribute("src", "image/womanTCD-removebg-preview.png");
            indexDetectiveImage.setAttribute("src", "image/womanTCD-removebg-preview.png");
        } 
        else 
        {
            cssLink.setAttribute("href", "style.css");
            detectiveImage.setAttribute("src", "image/image-removebg-preview.png");
            indexDetectiveImage.setAttribute("src", "image/image-removebg-preview.png");
        }

        // Enregistrer le style et l'image dans des cookies
        document.cookie = "style=" + cssLink.getAttribute("href") + "; expires=Wed, 31 Dec 3000 23:59:59 GMT; path=/";
        document.cookie = "image=" + indexDetectiveImage.getAttribute("src") + "; expires=Fri, 31 Dec 3000 23:59:59 GMT; path=/";
    }

    function loadStyleFromCookie() 
    {
        var cssLink = document.getElementById("cssLink");
        var detectiveImage = document.getElementById("detectiveImage");
        var indexDetectiveImage = document.getElementById("indexDetectiveImage");
        var cookies = document.cookie.split(";");

        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();

            if (cookie.startsWith("style=")) 
            {
                var style = cookie.substring(6);
                cssLink.setAttribute("href", style);
            }
            else if (cookie.startsWith("image="))
            {
                var image = cookie.substring(6);
                detectiveImage.setAttribute("src", image);
                indexDetectiveImage.setAttribute("src", image);
            }
        }
    }

    window.addEventListener("DOMContentLoaded", function() {
        updateBadgeValue();
        loadStyleFromCookie();
    });
</script>
<div class="allcontenu">
    <div class="contenue">
        <div class="background_contenue">
